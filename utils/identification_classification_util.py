import math
from sklearn.cluster import DBSCAN, KMeans
from configurations.identification_classification_config import ENTROPY_BASE

def fscore(n0, n1):

    if n0 + n1 == 0:
        return 0
    else:
        return 2*(n0*n1)/(n0 + n1)

def min_max(n, max_value):

    scale = n/max_value

    return scale

def score(n0, n1):

    return n0

def calculate_entropy(probabilities: list):
    entropy = 0
    if sum(probabilities) == 0:
        probabilities = [0.333, 0.333, 0.333]
    for p in probabilities:
        if p > 0:
            entropy = entropy - p * math.log(p, ENTROPY_BASE)

    return entropy

def dbscan_clustering(data:list, min_samples:int, eps:float):


    db = DBSCAN(eps=eps, min_samples=min_samples, algorithm='auto').fit(data)
    labels = db.labels_

    return labels

def kmeans_clustering(data:list, n_clusters:int):

   model = KMeans(n_clusters=n_clusters, random_state=0).fit(data)
   return model.labels_
