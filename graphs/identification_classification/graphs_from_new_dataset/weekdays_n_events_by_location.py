import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd

import sys
sys.path.append("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest")

from configurations.graphs_config import FILENAME, FILENAME_INDEX
palette = sns.color_palette()

dir = "/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/"
file = [dir + "location_n_events_by_week_1mil_.csv"]

df = pd.read_csv(file[0], encoding='utf-8')
print(df.columns, df, 'tamanho: ', df['outro'].shape[0])
location = ['Casa' for i in range(df['casa'].shape[0])] + ['Outro' for j in range(df['outro'].shape[0])] + ['Deslocamento' for k in range(df['deslocamento'].shape[0])]
events = []
events = events + df['casa'].tolist()
events = events + df['outro'].tolist()
events = events + df['deslocamento'].tolist()

df = pd.DataFrame({'Localização': location, 'Eventos': events})
# print("media dia de semana: ", df.query("'Tipo do dia' == 'Dia de semana'")['Entropia'].describe())
# print("media final de semana: ", df.query("'Tipo do dia' == 'Final de semana'")['Entropia'].describe())
print(df)
n = 0
metric = ['Eventos']

ax = plt.gca()
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(16)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(16)
#ax.tick_params(labelsize=14)
graph = sns.boxplot(x="Localização", y=metric[n], data=df).set_title("Eventos por localização (dia de semana)")
#plt.setp(ax.get_legend().get_texts(), fontsize='15')
ax.legend(loc='lower right', fontsize='15')
#precisao = sns.lineplot(x="Distância (m)", y="Precisão", data=dbscanpo, hue=["Dbscan puro"]*10, palette="prism_r")
figura_fscore = graph.get_figure()
figura_fscore.savefig("plots/" + "_week_" + metric[n] + "by_location" + ".png", dpi=400, bbox_inches='tight')