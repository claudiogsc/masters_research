import pandas as pd
from bokeh.io import output_file, show, output_notebook
from bokeh.models import ColumnDataSource, GMapOptions
from bokeh.plotting import figure, output_file, show, save
from operator import itemgetter
from geopy.distance import distance as distance
from geopy.distance import lonlat
import numpy as np
import seaborn as sns
from scipy.stats import norm
import statistics as st
import scipy.stats as ss
import matplotlib.pyplot as plt
import sys
sys.path.append("/home/claudio/Documentos/")
output_notebook()

file = "/home/claudio/Documentos/users_steps_datasets/df_mais_de_5272_mil_limite_500_pontos.csv"
df = pd.read_csv(file, infer_datetime_format=True, encoding='utf-8')[['installation_id', 'datetime']]
df.columns = ['id', 'datetime']
ids = df['id'].unique()
df['datetime'] = pd.to_datetime(df['datetime'], infer_datetime_format=True)
df['datetime'] = pd.to_datetime(df['datetime'], format='%Y-%m-%d %H:%M:%S')
ajuste = pd.to_datetime('1970-01-01 03:00:00', format='%Y-%m-%d %H:%M:%S')
datetime = pd.to_datetime(df['datetime'] - ajuste, format='%Y-%m-%d %H:%M:%S')
df['datetime'] = datetime
quantidade_marcados=[]
quantidade=[]
minimo=10000000000
maximo=0
cont=0
usuarios_pontos=[0]*len(ids)
for i in range(len(ids)):
    usuario=df.query("id=="+str(ids[i])).sort_values(by=['datetime'])
    usuario=usuario.drop_duplicates(subset=['datetime'])
    #print(i)
    #print(tempo)
    datetime=pd.to_timedelta(usuario['datetime'])
    tempos=datetime.dt.total_seconds().tolist()
    usuarios_pontos[i]=len(tempos)

df=pd.DataFrame(data={"Id":[i for i in range(len(ids))],"Total de registros":usuarios_pontos})
sns.set()
ax = plt.subplots()[1]
ax.set_xlabel("Users", fontsize=14)
ax.set_ylabel("Total of records", fontsize=14)
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(12)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(12)
total = sns.distplot(df['Total de registros'], kde=False, bins=[150,300,450,600])
total.set_ylabel("Users")
total = total.get_figure()
total.savefig("plots/total_pontosxusuario_ingles.png", bbox_inches='tight', dpi=400, ax=ax)