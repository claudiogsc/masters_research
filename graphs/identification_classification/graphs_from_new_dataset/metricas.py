import pandas as pd
from bokeh.io import output_file, show, output_notebook
from bokeh.models import ColumnDataSource, GMapOptions
from bokeh.plotting import figure, output_file, show, save
from operator import itemgetter
from geopy.distance import vincenty as distance
from geopy.distance import lonlat
import numpy as np
import seaborn as sns
from scipy.stats import norm
import statistics as st
import scipy.stats as ss
import matplotlib.pyplot as plt
import datetime as dt
import sys

sys.path.append("/home/claudio/Documentos/")
output_notebook()

dir_ = "/home/claudio/Documentos/users_steps_datasets/df_mais_de_5272_mil_limite_500_pontos.csv"
df = pd.read_csv(dir_, infer_datetime_format=True, encoding='utf-8')

#df2 = pd.read_csv("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/location_sequence_24hours_10mil_usuarios.csv", infer_datetime_format=True, encoding='utf-8')

ids = df['installation_id'].unique().tolist()
print("ids: ", len(ids))
df['datetime'] = pd.to_datetime(df['datetime'], infer_datetime_format=True)
#df['datetime'] = pd.to_datetime(df['datetime'], format='%Y-%m-%d %H:%M:%S')
#ajuste = pd.to_datetime('1970-01-01 03:00:00', format='%Y-%m-%d %H:%M:%S')
#datetime = pd.to_datetime(df['datetime'] - ajuste, format='%Y-%m-%d %H:%M:%S')
#df['datetime'] = datetime
print("Datetime describe: ", df['datetime'].describe())
quantidade_marcados = []
quantidade = []
cont = 0
diferencas = []
periodo = []
total_registros = 0
for i in ids:
    usuario = df.query("installation_id==" + str(i)).sort_values(by=['datetime'])
    cont = cont + 1
    usuario = usuario.drop_duplicates(subset=['datetime'])
    tempos = usuario['datetime'].tolist()
    total_registros = total_registros + len(tempos)

print("usuarios: ", cont)
print("total registros: ", total_registros)