import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd

import sys
sys.path.append("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest")

from configurations.graphs_config import FILENAME, FILENAME_INDEX
palette = sns.color_palette()

#conv = {"Week": "Dia de semana", "Weekend": "Final de semana"}
ing = {"Week": "Weekday", "Weekend": "Weekend"}

dir = "/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/"
file = [dir + "entropy_by_week_weekend_regiontype_10mil_usuarios.csv"]

df = pd.read_csv(file[0], encoding='utf-8')
df = df[[df.columns[0], df.columns[5]]]
df['Type'] = df['Type'].apply(lambda e:ing[e])
print(df.columns, df)
df.columns = ['Entropy', 'Day type']
# print("media dia de semana: ", df.query("'Tipo do dia' == 'Dia de semana'")['Entropia'].describe())
# print("media final de semana: ", df.query("'Tipo do dia' == 'Final de semana'")['Entropia'].describe())

#df['Tipo do dia'] = pd.Series([conv[e] for e in df['Tipo do dia'].tolist()])
print(df.columns)
n = 0
metric = ['Entropy']

ax = plt.gca()
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(16)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(16)
#ax.tick_params(labelsize=14)
graph = sns.boxplot(x="Day type", y=metric[n], data=df).set_title("Entropy per day type")
#plt.setp(ax.get_legend().get_texts(), fontsize='15')
ax.legend(loc='lower right', fontsize='15')
#precisao = sns.lineplot(x="Distância (m)", y="Precisão", data=dbscanpo, hue=["Dbscan puro"]*10, palette="prism_r")
figura_fscore = graph.get_figure()
figura_fscore.savefig("plots/" + "_week_weekend_" + metric[n] + "_ingles.png", dpi=400, bbox_inches='tight')