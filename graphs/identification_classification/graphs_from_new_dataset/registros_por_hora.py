import pandas as pd
from bokeh.io import output_file, show, output_notebook
import seaborn as sns
from scipy.stats import norm
import statistics as st
import scipy.stats as ss
import matplotlib.pyplot as plt
import pytz
import datetime as dt
import sys
from timezonefinder import TimezoneFinder
from tzwhere import tzwhere

sys.path.append("/home/claudio/Documentos/")
output_notebook()

# def Horarios_Marcacao(tempos):
#     t = [None] * len(tempos)
#     q = [0] * 24
#     for i in range(len(tempos)):
#         t[i] = dt.datetime.fromtimestamp(int(tempos[i]))
#     for i in range(len(t)):
#         q[t[i].hour] = q[t[i].hour] + 1

def utc_to_local_time(df):
    cont = 0
    nulos = 0
    proximo = 1000
    #tzw = tzwhere.tzwhere()
    datetimes = []
    tf = TimezoneFinder()
    for i in range(df.shape[0]):
        cont+=1
        if cont > proximo:
            #print(cont)
            proximo+=1000
            #print(anterior, datetime, tz)
        r = df.iloc[i]
        id = r[0]


        latitude, longitude = r[1], r[2]
        datetime = dt.datetime.strptime(str(r[3]), '%Y-%m-%d %H:%M:%S')
        anterior = datetime
        #tz = tzw.tzNameAt(latitude, longitude)
        tz = tf.timezone_at(lng=longitude, lat=latitude)
        if tz is None:
            nulos+=1
            datetimes.append(datetime)
            continue
        tz = pytz.timezone(tz)
        datetime = datetime.replace(tzinfo=dt.timezone.utc)
        datetime = datetime.astimezone(tz=tz)
        #print(datetime)
        datetimes.append(datetime.strftime('%Y-%m-%d %H:%M:%S'))

    #print("nulos: ", nulos)
    df['datetimes'] = pd.Series(datetimes)
    return df

file = "/home/claudio/Documentos/users_steps_datasets/df_mais_de_5272_mil_limite_500_pontos.csv"
df = pd.read_csv(file, infer_datetime_format=True, encoding='utf-8')[['installation_id', 'latitude', 'longitude', 'datetime']]
print("tamanho: ", df.shape)
df.columns = ['id', 'latitude', 'longitude', 'datetime']
ids = df['id'].unique()
#df['datetime'] = pd.to_datetime(df['datetime'], infer_datetime_format=True)
df['datetime'] = pd.to_datetime(df['datetime'], format='%Y-%m-%d %H:%M:%S')
datetimes = df['datetime'].tolist()
datetimes = pd.Series([d-dt.timedelta(hours=3) for d in datetimes])
df['datetime'] = datetimes
df['latitude'] = df['latitude'].apply(lambda e: round(e, 3))
df['longitude'] = df['longitude'].apply(lambda e: round(e, 3))
df = utc_to_local_time(df)
quantidade_marcados = []
quantidade = []
minimo = 10000000000
maximo = 0
cont = 0
diferencas = []
q = [0] * 24
aux = 0
primeiro = []
horas = []
for i in list(ids):
    usuario = df.query("id==" + str(i)).sort_values(by=['datetime'])
    usuario = usuario.drop_duplicates(subset=['datetime'])
    # print(i)
    # print(tempo)
    #datetime = pd.to_timedelta(usuario['datetime'])
    tempos = usuario['datetime'].tolist()
    t = [None] * len(tempos)
    for j in range(len(tempos)):
        t[j] = tempos[j]
    for j in range(len(t)):
        q[t[j].hour] = q[t[j].hour] + 1

print("primeiro")
s = sum(q[5:18])
t = sum(q)
#q = [i/t for i in q]
print("Total de registros: ", t)
print("Porcentagem:", (s * 100) / t)
# print(sorted(primeiro))
ax = plt.subplots()[1]
ax.set_xlabel("Hora", fontsize=14)
ax.set_ylabel("Registros", fontsize=14)
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(12)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(12)
df = pd.DataFrame(data={"Hour": [i for i in range(len(q))], "Total of records": q})
sns.set()
total = sns.barplot(x="Hour", y='Total of records', data=df, color="cornflowerblue")
# total = sns.distplot(horas, kde=False, hist=True)
total.set_ylabel("Total of records")
plt.xticks(rotation=45)
# plt.xltickslabels(['0'])
total = total.get_figure()
total.savefig("plots/total_pontosxhorario_ingles.png", dpi=400, bbox_inches='tight', figsize=(110, 110), ax=ax)