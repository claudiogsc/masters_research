import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd

import sys
sys.path.append("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest")

from configurations.graphs_config import FILENAME, FILENAME_INDEX
palette = sns.color_palette()

dir = "/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/"
file = [dir + "location_n_events_by_week_10mil_.csv", dir + "location_n_events_by_weekend_10mil_.csv"]

df_weekdays = pd.read_csv(file[0], encoding='utf-8')

print(df_weekdays.columns, df_weekdays, 'tamanho: ', df_weekdays['outro'].shape[0])
location_weekdays = ['Home' for i in range(df_weekdays['casa'].shape[0])] + \
           ['Other' for j in range(df_weekdays['outro'].shape[0])] + \
           ['Commuting' for k in range(df_weekdays['deslocamento'].shape[0])]
events_weekdays = []
events_weekdays = events_weekdays + df_weekdays['casa'].tolist()
events_weekdays = events_weekdays + df_weekdays['outro'].tolist()
events_weekdays = events_weekdays + df_weekdays['deslocamento'].tolist()
weekday = ['Weekday']*len(events_weekdays)

df_weekends = pd.read_csv(file[1], encoding='utf-8')
print(df_weekends.columns, df_weekends, 'tamanho: ', df_weekends['outro'].shape[0])
location_weekends = ['Home' for i in range(df_weekends['casa'].shape[0])] + \
           ['Other' for j in range(df_weekends['outro'].shape[0])] + \
           ['Commuting' for k in range(df_weekends['deslocamento'].shape[0])]
events_weekends = []
events_weekends = events_weekends + df_weekends['casa'].tolist()
events_weekends = events_weekends + df_weekends['outro'].tolist()
events_weekends = events_weekends + df_weekends['deslocamento'].tolist()
weekday = weekday + ['Weekend']*len(events_weekends)

location = location_weekdays + location_weekends
events = events_weekdays + events_weekends
events = [e*100 for e in events]
df = pd.DataFrame({'Location': location, 'Records (%)': events, 'Weekday': weekday})
# print("media dia de semana: ", df.query("'Tipo do dia' == 'Dia de semana'")['Entropia'].describe())
# print("media final de semana: ", df.query("'Tipo do dia' == 'Final de semana'")['Entropia'].describe())
print(df)
n = 0
metric = ['Records (%)']

ax = plt.gca()
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(12)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(16)
#ax.tick_params(labelsize=14)
graph = sns.boxplot(x="Location", y=metric[n], hue='Weekday', data=df).set_title("Events per location")
#plt.setp(ax.get_legend().get_texts(), fontsize='15')
ax.legend(loc='upper center', fontsize='12')
#precisao = sns.lineplot(x="Distância (m)", y="Precisão", data=dbscanpo, hue=["Dbscan puro"]*10, palette="prism_r")
figura_fscore = graph.get_figure()
figura_fscore.savefig("plots/" + "_week_weekend_eventos_" + "by_location_ingles" + ".png", dpi=400, bbox_inches='tight')