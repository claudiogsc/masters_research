import pandas as pd
from bokeh.io import output_file, show, output_notebook
from bokeh.models import ColumnDataSource, GMapOptions
from bokeh.plotting import figure, output_file, show, save
from operator import itemgetter
from geopy.distance import vincenty as distance
from geopy.distance import lonlat
import numpy as np
import seaborn as sns
from scipy.stats import norm
import statistics as st
import scipy.stats as ss
import matplotlib.pyplot as plt
import datetime as dt
import sys

sys.path.append("/home/claudio/Documentos/")
output_notebook()

# "/home/claudio/Documentos/users_steps_datasets/df_mais_de_10_mil_limite_500_pontos.csv"
def porcentagem(l):
    l2 = 0
    for i in l:
        if i > 700:
            l2 = l2 + 1
    print("acime de 700 minutos:", (l2 * 100) / len(l))

dir_ = "/home/claudio/Documentos/users_steps_datasets/df_mais_de_10_mil_limite_500_pontos.csv"
df = pd.read_csv(dir_, infer_datetime_format=True, encoding='utf-8')
df.columns = ['id', 'installation_id', 'reference_date', 'latitude', 'longitude', 'created_at', 'updated_at']
df = df[['id', 'installation_id', 'reference_date', 'latitude', 'longitude']]
df.columns = ['id', 'installation_id', 'datetime', 'latitude', 'longitude']

#df2 = pd.read_csv("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/location_sequence_24hours_10mil_usuarios.csv", infer_datetime_format=True, encoding='utf-8')

dir_sequence = "/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/location_sequence_24hours_10mil_usuarios_id.csv"
ids = pd.read_csv(dir_sequence)['user_id_original'].tolist()
print("ids: ", len(ids))

df = df.query("installation_id in " + str(ids))
print("ids final: ", len(df['installation_id'].unique().tolist()))

df.to_csv("/home/claudio/Documentos/users_steps_datasets/df_mais_de_5272_mil_limite_500_pontos.csv")