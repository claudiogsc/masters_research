import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import geopandas as gp
from scipy import stats
import sys
sys.path.append("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/")
sys.path.append("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest")

from configurations.identification_classification_config import CIDADES_INFO_COLUMS, NEW_CIDADES_INFO_COLUMNS

dir = "/home/claudio/Documentos/pycharmprojects/git_points_of_interest/"
entropy = "1994points_ufvinterest/"

df_ibge = pd.read_csv(dir + "cidades_info_2016.csv")[CIDADES_INFO_COLUMS]
df_ibge.columns = NEW_CIDADES_INFO_COLUMNS
df_ibge['State'] = df_ibge['State'].str.lower()
df_ibge = df_ibge[['State', 'População']].groupby('State').sum()

df_entropy = pd.read_csv(dir + entropy + "entropy_all_regiontype_1mil_usuarios.csv")
df_entropy = df_entropy[["Average entropy", "State"]].groupby('State').median().reset_index(level='State')
print("entropy: ", df_entropy.columns, df_entropy.shape)
print("ibge: ", df_ibge.shape, df_ibge.head(10))

df = df_entropy.join(df_ibge, on='State', how='inner')[['State', 'População', 'Average entropy']]

print(df.shape, df)

sns.set(style="white", color_codes=True)

#f, ax = plt.subplots(figsize=(7, 7))
#ax.set(xscale="log")

#df['pib_per_c'] = df['pib_per_c'].apply(lambda e: e/1000)
f = sns.jointplot(x="Average entropy", y="População", data=df, kind="reg")
f.ax_joint.set_xscale('log')
#f.ax_joint.set_xticks(f.ax_joint.get_xticks())
f.annotate(stats.pearsonr, fontsize=16, loc = (0.15, 0.86))
print(type(f))
#figura = f.get_figure()
f.set_axis_labels("Entropy", "População", fontsize = 18)
f.savefig("plots/joint_plot_state_entropy_populacao.png", bbox_inches='tight', dpi=400)