import folium
from folium import plugins
import pandas as pd
from bokeh.io import output_file, show, output_notebook
import sys

sys.path.append("/home/claudio/Documentos/")

file = "/home/claudio/Documentos/users_steps_datasets/df_mais_de_10_mil_limite_500_pontos.csv"
df = pd.read_csv(file, infer_datetime_format=True, encoding='utf-8')[['installation_id', 'latitude', 'longitude', 'reference_date']]
df.columns = ['id', 'latitude', 'longitude', 'datetime']
ids = df['id'].unique()
df['datetime'] = pd.to_datetime(df['datetime'], infer_datetime_format=True)
df['datetime'] = pd.to_datetime(df['datetime'], format='%Y-%m-%d %H:%M:%S')
ajuste = pd.to_datetime('1970-01-01 03:00:00', format='%Y-%m-%d %H:%M:%S')
datetime = pd.to_datetime(df['datetime'] - ajuste, format='%Y-%m-%d %H:%M:%S')
df['datetime'] = datetime
for i in range(len(ids)):
    usuario = df.query("id==" + str(ids[i])).sort_values(by=['datetime'])
    usuario = usuario.drop_duplicates(subset=['datetime'])
    if i == 0:
        pontos = usuario[['latitude', 'longitude']]
    else:
        pontos = pd.concat([pontos, usuario[['latitude', 'longitude']]])

#print(pontos)
latitude = pontos['latitude'].tolist()
longitude = pontos['longitude'].tolist()
coordenadas = pd.DataFrame({"latitude": latitude, "longitude": longitude}).values

# plot heatmap
m = folium.Map([latitude[0], longitude[0]], zoom_start=4)
m.add_child(plugins.HeatMap(coordenadas, radius=15))
m.save("heatmap_eventos.html")