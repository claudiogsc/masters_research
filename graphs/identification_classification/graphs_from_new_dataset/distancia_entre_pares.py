import pandas as pd
from bokeh.io import output_file, show, output_notebook
from bokeh.models import ColumnDataSource, GMapOptions
from bokeh.plotting import figure, output_file, show, save
from operator import itemgetter
from geopy.distance import vincenty as distance
from geopy.distance import lonlat
import numpy as np
import seaborn as sns
from scipy.stats import norm
import statistics as st
import scipy.stats as ss
import matplotlib.pyplot as plt
import datetime as dt
import sys
from geopy.distance import distance as distance

sys.path.append("/home/claudio/Documentos/")
output_notebook()

# "/home/claudio/Documentos/users_steps_datasets/df_mais_de_10_mil_limite_500_pontos.csv"
def porcentagem(l):
    l2 = 0
    for i in l:
        if i > 700:
            l2 = l2 + 1
    print("acime de 700 minutos:", (l2 * 100) / len(l))

idioma = 'ingles'
idioma_dict = {'portugues': {'x': 'Distância (metros)', 'y': 'Probabilidade (x <= X)', 'file': ''}, 'ingles':
    {'x': 'Distance (meters)', 'y': 'Probability (x <= X)', 'file': 'ingles'}}
idioma_dict = idioma_dict[idioma]

file = "/home/claudio/Documentos/users_steps_datasets/df_mais_de_5272_mil_limite_500_pontos.csv"
df = pd.read_csv(file, infer_datetime_format=True, encoding='utf-8')[['id', 'installation_id', 'datetime', 'latitude', 'longitude']]
df.columns = ['id', 'installation_id', 'datetime', 'latitude', 'longitude']
df = df[['id', 'installation_id', 'datetime', 'latitude', 'longitude']]
df.columns = ['id', 'installation_id', 'datetime', 'latitude', 'longitude']

#df2 = pd.read_csv("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/location_sequence_24hours_10mil_usuarios.csv", infer_datetime_format=True, encoding='utf-8')

ids = df['installation_id'].unique().tolist()
print("ids: ", len(ids))
df['datetime'] = pd.to_datetime(df['datetime'], infer_datetime_format=True)
#df['datetime'] = pd.to_datetime(df['datetime'], format='%Y-%m-%d %H:%M:%S')
#ajuste = pd.to_datetime('1970-01-01 03:00:00', format='%Y-%m-%d %H:%M:%S')
#datetime = pd.to_datetime(df['datetime'] - ajuste, format='%Y-%m-%d %H:%M:%S')
#df['datetime'] = datetime
quantidade_marcados = []
quantidade = []
cont = 0
diferencas = []
periodo = []
for i in ids:
    usuario = df.query("installation_id==" + str(i)).sort_values(by=['datetime'])
    # if usuario.shape[0] < 100:
    #     continue
    cont = cont + 1
    usuario = usuario.drop_duplicates(subset=['datetime'])
    coordenadas = usuario[['latitude', 'longitude']]
    la = usuario['latitude'].tolist()
    lo = usuario['longitude'].tolist()
    for j in range(len(la) - 1):
        dife = distance((la[j],lo[j]),(la[j+1], lo[j+1])).meters
        diferencas.append(dife)

print("usuarios: ", cont)
porcentagem_diferencas = [0] * len(quantidade)
diferencas = sorted(diferencas)

sns.set()

diferencas = sorted(diferencas)
cont = 0
for i in diferencas:
    if i > 47000:
        cont+=1
print("Maiores que 47000: ", cont/len(diferencas))
print("Mediana: ", st.median(diferencas))
s = pd.Series(diferencas)
print(s.describe())
print(s.median())

plt.style.use('seaborn')
box = dict(alpha=0.2)
s = s.sort_values()  # funcionando
s[len(s)] = s.iloc[-1]
cum_dist = np.linspace(0., 1., len(s))
s_cdf = pd.Series(cum_dist, index=s)
fig, ax = plt.subplots()
ax.set_xlabel(idioma_dict['x'], fontsize=18)
ax.set_ylabel(idioma_dict['y'], fontsize=18)
ax.set_xlim((0, 47000))
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(17)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(17)
figura_tempo = s_cdf.plot(ax=ax)  # , drawstyle='steps'
figura_tempo = figura_tempo.get_figure()
figura_tempo.savefig("plots/distancia_entre_pares" + "_" + idioma_dict['file'] + ".png", bbox_inches='tight', dpi=400, ax=ax)