import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd

from configurations.graphs_config import FILENAME, FILENAME_INDEX
palette = sns.color_palette()

dir = "/home/claudio/Documentos/pycharmprojects/git_points_of_interest/points_of_interest/"
file = [dir + FILENAME[FILENAME_INDEX]]

df = pd.read_csv(file[0], encoding='utf-8')
print(df.columns)
df.columns = ['Average entropy', 'Average score', 'Median entropy', 'Median score', 'Region type', 'Type']
print("average weekend score: ", df.query("Type == 'Weekend'")['Average score'].describe())
print("average week score: ", df.query("Type == 'Week'")['Average score'].describe())
print("median weekend score: ", df.query("Type == 'Weekend'")['Average score'].describe())
print("median week score: ", df.query("Type == 'Week'")['Average score'].describe())
print(df.columns)
n = 1
metric = ['Average entropy', 'Average score', 'Median entropy', 'Median score', 'Type']

ax = plt.gca()
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(16)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(16)
#ax.tick_params(labelsize=14)
graph = sns.boxplot(x="Type", y=metric[n], data=df).set_title("Week vs Weekend")
#plt.setp(ax.get_legend().get_texts(), fontsize='15')
ax.legend(loc='lower right', fontsize='15')
#precisao = sns.lineplot(x="Distância (m)", y="Precisão", data=dbscanpo, hue=["Dbscan puro"]*10, palette="prism_r")
figura_fscore = graph.get_figure()
figura_fscore.savefig("plots/" + metric[n] + ".png", dpi=400, bbox_inches='tight')