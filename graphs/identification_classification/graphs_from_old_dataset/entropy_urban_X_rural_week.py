import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd

import sys
sys.path.append("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/points_of_interest")
from configurations.graphs_config import FILENAME, FILENAME_INDEX
palette = sns.color_palette()

REGION_INDEX = 1
REGION_TYPE = ['URBAN', 'RURAL'][REGION_INDEX]

metric_index = 0
metric = ['Average entropy', 'Average score', 'Median entropy', 'Median score', 'Type']

eng_to_pt = {"URBANO":"URBAN", "RURAL":"RURAL"}

dir = "/home/claudio/Documentos/pycharmprojects/git_points_of_interest/points_of_interest/"
file = [dir + FILENAME[FILENAME_INDEX]]

df = pd.read_csv(file[0], encoding='utf-8')
df.columns = ['Average entropy', 'Average score', 'Median entropy', 'Median score', 'Region_type', 'Type']
df['Region_type'] = df['Region_type'].apply(lambda e:eng_to_pt[e])
df = df.query("Type == 'Week'")
print("Quantidade de usuarios que moram em áreas rurais: ", df.query("Region_type == '" + REGION_TYPE + "'").shape[0])
# print("filtro: ", df.shape)
print("average urban score: ", df.query("Region_type == 'URBAN'")['Average score'].describe())
print("average rural score: ", df.query("Region_type == 'RURAL'")['Average score'].describe())

print(df.columns)

ax = plt.gca()
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(16)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(16)
#ax.tick_params(labelsize=14)
graph = sns.boxplot(x="Region_type", y=metric[metric_index], data=df).set_title("Urban vs Rural (Week)")
#plt.setp(ax.get_legend().get_texts(), fontsize='15')
ax.legend(loc='lower right', fontsize='15')
#precisao = sns.lineplot(x="Distância (m)", y="Precisão", data=dbscanpo, hue=["Dbscan puro"]*10, palette="prism_r")
figura_fscore = graph.get_figure()
figura_fscore.savefig("plots/" + "urban_X_rural_" + metric[metric_index] + "_week.png", dpi=400, bbox_inches='tight')