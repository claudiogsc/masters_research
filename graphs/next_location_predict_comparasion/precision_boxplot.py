import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd

from pathlib import Path
import sys
sys.path.append("/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/")
from configurations.next_location_predict_baseline_config import RNNs, FIGURES_DIR
from configurations.next_location_predict_config import RNNs as RNNs_2, FIGURES_DIR as FIGURES_DIR_2
palette = sns.color_palette()

dir = "/home/claudio/Documentos/pycharmprojects/git_points_of_interest/1994points_ufvinterest/"

idioma = {'portugues': {'metric': ["Precisão", "Revocação", "F1-score"],
                        'x_axis': 'Solução', 'col': 'Localização', 'location': {'Casa': 'Casa',
                                                                                'Outro': 'Outro',
                                                                                'Deslocamento': 'Deslocamento'}},
          'ingles': {'metric': ["Precision", "Recall", "F1-score"],
                     'x_axis': 'Solution', 'col': 'Location', 'location': {'Casa': 'Home',
                                                                           'Outro': 'Other',
                                                                           'Deslocamento': 'Commuting'}}}

idioma_index = 'portugues'

idioma = idioma[idioma_index]

# whether to get data generated through cross validation (5 folds) or not
data_of_cross_validation = {True: '5', False: '1'}
data_of_cross_validation_index = True
data_of_cross_validation = data_of_cross_validation[data_of_cross_validation_index]

type = {0: 1, 1: 1}
type_index = 1
dataset_type = {0: "_original", 1: "_filled"}[type_index]
baselines_solutions_ids = {0: [0, 2, 4, 7], 1: [1, 3, 5, 8]}[type_index]
baselines_solutions_names = {0: "MHSA+PE", 1: "MHSA+PE", 2: "STF-RNN", 3: "STF-RNN", 4: "SERM",
                             5: "SERM", 7: "MAP", 8: "MAP"}
soludion_id = type[type_index]
solution_name = {1: "MFA-RNN", 5: "MFA-RNN", 6: "MFA-RNN"}

metric = idioma['metric']
metric_index = 0

dfs = None
for id_ in baselines_solutions_ids:
    filename = dir + FIGURES_DIR[id_] + "plot_report__10mil___" + data_of_cross_validation + ".csv"
    df = pd.read_csv(filename)[['precision', 'location']]
    df['location'] = df['location'].apply(lambda e: idioma['location'][e])
    size = df.shape[0]
    df['Solução'] = [baselines_solutions_names[id_]] * size
    if dfs is None:
        dfs = df
    else:
        dfs = pd.concat([dfs, df], ignore_index=True)

for id_ in [soludion_id]:
    filename = dir + FIGURES_DIR_2[id_] + "plot_report__10mil___" + data_of_cross_validation + ".csv"
    df = pd.read_csv(filename)[['precision', 'location']]
    df['location'] = df['location'].apply(lambda e: idioma['location'][e])
    size = df.shape[0]
    df['Solução'] = [solution_name[id_]] * size
    dfs = pd.concat([dfs, df], ignore_index=True)

dfs.columns = [metric[metric_index], idioma['col'], idioma['x_axis']]
print(dfs.columns)

ax = plt.gca()
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(18)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(18)
#ax.tick_params(labelsize=14)
graph = sns.catplot(x=idioma['x_axis'], y=metric[metric_index], col=idioma['col'], data=dfs, kind="box")
#.set_title("Urban vs Rural (Week)")
#plt.setp(ax.get_legend().get_texts(), fontsize='15')
ax.legend(loc='lower right', fontsize='18')
#precisao = sns.lineplot(x="Distância (m)", y="Precisão", data=dbscanpo, hue=["Dbscan puro"]*10, palette="prism_r")
#figura_fscore = graph.get_figure()
graph.savefig(metric[metric_index] + dataset_type+ "_10mil_" + idioma_index + ".png", dpi=400, bbox_inches='tight')

