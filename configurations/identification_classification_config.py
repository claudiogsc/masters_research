N_USUARIOS = 5500
#DAYS_TO_PREDICT = 8
DAYS_TO_TRAIN = 14
METRIC = "cosine"
DAYS_TO_PREDICT = [3, 7, 11, 14]

MIN_TRAIN_PERIOD = 6
N_AMOSTRAS = 8
EPS = 0.03/6371.0088
RESTRICTION = True

ENTROPY_BASE = 2.7

# distância mínima de um step e um poi
DISTANCIA_MINIMA = 30
LOCATION_ID = {"Casa":0, "Trabalho":1, "Outro":1, "Deslocamento":2}

JOB_CONFIG_INDEX = 1

FILENAME = ["df_mais_de_1_mil_limite_500_pontos.csv",
                    "/home/claudio/Documentos/users_steps_datasets/df_mais_de_10_mil_limite_500_pontos.csv"][1]

JOB_CONFIG = {0: {"filename": "df_mais_de_1_mil_limite_500_pontos.csv", "output_sufix": "_1mil_usuarios.csv", "users_limit": -1},
              1: {"filename": "/home/claudio/Documentos/users_steps_datasets/df_mais_de_10_mil_limite_500_pontos.csv",
                      "output_sufix": "_10mil_usuarios_id.csv", "users_limit": -1},
              2: {"filename": "/home/claudio/Documentos/users_steps_datasets/df_mais_de_10_mil_limite_500_pontos.csv",
                  "output_sufix": "_5mil_usuarios.csv", "users_limit": 5200}}

CIDADES_INFO_COLUMS = ['Ano', 'Código da Grande Região', 'Nome da Grande Região',
       'Código da Unidade da Federação', 'Sigla da Unidade da Federação',
       'Nome da Unidade da Federação', 'Código do Município',
       'Nome do Município', 'Região Metropolitana', 'Código da Mesorregião',
       'Nome da Mesorregião', 'Código da Microrregião', 'Nome da Microrregião',
       'Código da Região Rural', 'Nome da Região Rural',
       'Tipo da Região Rural', 'Código da Região Geográfica Imediata',
       'Nome da Região Geográfica Imediata',
       'Município da Região Geográfica Imediata',
       'Código da Região Geográfica Intermediária',
       'Nome da Região Geográfica Intermediária',
       'Município da Região Geográfica Intermediária', 'Amazônia Legal',
       'Semiárido', 'Código Concentração Urbana', 'Nome Concentração Urbana',
       'Tipo Concentração Urbana', 'Código Arranjo Populacional',
       'Nome Arranjo Populacional', 'Tipologia Rural-Urbana',
       'Hierarquia Urbana', 'Hierarquia Urbana (principais categorias)',
       'Cidade-Região de São Paulo',
       'Valor adicionado bruto da Agropecuária, a preços correntes (R$ 1000)',
       'Valor adicionado bruto da Indústria, a preços correntes (R$ 1000)',
       'Valor adicionado bruto dos Serviços, a preços correntes - exclusive Administração, defesa, educação e saúde públicas e seguridade social (R$ 1000)',
       'Valor adicionado bruto da Administração, defesa, educação e saúde públicas e seguridade social (R$ 1000)',
       'Valor adicionado bruto total, a preços correntes (R$ 1000)',
       'Impostos, líquidos de subsídios, sobre produtos, a preços correntes (R$ 1000)',
       'Produto Interno Bruto, a preços correntes (R$ 1000)',
       'População (Nº de habitantes)',
       'Produto Interno Bruto per capita (R$ 1,00)',
       'Atividade com maior valor adicionado bruto',
       'Atividade com segundo maior valor adicionado bruto',
       'Atividade com terceiro maior valor adicionado bruto']

NEW_CIDADES_INFO_COLUMNS = ['Ano', 'Código da Grande Região', 'Nome da Grande Região',
       'Código da Unidade da Federação', 'State',
       'Nome da Unidade da Federação', 'CD_GEOCODM',
       'NM_MUNICIO', 'Região Metropolitana', 'Código da Mesorregião',
       'Nome da Mesorregião', 'Código da Microrregião', 'Nome da Microrregião',
       'Código da Região Rural', 'Nome da Região Rural',
       'Tipo da Região Rural', 'Código da Região Geográfica Imediata',
       'Nome da Região Geográfica Imediata',
       'Município da Região Geográfica Imediata',
       'Código da Região Geográfica Intermediária',
       'Nome da Região Geográfica Intermediária',
       'Município da Região Geográfica Intermediária', 'Amazônia Legal',
       'Semiárido', 'Código Concentração Urbana', 'Nome Concentração Urbana',
       'Tipo Concentração Urbana', 'Código Arranjo Populacional',
       'Nome Arranjo Populacional', 'Tipologia Rural-Urbana',
       'Hierarquia Urbana', 'Hierarquia Urbana (principais categorias)',
       'Cidade-Região de São Paulo',
       'Valor adicionado bruto da Agropecuária, a preços correntes (R$ 1000)',
       'Valor adicionado bruto da Indústria, a preços correntes (R$ 1000)',
       'Valor adicionado bruto dos Serviços, a preços correntes - exclusive Administração, defesa, educação e saúde públicas e seguridade social (R$ 1000)',
       'Valor adicionado bruto da Administração, defesa, educação e saúde públicas e seguridade social (R$ 1000)',
       'Valor adicionado bruto total, a preços correntes (R$ 1000)',
       'Impostos, líquidos de subsídios, sobre produtos, a preços correntes (R$ 1000)',
       'PIB',
       'População',
       'Produto Interno Bruto per capita (R$ 1,00)',
       'Atividade com maior valor adicionado bruto',
       'Atividade com segundo maior valor adicionado bruto',
       'Atividade com terceiro maior valor adicionado bruto']