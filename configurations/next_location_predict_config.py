from keras.optimizers import Nadam, Adam

STEP_SIZE = 20

LOCATION_INPUT_DIM = 3
TIME_INPUT_DIM = 24

RNNs = {0: "GRU_enhaced_1location_original_10mil", 1: "GRU_enhaced_1location_filled_10mil", 3: "GRUenhaced_GroupedUsersRoutine",
        4: "GRUenhaced_1location_output", 5: "GRU_enhaced_1location_original", 6: "GRU_enhaced_1location_filled"}

N_TESTES = 5
EPOCHS = 8

N_SPLITS = 5

CLASS_WEIGHT = {0: {0: 2, 1: 1.3, 2: 0.5}, 1: {0: 1.2, 1: 2.8, 2: 1}, 5: {0: 2.6, 1: 2, 2: 1}, 6: {0: 1, 1: 2.6, 2: 1}}

INDEX_SIZE = 1

SIZE = {0: "_1mil_", 1: "_10mil_"}[INDEX_SIZE]
N_USUARIOS = {0: 970, 1: 3050}[INDEX_SIZE]

DATASET = {0: {"file_name": "location_sequence_24hours" + SIZE + "usuarios.csv",
               "time_num_classes": 24, "use_entropy": False, "entropy_num_classes": 8, "features": 3},
           1: {"file_name": "location_sequence_24hours_filled" + SIZE + "usuarios.csv",
               "time_num_classes": 24, "use_entropy": False, "entropy_num_classes": 8, "features": 3},
           2: {"file_name": "location_sequence_48hours.csv", "time_num_classes": 48,
               "use_entropy": False, "entropy_num_classes": 0, "features": 2},
           3: {"file_name": "location_sequence_new_id_grouped_routine_dbscan_1mil_usuarios.csv",
               "time_num_classes": 24, "use_entropy": False, "entropy_num_classes": 0, "n_usuarios": 949, "features": 2},
           4: {"file_name": "location_sequence_24hours_week_weekend_1mil_usuarios.csv",
               "time_num_classes": 24, "use_entropy": False, "entropy_num_classes": 0, "features": 3},
           5: {"file_name": "location_sequence_24hours" + SIZE + "usuarios.csv",
               "time_num_classes": 24, "use_entropy": False, "entropy_num_classes": 8, "features": 3},
           6: {"file_name": "location_sequence_24hours_filled" + SIZE + "usuarios.csv",
               "time_num_classes": 24, "use_entropy": False, "entropy_num_classes": 8, "features": 3}}
#location_sequence_24hours_week_weekend_1mil_usuarios
TIME_OUTPUT = {0: False, 1: False, 2: True, 3: True, 4: False, 5: False, 6: False}

N_LOCATION_OUTPUT = {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5:1, 6:1}

nadam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, amsgrad=False)
OPTIMIZER = {0: nadam, 1: nadam, 2: "adam", 3: "adam", 4: nadam, 5: nadam, 6: nadam}

FIGURES_DIR = {0: "outputs/next_location_predict/figures/gru_enhaced_1location_original_10mil/",
               1: "outputs/next_location_predict/figures/gru_enhaced_1location_filled_10mil/",
               3: "outputs/next_location_predict/figures/gru_enhaced_grouped_users_routine/",
               4: "outputs/next_location_predict/figures/gru_enhaced_1location_output/",
               5: "outputs/next_location_predict/figures/gru_enhaced_1location_original/",
               6: "outputs/next_location_predict/figures/gru_enhaced_1location_filled/"}

VALIDATION_METRICS_NAMES = {0: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")],
                            1: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")],
                            3: [("loss", "val_loss" , "Loss"),
                                ("ma_activation_1_loss", "val_ma_activation_1_loss", "Loss multi-head attention"),
                                ("pe_activation_1_loss", "val_pe_activation_1_loss", "Loss positional encoding"),
                                ("ma_activation_1_acc", "val_ma_activation_1_acc", "Accuracy multi-head attention"),
                                ("pe_activation_1_acc", "val_pe_activation_1_acc", "Accuracy positional encoding")
                                ],
                            4: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")],
                            5: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")],
                            6: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")]}
