import pandas as pd

class CSVExtractor:

    def read(self, filename: str):
        df = pd.read_csv(filename,infer_datetime_format=True, encoding='utf-8')[
            ['installation_id', 'reference_date', 'latitude', 'longitude']]

        df.columns = ['id', 'datetime', 'latitude', 'longitude']
        df['datetime'] = pd.to_datetime(df['datetime'], infer_datetime_format=True)
        df['datetime'] = pd.to_datetime(df['datetime'], format='%Y-%m-%d %H:%M:%S')

        return df
        dados = {}  # dados de cada usuario

        ids = df['id'].unique().tolist()
        quantidade_amostras = len(df['latitude'].tolist())
        media_pontos_usuarios = quantidade_amostras / len(ids)
        cont = 0
        for i in ids:
            # if cont >= N_USUARIOS:
            #     break
            df2 = df.query("id==" + str(i) + "")
            d = df2.sort_values(by=['datetime'])
            dados[i] = d
            cont = cont + 1