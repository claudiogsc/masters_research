from configurations.identification_classification_config import JOB_CONFIG, JOB_CONFIG_INDEX
from models.identification_classification_models.identification import Identification
from models.identification_classification_models.proposta import Proposta
from domains.identification_classification_domain import IdentificationClassificationDomain
import copy


def location_id_to_name(location_id):
    class_integer = {0: "Casa", 1: "Outro", 2: "Outro"}
    return class_integer[location_id]


class IdentificationClassificationJob:

    def __init__(self):

        self.identification_classification_domain = IdentificationClassificationDomain()

    def start(self):

        data = self.identification_classification_domain\
            .extract_events_from_csv(JOB_CONFIG[JOB_CONFIG_INDEX]["filename"])
        poi_identification = Identification()
        usuarios = poi_identification.identify_points_of_interest(data)
        solution = Proposta(copy.deepcopy(usuarios))
        solution.classify_users_points_of_interest()
        # solution.plot_entropy(0.8, 0.08, 0.006)
        # solution.display(0.1)
        #solution.classified_users_pois_analysis()
        """solution.users_pois_to_dataframe("pois_1k_to_predict.csv")
        clf = load('classificadortreinado.joblib') 
        df = pd.read_csv('pois_10k_to_predict.csv')[['home_percentage_total_events', 'home_time_events', \
                                                     'work_percentage_total_events', 'work_time_events', \
                                                    'installation_id', 'poi_id', 'user_index']]
        x_predict = df[['home_percentage_total_events', 'home_time_events', 'work_percentage_total_events', 'work_time_events']].values.tolist()
        predicted = clf.predict(x_predict)
        predicted = [location_id_to_name(e) for e in predicted]
        df['class'] = pd.Series(predicted)
        df['poi_id'] = df['poi_id'].astype(np.int64)
        solution.calculate_users_locations_probabilities(df[['installation_id', 'poi_id', 'user_index', 'class']])"""
        #solution.solution_one_calculate_users_locations_probabilities()
        solution.calculate_users_routine_and_entropy()
        solution.calculate_sequence_of_visited_places(hours48=False, week_weekend=False, fill_sequence=False)
        #solution.solution_one_calculate_users_locations_probabilities_all()
        #solution.users_predicted_places_to_csv()
        #solution.users_home_entropy_to_csv_all("users_home_entropy_10k_all.csv")