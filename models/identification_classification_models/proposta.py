from configurations.identification_classification_config import JOB_CONFIG, JOB_CONFIG_INDEX, CIDADES_INFO_COLUMS, NEW_CIDADES_INFO_COLUMNS
from models.identification_classification_models.classification import *
from utils.identification_classification_util import kmeans_clustering, dbscan_clustering
import statistics as st
import pandas as pd
import geopandas as gpd

class Proposta(Classification):

    def __init__(self, users):
        super().__init__("Proposta", users)
        # self._usuarios_dias_trabalho = self.calcular_usuarios_dias_trabalho()
        self.mean_n_events = 0
        self.mean_n_events_week = 0
        self.mean_n_events_weekend = 0
        self.mean_work_events = 0
        self.mean_home_events = 0
        self.mean_work_week_events = 0
        self.mean_home_week_events = 0
        self.n_pois = 0
        self.ids = []

    def calculate_sequence_of_visited_places(self, clustering_algorithm="dbscan", hours48=False, week_weekend=False, fill_sequence=False):

        print("Gerando sequencia")
        users_sequence = []
        #new_users_ids_of_grouped_routine = self.calculate_users_routine_similarity(clustering_algorithm)
        ids = []
        ids_original = []
        ids_group = []
        users_entropy = []
        users_entropy_week = []
        users_entropy_weekend = []
        for i in range(len(self.users)):
            if self.users[i].id in self.ids:
                users_sequence.append(self.users[i].sequence_of_visited_locations(hours48=hours48, week_weekend=week_weekend, fill_sequence=fill_sequence))
                #ids_group.append(new_users_ids_of_grouped_routine[i])
                ids_group.append(0)
                ids.append(i)
                ids_original.append(self.users[i].id)
                users_entropy.append(self.users[i].user_entropy)
                users_entropy_week.append(self.users[i].user_entropy_week)
                users_entropy_weekend.append(self.users[i].user_entropy_weekend)

        df = pd.DataFrame({"location_sequence": users_sequence, "user_id": ids,
                           "user_id_original": ids_original, "user_group_id": ids_group,
                           "user_entropy": users_entropy, "user_entropy_week": users_entropy_week,
                           "user_entropy_weekend": users_entropy_weekend})

        if fill_sequence:
            dataset_type = "_filled"
        else:
            dataset_type = ""
        if hours48:
            hours = "_48hours"
        else:
            hours = "_24hours"

        name = hours + dataset_type + JOB_CONFIG[JOB_CONFIG_INDEX]["output_sufix"]
        #name = "_new_id_grouped_routine_" + clustering_algorithm + JOB_CONFIG[JOB_CONFIG_INDEX]["output_sufix"]
        print("antes escrever")
        df.to_csv("location_sequence" + name, index_label=False)

    def routine_performance_dataframe(self, df, metric):

        df_metric = self.calculate_users_routine_predict_performance()[metric]
        if df.shape[0] == 0:
            df = df.append(df_metric, ignore_index=True)
        else:
            # Usado para garantir que todos os resultados contenham apenas usuários em comum
            intersection = pd.Index(df['id'].unique().tolist()).intersection(pd.Index(df_metric['id'].unique().tolist())).tolist()
            df_metric = df_metric.query("id in " + str(intersection))
            df = df.query("id in " + str(intersection))
            df = df.append(df_metric, ignore_index=True)
        return df

    def rotulate_user_poi(self, id_):
        super().rotulate_user_poi()
        self.mean_n_events = 0
        self.mean_n_events_week = 0
        self.mean_n_events_weekend = 0
        self.mean_work_events = 0
        events_hours = [0] * 24
        pois = [[i, self.users[id_].pois[i]] for i in range(len(self.users[id_].pois))]
        pois_indexes = sorted(pois, key=lambda p: p[1].n_events, reverse=True)[
                       :5]  # Selecionar os top N pois com mais eventos
        pois_indexes = [poi_index[0] for poi_index in pois_indexes]

        self.n_pois = len(pois_indexes)
        # pois_indexes = [i for i in range(len(self.users[id_]))]
        # t = []
        for i in pois_indexes:  # definir se há intervalo de horário inativo
            times = self.users[id_].pois[i].times.times
            t = []
            for k in range(len(times)):
                d = times[k]
                if d.weekday() >= 5:
                    continue
                t.append(d)
            for k in range(len(t)):
                events_hours[t[k].hour] = events_hours[t[k].hour] + 1
        # print("horario: ", events_hours)
        inactive_user = self.inactive_interval(events_hours)

        cont_inactive_users = 0
        if inactive_user[0] != -1 and inactive_user[1] != -1:
            cont_inactive_users = 1

        # se nao foi possivel estabelecer o intervalo inativo, utiliza-se parametros fixos
        if inactive_user[0] < 0:
            home_hour = {}
            home_hour['start'] = 20
            home_hour['end'] = 8
            work_hour = {}
            work_hour['start'] = 10
            work_hour['end'] = 18
        else:
            # Obter as horas inativas do usuário
            self.users[id_].pois[i].inactive_hours = self.find_inactive_hours(inactive_user[0], inactive_user[1])
            home_hour = {}
            if (inactive_user[0] - 3) < 0:
                home_hour['start'] = 23 + (inactive_user[0] - 3)
            else:
                home_hour['start'] = inactive_user[0] - 3
            if (inactive_user[1] + 1) > 23:
                home_hour['end'] = inactive_user[1] + 1 - 23
            else:
                home_hour['end'] = inactive_user[1] + 1
            work_hour = {}
            if (inactive_user[0] - 3) < 0:
                work_hour['end'] = 23 + (inactive_user[0] - 3)
            else:
                work_hour['end'] = inactive_user[0] - 3
            if (inactive_user[1] + 3) > 23:
                work_hour['start'] = inactive_user[1] + 3 - 23
            else:
                work_hour['start'] = inactive_user[1] + 3

        # processa cada grupo do usuario de id id_
        for i in range(len(self.users[id_].pois)):
            self.users[id_].pois[i].set_has_inactive_interval()
            self.users[id_].pois[i].inactive_hours = self.find_inactive_hours(inactive_user[0], inactive_user[1])
            self.users[id_].inactive_hours = self.find_inactive_hours(inactive_user[0], inactive_user[1])
            self.users[id_].pois[i].home_time = home_hour
            self.users[id_].pois[i].work_time = work_hour
            self.mean_n_events = self.mean_n_events + self.users[id_].pois[i].n_events
            self.mean_work_events = self.mean_work_events + self.users[id_].pois[i].total_work_events
            self.mean_home_events = self.mean_home_events + self.users[id_].pois[i].total_home_events

        self.mean_n_events = self.mean_n_events / self.n_pois
        self.mean_work_events = self.mean_work_events / self.n_pois

        for i in range(len(self.users[id_].pois)):
            self.users[id_].pois[i].calculate_n_events_proposta()
            self.users[id_].pois[i].calculate_probabilities_home_work_events()

        return cont_inactive_users

    def find_inactive_hours(self, start, end):

        inactive_hours = []

        if start <= end:
            for i in range(start, end + 1):
                inactive_hours.append(i)
            return inactive_hours
        else:
            for i in range(start, 24):
                inactive_hours.append(i)
            for i in range(0, end + 1):
                inactive_hours.append(i)
            return inactive_hours

    def calculate_users_locations_probabilities(self, df):

        ids = df['user_index'].unique().tolist()
        users_average_entropy_week = []
        users_average_entropy_weekend = []
        users_median_entropy_week = []
        users_median_entropy_weekend = []
        users_p75_entropy_week = []
        users_p75_entropy_weekend = []
        users_p95_entropy_week = []
        users_p95_entropy_weekend = []
        for i in ids:
            i = int(i)
            user_df = df.query("user_index == " + str(i))
            pois_id = user_df['poi_id'].tolist()
            classes = user_df['class'].tolist()
            self.users[i].calculate_locations_probabilities(pois_id, classes)
            # users_average_entropy_week.append(self.users[i].average_entropy_hours_week)

            # Week

            average = self.users[i].average_entropy_hours_week
            median = self.users[i].median_entropy_hours_week
            p75 = self.users[i].p75_entropy_hours_week
            p95 = self.users[i].p95_entropy_hours_week
            if average >= 0:
                users_average_entropy_week.append(average)
            if median >= 0:
                users_median_entropy_week.append(median)
            if p75 >= 0:
                users_p75_entropy_week.append(p75)
            if p95 >= 0:
                users_p95_entropy_week.append(p95)

            # Weekend
            average = self.users[i].average_entropy_hours_weekend
            median = self.users[i].median_entropy_hours_weekend
            p75 = self.users[i].p75_entropy_hours_weekend
            p95 = self.users[i].p95_entropy_hours_weekend
            if average >= 0:
                users_average_entropy_weekend.append(average)
            if median >= 0:
                users_median_entropy_weekend.append(median)
            if p75 >= 0:
                users_p75_entropy_weekend.append(p75)
            if p95 >= 0:
                users_p95_entropy_weekend.append(p95)

        print("Media das medias das entropias dos usuarios week: ", st.mean(users_average_entropy_week))
        print("Media das medias das entropias dos usuarios weekend: ", st.mean(users_average_entropy_weekend))

        df_week = pd.DataFrame({'users_average_entropy_week': users_average_entropy_week,
                                'users_median_entropy_week': users_median_entropy_week,
                                'users_p75_entropy_week': users_p75_entropy_week,
                                'users_p95_entropy_week': users_p95_entropy_week})
        df_week.to_csv("entropy_metrics_week.csv", index_label=False)

        df_weekend = pd.DataFrame({'users_average_entropy_weekend': users_average_entropy_weekend,
                                   'users_median_entropy_weekend': users_median_entropy_weekend,
                                   'users_p75_entropy_weekend': users_p75_entropy_weekend,
                                   'users_p95_entropy_weekend': users_p95_entropy_weekend})
        df_weekend.to_csv("entropy_metrics_weekend.csv", index_label=False)

    def calculate_users_routine_and_entropy(self):

        """
            When user has home it is possible to correlate his home location with census data
        :return:
        """
        ids = []
        home_points = []
        for i in range(len(self.users)):
            if len(self.users[i].find_home_centroid()[0]) == 0:
                continue
            # A posição 1 contém o ponto
            home_points.append(self.users[i].find_home_centroid()[1])
            ids.append(self.users[i].id)

        """
            antigo
        
        :return: 
        """
        entropies = []
        entropies_week = []
        entropies_weekend = []
        plot_routine = False
        for i in range(len(self.users)):
            if self.users[i].id not in ids:
                continue
            self.users[i].calculate_user_routine_and_entropy(entropy_type="all_days")
            entropies.append(self.users[i].user_entropy)
            if i < 20:
                plot_routine = True
            else:
                plot_routine = False
            self.users[i].calculate_user_routine_and_entropy(entropy_type="week_weekend", plot_routine=plot_routine)
            entropies_week.append(self.users[i].user_entropy_week)
            entropies_weekend.append(self.users[i].user_entropy_weekend)

        serie = pd.Series(entropies)

        print("Users entropy: ")
        print(serie.describe())
        print("Users entropy week: ")
        print(pd.Series(entropies_week).describe())
        print("Users entropy weekend: ")
        print(pd.Series(entropies_weekend).describe())

        day_type = ["Dia de semana"]*len(entropies_week)
        day_type = day_type + ["Final de semana"]*len(entropies_weekend)
        entropies = entropies_week + entropies_weekend
        df = pd.DataFrame({"entropy": entropies, "day_type": day_type})
        df.to_csv("metrics_by_week_weekend_updated.csv")


        """
            novo
        """

        # Descobrir se a casa de cada usuário está em uma área urbana ou rural
        df = self.geocoding_into_urban_rural_areas(ids, home_points)

        type_urban_rural = []
        cd_geocodm_2 = []
        nm_municip_2 = []
        state_2 = []
        regions_types = df['Type'].tolist()
        nm_municip = df['NM_MUNICIP'].tolist()
        state = df['State'].tolist()
        cd_geocodm = df['CD_GEOCODM'].tolist()
        for i in range(len(regions_types)):
            """
                É preciso duplicar a lista porque a análise é feita para dias de semana e finais de semana.
                Então se o tipo é rural o dataset vai ter uma linha de rural para dia de semana e outra de rural
                para final de semana. O mesmo vale para a regĩao urbana. Por outro lado, os gráficos a serem
                gerados deve em algum momento separar dias de semana e finais de semana para evitar considerar dados
                duplicados.
            """
            type_urban_rural.append(regions_types[i])
            type_urban_rural.append(regions_types[i])
            cd_geocodm_2.append(cd_geocodm[i])
            cd_geocodm_2.append(cd_geocodm[i])
            nm_municip_2.append(nm_municip[i])
            nm_municip_2.append(nm_municip[i])
            state_2.append(state[i])
            state_2.append(state[i])

        types = []
        median_entropy = []
        average_entropy = []
        average_score = []
        median_score = []
        # ids dos usuários em que a geocodificação foi bem sucedida
        ids = df['id'].tolist()
        for i in range(len(self.users)):
            if self.users[i].id not in ids:
                continue
            types.append("Week")
            average_entropy.append(self.users[i].user_entropy_week)

            types.append("Weekend")
            average_entropy.append(self.users[i].user_entropy_weekend)

        df_metrics_by_type = pd.DataFrame({'NM_MUNICIP': nm_municip_2,
                                           'State': state_2,
                                           'Type': types,
                                           'Average entropy': average_entropy,
                                           'Region type': type_urban_rural,
                                           'CD_GEOCODM': cd_geocodm_2})

        df_metrics_by_type.to_csv(
            "entropy_by_week_weekend_regiontype" + JOB_CONFIG[JOB_CONFIG_INDEX]["output_sufix"], index_label=False)

        type_urban_rural = []
        cd_geocodm_2 = []
        average_entropy = []
        regions_types = df['Type'].tolist()
        for i in range(len(regions_types)):
            type_urban_rural.append(regions_types[i])
            cd_geocodm_2.append(cd_geocodm[i])


        for i in range(len(self.users)):
            if self.users[i].id not in ids:
                continue
            average_entropy.append(self.users[i].user_entropy)

        df_entropy_by_type = pd.DataFrame({'NM_MUNICIP': nm_municip,
                                           'State': state,
                                           'Average entropy': average_entropy,
                                           'Region type': type_urban_rural,
                                           'CD_GEOCODM': cd_geocodm_2})

        df_entropy_by_type.to_csv(
            "entropy_all_regiontype" + JOB_CONFIG[JOB_CONFIG_INDEX]["output_sufix"], index_label=False)

        self.ids = ids

    def calculate_users_routine_similarity(self, clustering_algorithm):

        users_flatten_weekend_routine = []
        users_flatten_weekday_routine = []
        users_flatten_routine = []
        users_originals_ids = []
        for i in range(len(self.users)):
            users_originals_ids.append(self.users[i].id)
            routine = self.users[i].locations_prob_days_hours
            weekday_routine = routine[0]
            for j in range(2, 5):
                weekday_routine = weekday_routine.add(routine[j], fill_value=0)

            weekday_routine = weekday_routine.apply(lambda e: e/5)
            home = weekday_routine.loc['Home'].tolist()
            other = weekday_routine.loc['Other'].tolist()
            displacement = weekday_routine.loc['Displacement'].tolist()
            user_flatten_weekday_routine = home + other + displacement

            weekend_routine = routine[5]
            weekend_routine = weekend_routine.add(routine[6], fill_value=0)

            weekend_routine = weekend_routine.apply(lambda e: e / 2)
            home = weekend_routine.loc['Home'].tolist()
            other = weekend_routine.loc['Other'].tolist()
            displacement = weekend_routine.loc['Displacement'].tolist()
            user_flatten_weekend_routine = home + other + displacement

            users_flatten_routine.append(user_flatten_weekday_routine + user_flatten_weekend_routine)

        min_samples = int(5)
        eps = 2
        if clustering_algorithm == "dbscan":
            print("eps: ", eps, "min_samples: ", min_samples)
            labels = dbscan_clustering(users_flatten_routine, min_samples, eps)
        elif clustering_algorithm == "kmeans":
            n_clusters = 100
            labels = kmeans_clustering(users_flatten_routine, n_clusters)
        else:
            print("Informe um algoritmo")
        n_clusters = max(labels)
        print("n_clusters: ", n_clusters, "rotulos: ", labels)
        users_news_ids = [i for i in range(len(labels))]
        for i in range(len(labels)):
            if labels[i] == -1:
                # If the user does'nt belong to any cluster, his cluster his own new id (original id + number of clusters_
                users_news_ids[i] = labels[i] + len(labels)

        return users_news_ids

    def solution_one_calculate_users_locations_probabilities(self):

        users_average_entropy_week = []
        users_average_entropy_weekend = []
        users_median_entropy_week = []
        users_median_entropy_weekend = []
        users_p75_entropy_week = []
        users_p75_entropy_weekend = []
        users_p95_entropy_week = []
        users_p95_entropy_weekend = []
        users_empty_hours_week = []
        users_empty_hours_weekend = []
        users_empty_hours_week_filled = []
        users_result_empty_hours_week = []
        users_result_empty_hours_weekend = []
        users_empty_hours_weekend_filled = []
        users_average_score_week = []
        users_average_score_weekend = []
        users_median_score_week = []
        users_median_score_weekend = []
        ids = []
        ids_week = []
        ids_weekend = []
        number_of_events_on_week = 0
        number_of_events_on_weekend = 0
        week_hours_list = [0]*24
        weekend_hours_list = [0]*24
        for i in range(len(self.users)):
            # Calcular métricas de cada usuário
            self.users[i].calculate_locations_probabilities()

            users_empty_hours_week.append(self.users[i].empty_hours_week)
            users_empty_hours_weekend.append(self.users[i].empty_hours_weekend)
            users_empty_hours_week_filled.append(self.users[i].empty_hours_week_filled)
            users_result_empty_hours_week.append(self.users[i].result_empty_hours_week)
            users_result_empty_hours_weekend.append(self.users[i].result_empty_hours_weekend)
            # users_average_entropy_week.append(self.users[i].average_entropy_hours_week)
            users_empty_hours_weekend_filled.append(self.users[i].empty_hours_weekend_filled)
            users_average_score_week.append(self.users[i].average_score_week)
            users_median_score_week.append(self.users[i].median_score_week)
            users_average_score_weekend.append(self.users[i].average_score_weekend)
            users_median_score_weekend.append(self.users[i].median_score_weekend)
            number_of_events_on_week_weekend = self.users[i].number_of_events_on_week_weekend()
            number_of_events_on_week = number_of_events_on_week + number_of_events_on_week_weekend[0]
            number_of_events_on_weekend = number_of_events_on_weekend + number_of_events_on_week_weekend[1]
            week_hours_list = [a + b for a, b in zip(week_hours_list, number_of_events_on_week_weekend[2])]
            weekend_hours_list = [a + b for a, b in zip(weekend_hours_list, number_of_events_on_week_weekend[3])]
            ids.append(self.users[i].id)

            # Week

            average = self.users[i].average_entropy_hours_week
            median = self.users[i].median_entropy_hours_week
            p75 = self.users[i].p75_entropy_hours_week
            p95 = self.users[i].p95_entropy_hours_week
            if average >= 0:
                users_average_entropy_week.append(average)
            if median >= 0:
                users_median_entropy_week.append(median)
            if p75 >= 0:
                users_p75_entropy_week.append(p75)
            if p95 >= 0:
                users_p95_entropy_week.append(p95)

            # Weekend
            average = self.users[i].average_entropy_hours_weekend
            median = self.users[i].median_entropy_hours_weekend
            p75 = self.users[i].p75_entropy_hours_weekend
            p95 = self.users[i].p95_entropy_hours_weekend
            if average >= 0:
                users_average_entropy_weekend.append(average)
            if median >= 0:
                users_median_entropy_weekend.append(median)
            if p75 >= 0:
                users_p75_entropy_weekend.append(p75)
            if p95 >= 0:
                users_p95_entropy_weekend.append(p95)

        # Exibir característica dos usuários apenas
        print("Media das medias das entropias dos usuarios week: ", st.mean(users_average_entropy_week))
        print("Media das medias das entropias dos usuarios weekend: ", st.mean(users_average_entropy_weekend))
        print("Mediana das medianas das entropias dos usuarios week: ", st.median(users_median_entropy_week))
        print("Mediana das medianas das entropias dos usuarios weekend: ", st.median(users_median_entropy_weekend))
        print("Media de horas vazias week: ", st.mean(users_empty_hours_week))
        print("Media de horas vazias week resolvidas: ", st.mean(users_empty_hours_week_filled))
        print("Media de horas vazias resultante week: ", st.mean(users_result_empty_hours_week))
        print("Media de horas vazias weekend: ", st.mean(users_empty_hours_weekend))
        print("Media de horas vazias weekend resolvidas: ", st.mean(users_empty_hours_weekend_filled))
        print("Media de horas vazias resultante weekend: ", st.mean(users_result_empty_hours_weekend))
        print("Quantidade de horários com eventos week:", number_of_events_on_week)
        print("Quantidade de horários com eventos weekend:", number_of_events_on_weekend)
        # print("Lista horários de eventos week: ", week_hours_list)
        # print("Lista horários de eventos weekend: ", weekend_hours_list)
        print("Media score (media entropia) week: ", st.mean(users_average_score_week))
        print("Media score (media entropia) weekend: ", st.mean(users_average_score_weekend))
        print("Mediana score (mediana entropia) week: ", st.median(users_median_score_week))
        print("Mediana score (mediana entropia) weekend: ", st.median(users_median_score_weekend))

        # Não está sendo utilizado
        # df_week = pd.DataFrame({'id': ids,
        #                         'users_average_entropy_week': users_average_entropy_week,
        #                         'users_median_entropy_week': users_median_entropy_week,
        #                         'users_p75_entropy_week': users_p75_entropy_week,
        #                         'users_p95_entropy_week': users_p95_entropy_week,
        #                         'users_average_score_week': users_average_score_week,
        #                         'users_median_score_week': users_median_score_week})
        # df_week.to_csv("entropy_metrics_week.csv", index_label=False)
        #
        # df_weekend = pd.DataFrame({'id': ids,
        #                            'users_average_entropy_weekend': users_average_entropy_weekend,
        #                            'users_median_entropy_weekend': users_median_entropy_weekend,
        #                            'users_p75_entropy_weekend': users_p75_entropy_weekend,
        #                            'users_p95_entropy_weekend': users_p95_entropy_weekend,
        #                             'users_average_score_weekend': users_average_score_weekend,
        #                             'users_median_score_weekend': users_median_score_weekend})
        # df_weekend.to_csv("entropy_metrics_weekend.csv", index_label=False)

        self.create_df_entropy_week_weekend()
        self.create_df_entropy_week_weekend_when_user_has_home()

    def create_df_entropy_week_weekend_when_user_has_home(self):
        """
            When user has home it is possible to correlate his home location with census data
        :return:
        """
        ids = []
        home_points = []
        for i in range(len(self.users)):
            if len(self.users[i].find_home_centroid()[0]) == 0:
                continue
            # A posição 1 contém o ponto
            home_points.append(self.users[i].find_home_centroid()[1])
            ids.append(self.users[i].id)

        # Descobrir se a casa de cada usuário está em uma área urbana ou rural
        df = self.geocoding_into_urban_rural_areas(ids, home_points)

        type_urban_rural = []
        cd_geocodm_2 = []
        types = df['Type'].tolist()
        cd_geocodm = df['CD_GEOCODM'].tolist()
        for i in range(len(types)):
            """
                É preciso duplicar a lista porque a análise é feita para dias de semana e finais de semana.
                Então se o tipo é rural o dataset vai ter uma linha de rural para dia de semana e outra de rural
                para final de semana. O mesmo vale para a regĩao urbana. Por outro lado, os gráficos a serem
                gerados deve em algum momento separar dias de semana e finais de semana para evitar considerar dados
                duplicados.
            """
            type_urban_rural.append(types[i])
            type_urban_rural.append(types[i])
            cd_geocodm_2.append(cd_geocodm[i])
            cd_geocodm_2.append(cd_geocodm[i])

        types = []
        median_entropy = []
        average_entropy = []
        average_score = []
        median_score = []
        # ids dos usuários em que a geocodificação foi bem sucedida
        ids = df['id'].tolist()
        for i in range(len(self.users)):
            if self.users[i].id not in ids:
                continue
            types.append("Week")
            median_score.append(self.users[i].median_score_week)
            average_score.append(self.users[i].average_score_week)
            median_entropy.append(self.users[i].median_entropy_hours_week)
            average_entropy.append(self.users[i].average_entropy_hours_week)

            types.append("Weekend")
            median_score.append(self.users[i].median_score_weekend)
            average_score.append(self.users[i].average_score_weekend)
            median_entropy.append(self.users[i].median_entropy_hours_weekend)
            average_entropy.append(self.users[i].average_entropy_hours_weekend)

        df_metrics_by_type = pd.DataFrame({'Type': types,
                                           'Median entropy': median_entropy,
                                           'Average entropy': average_entropy,
                                           'Average score': average_score,
                                           'Median score': median_score,
                                           'Region type': type_urban_rural,
                                           'CD_GEOCODM': cd_geocodm_2})

        df_metrics_by_type.to_csv("metrics_by_week_weekend_regiontype" + JOB_CONFIG[JOB_CONFIG_INDEX]["output_sufix"], index_label=False)

    def geocoding_into_urban_rural_areas(self, ids, points):

        gdf = gpd.read_file("/home/claudio/Documentos/census_brasil/census_brasil/census_brasil.shp",
                            encoding='ISO-8859-9')[['TIPO', 'ESTADO', 'CD_GEOCODI', 'CD_GEOCODM',
                                                    'NM_MUNICIP', 'geometry']]

        points = gpd.GeoDataFrame(data={'id': ids}, geometry=points, crs={'init': 'epsg:4674'})
        result = gpd.sjoin(points, gdf, how="inner", op='within')[['id', 'TIPO', 'ESTADO', 'CD_GEOCODI', 'CD_GEOCODM',
                                                    'NM_MUNICIP']]
        result.columns = ['id', 'Type', 'State', 'CD_GEOCODI', 'CD_GEOCODM','NM_MUNICIP']
        result = pd.DataFrame(result.values, columns=['id', 'Type', 'State', 'CD_GEOCODI', 'CD_GEOCODM','NM_MUNICIP'])
        result = result[['id', 'Type', 'CD_GEOCODI', 'CD_GEOCODM','NM_MUNICIP']]
        result['CD_GEOCODM'] = result['CD_GEOCODM'].astype('int64')
        df = pd.read_csv("/home/claudio/Documentos/ibge_datasets/cidades_info_2016.csv")[CIDADES_INFO_COLUMS]
        df.columns = NEW_CIDADES_INFO_COLUMNS
        df['State'] = df['State'].str.lower()
        result = df.join(result.set_index('CD_GEOCODM'), on='CD_GEOCODM', how='inner')
        print("Geocodificando: ", result.shape)


        return result

    def create_df_entropy_week_weekend(self):
        """
            Seleciona os valores das métricas para cada usuário
        :return:
        """
        types = []
        median_entropy = []
        average_entropy = []
        average_score = []
        median_score = []

        for i in range(len(self.users)):
            types.append("Week")
            median_score.append(self.users[i].median_score_week)
            average_score.append(self.users[i].average_score_week)
            median_entropy.append(self.users[i].median_entropy_hours_week)
            average_entropy.append(self.users[i].average_entropy_hours_week)

            types.append("Weekend")
            median_score.append(self.users[i].median_score_weekend)
            average_score.append(self.users[i].average_score_weekend)
            median_entropy.append(self.users[i].median_entropy_hours_weekend)
            average_entropy.append(self.users[i].average_entropy_hours_weekend)

        df_metrics_by_type = pd.DataFrame({'Type': types,
                                           'Median entropy': median_entropy,
                                           'Average entropy': average_entropy,
                                           'Average score': average_score,
                                           'Median score': median_score})

        df_metrics_by_type.to_csv("metrics_by_week_weekend" + JOB_CONFIG[JOB_CONFIG_INDEX]["output_sufix"], index_label=False)

    def solution_one_calculate_users_locations_probabilities_all(self):

        users_average_entropy = []
        users_median_entropy = []
        users_p75_entropy = []
        users_p95_entropy = []
        users_empty_hours = []
        users_empty_hours_filled = []
        users_result_empty_hours = []
        for i in range(len(self.users)):
            self.users[i].calculate_locations_probabilities_all()

            users_empty_hours.append(self.users[i].empty_hours)
            users_empty_hours_filled.append(self.users[i].empty_hours_filled)
            users_result_empty_hours.append(self.users[i].result_empty_hours)

            # Week

            average = self.users[i].average_entropy_hours
            median = self.users[i].median_entropy_hours
            p75 = self.users[i].p75_entropy_hours
            p95 = self.users[i].p95_entropy_hours
            if average >= 0:
                users_average_entropy.append(average)
            if median >= 0:
                users_median_entropy.append(median)
            if p75 >= 0:
                users_p75_entropy.append(p75)
            if p95 >= 0:
                users_p95_entropy.append(p95)

            # Weekend
            average = self.users[i].average_entropy_hours
            median = self.users[i].median_entropy_hours
            p75 = self.users[i].p75_entropy_hours
            p95 = self.users[i].p95_entropy_hours
            if average >= 0:
                users_average_entropy.append(average)
            if median >= 0:
                users_median_entropy.append(median)
            if p75 >= 0:
                users_p75_entropy.append(p75)
            if p95 >= 0:
                users_p95_entropy.append(p95)

        print("Media das medias das entropias dos usuarios: ", st.mean(users_average_entropy))
        print("Media de horas vazias: ", st.mean(users_empty_hours))
        print("Media de horas vazias resolvidas: ", st.mean(users_empty_hours_filled))
        print("Media de horas vazias resultante: ", st.mean(users_result_empty_hours))

        df_week = pd.DataFrame(
            {'users_average_entropy': users_average_entropy, 'users_median_entropy': users_median_entropy,
             'users_p75_entropy': users_p75_entropy, 'users_p95_entropy': users_p95_entropy})
        df_week.to_csv("entropy_metrics.csv", index_label=False)

    def users_predicted_places_to_csv(self):

        data = {str(i): [] for i in range(24)}
        data['user_id'] = []
        df_users_predicted_places = pd.DataFrame(data = data)
        for i in range(len(self.users)):
            df_users_predicted_places = df_users_predicted_places.append(self.users[i].user_predicted_places(), ignore_index = True)

        df_users_predicted_places.to_csv("users_predicted_places.csv", index_label = False)

    def users_home_entropy_to_csv(self, name):

        latitude_list = []
        longitude_list = []
        week_entropy_list = []
        weekend_entropy_list = []
        for i in range(len(self.users)):
            if self.users[i].has_home:
                longitude, latitude = self.users[i].pois[self.users[i].home_index].centroid
                week_entropy = self.users[i].average_entropy_hours_week
                weekend_entropy = self.users[i].average_entropy_hours_weekend
                latitude_list.append(float(latitude))
                longitude_list.append(float(longitude))
                week_entropy_list.append(float(week_entropy))
                weekend_entropy_list.append(float(weekend_entropy))
        df = pd.DataFrame(
            {"week_entropy": week_entropy_list, "weekend_entropy": weekend_entropy_list, "latitude": latitude_list,
             "longitude": longitude_list})
        df.to_csv(name, index_label=False)

    def users_home_entropy_to_csv_all(self, name):

        latitude_list = []
        longitude_list = []
        entropy_list = []
        for i in range(len(self.users)):
            if self.users[i].has_home:
                longitude, latitude = self.users[i].pois[self.users[i].home_index].centroid
                entropy = self.users[i].average_entropy_hours
                latitude_list.append(float(latitude))
                longitude_list.append(float(longitude))
                entropy_list.append(float(entropy))
        df = pd.DataFrame({"entropy": entropy_list, "latitude": latitude_list, "longitude": longitude_list})
        df.to_csv(name, index_label=False)

    def classified_users_pois_analysis(self):
        users = self.users
        mean_home_week_events = []
        mean_work_week_events = []
        mean_other_week_events = []
        mean_home_work_week_events = []
        mean_home_different_days = []
        mean_work_different_days = []
        mean_other_different_days = []

        for user in users:
            user.classified_user_pois_analysis()
            if user.mean_home_week_events != None:
                mean_home_week_events = mean_home_week_events + user.mean_home_week_events
            if user.mean_work_week_events != None:
                mean_work_week_events = mean_work_week_events + user.mean_work_week_events
            if user.mean_other_week_events != None:
                mean_other_week_events = mean_other_week_events + user.mean_other_week_events
            if user.home_different_days != None:
                mean_home_different_days = mean_home_different_days + user.home_different_days
            if user.work_different_days != None:
                mean_work_different_days = mean_work_different_days + user.work_different_days
            if user.other_different_days != None:
                mean_other_different_days = mean_other_different_days + user.other_different_days

        print("media eventos semanais casa: ", st.mean(mean_home_week_events))
        print("media eventos semanas trabalho: ", st.mean(mean_work_week_events))
        print("media eventos semanais outro: ", st.mean(mean_other_week_events))
        print("media casa trabalhos semanais eventos: ", mean_home_work_week_events)
        print("casa dias diferentes: ", st.mean(mean_home_different_days))
        print("trabalho dias diferentes: ", st.mean(mean_work_different_days))
        print("outro dias diferentes: ", st.mean(mean_other_different_days))

        # for i in range(len(mean_home_week_events)):
        # mean_home_work_week_events.append((mean_home_week_events[i], mean_work_week_events[i]))

    def user_pois_classification(self, id_):
        super().user_pois_classification()
        pois = self.users[id_].pois

        # seleciona o grupo com mais amostras nos intervalos de casa e trabalho
        max_home = 3
        max_work = 5
        max_home_index = -1
        max_work_index = -1
        max_events = 0
        home_indexes = []
        work_indexes = []
        home = None
        work = None
        pois = [[i, self.users[id_].pois[i]] for i in range(len(self.users[id_].pois))]
        pois_indexes = sorted(pois, key=lambda p: p[1].n_events, reverse=True)[
                       :5]  # Selecionar os top N pois com mais eventos
        pois_indexes = [poi_index[0] for poi_index in pois_indexes]

        for i in range(len(self.users[id_].pois)):
            if max_work_index != i:
                poi = self.users[id_].pois[i]
                # home_indexes.append(i)
                if poi.total_home_events >= 3:
                    home_indexes.append(i)
                    pass
                if poi.total_home_events > max_home:
                    max_home = poi.total_home_events
                    max_home_index = i
        if max_home_index > -1:
            self.users[id_].pois[max_home_index].poi_classified_class = "Casa"
            home = self.users[id_].pois[max_home_index]

        for i in range(len(self.users[id_].pois)):
            if max_home_index != i:
                # work_indexes.append(i)
                poi = self.users[id_].pois[i]
                if poi.total_work_events >= 5:
                    work_indexes.append(i)
                    pass
                if poi.total_work_events >= max_work:
                    max_work = poi.total_work_events
                    max_work_index = i
                    max_events = poi.n_events
        if max_work_index > -1 and max_work_index != max_home_index and self.users[id_].pois[
            max_work_index].different_days > 5:
            self.users[id_].pois[max_work_index].poi_classified_class = "Trabalho"
            work = self.users[id_].pois[max_work_index]

        if home == None and work != None:
            print("home time: ", self.users[id_].pois[max_work_index].home_time, "work time: ",
                  self.users[id_].pois[max_work_index].work_time)
            max_home = 3
            max_work = 17
            max_leisure = -1
            max_home_index = -1
            max_work_index = -1
            max_events = 0
            home_indexes = []
            work_indexes = []
            home = None
            work = None
            for i in range(len(self.users[id_].pois)):
                if max_work_index != i:
                    # home_indexes.append(i)
                    poi = self.users[id_].pois[i]
                    if poi.total_home_events >= 3:
                        home_indexes.append(i)
                        pass
                    if poi.total_work_events > max_home:
                        max_home = poi.total_work_events
                        max_home_index = i
            if max_home_index > -1:
                self.users[id_].pois[max_home_index].poi_classified_class = "Casa"
                home = self.users[id_].pois[max_home_index]

            for i in range(len(self.users[id_].pois)):
                if max_home_index != i:
                    # work_indexes.append(i)
                    poi = self.users[id_].pois[i]
                    if poi.total_work_events >= 5:
                        work_indexes.append(i)
                        pass
                    if poi.total_home_events >= max_work:
                        max_work = poi.total_home_events
                        max_work_index = i
                        max_events = poi.n_events
            if max_work_index > -1 and max_work_index != max_home_index:
                self.users[id_].pois[max_work_index].poi_classified_class = "Trabalho"
                work = self.users[id_].pois[max_work_index]
        self.users[id_].home_indexes = home_indexes
        self.users[id_].work_indexes = work_indexes
        # self.selected_pois_validation(home, work, leisure, id_)

    def calculate_users_different_days(self):
        for i in range(len(self.users)):
            self.users[i].calculate_different_days()

    def users_pois_to_dataframe(self, file=None):
        users = self.users

        df = pd.DataFrame({'installation_id': [], 'poi_id': [], 'user_index': [], 'home_percentage_total_events': [],
                           'work_percentage_total_events': [], 'percentage': [], 'total_points': [],
                           'home_time_events': [], 'work_time_events': [], 'different_days': [], 'period': [],
                           'classe': []})

        for i in range(len(users)):
            df_novo = users[i].user_pois_to_dataframe()
            df_novo['user_index'] = pd.Series([i] * df_novo.shape[0])
            df = df.append(df_novo, ignore_index=True)
        print("totaldf: ", df.shape)
        if file == None:
            file = "pois.csv"

        df.to_csv(file, index=False)

    def plot_entropy(self, percentil, max_home_entropy=-1, max_work_entropy=-1):
        entropy_home_correct = []
        entropy_home_wrong = []
        entropy_work_correct = []
        entropy_work_wrong = []

        for i in range(len(self.users)):
            self.users[i].calculate_total_home_work_events()
            self.users[i].calculate_home_entropy()
            correct = self.users[i].home_correct
            home_entropy = self.users[i].home_entropy
            if correct == True:
                entropy_home_correct.append(home_entropy)
            else:
                entropy_home_wrong.append(home_entropy)

            self.users[i].calculate_work_entropy()
            correct = self.users[i].work_correct
            work_entropy = self.users[i].work_entropy
            if correct == True:
                entropy_work_correct.append(work_entropy)
            else:
                entropy_work_wrong.append(work_entropy)
        # print(entropy_home_correct)
        # print(entropy_home_wrong)
        # print(entropy_work_correct)
        # print(entropy_work_wrong)
        entropy_home_correct_t = [(e, 0) for e in entropy_home_correct]
        entropy_home_wrong_t = [(e, 1) for e in entropy_home_wrong]
        entropy_home = entropy_home_correct_t + entropy_home_wrong_t
        entropy_work_correct_t = [(e, 0) for e in entropy_work_correct]
        entropy_work_wrong_t = [(e, 1) for e in entropy_home_wrong]
        entropy_work = entropy_work_correct_t + entropy_work_wrong_t
        entropy_home = sorted(entropy_home, key=lambda e: e[0])
        entropy_work = sorted(entropy_work, key=lambda e: e[0])
        # print("entropy home: ", entropy_home)
        t_home = len(entropy_home)
        t_work = len(entropy_work)
        percentil_home = int(t_home * percentil)
        percentil_work = int(t_work * percentil)

        count_home_correct = 0
        count_work_correct = 0
        i2 = 0
        print("-------------------------------------------------------------------")
        if max_home_entropy == -1 and max_work_entropy == -1:
            print("Plot entropy - percentil: ", percentil)
            for i in range(len(entropy_home)):
                if entropy_home[i][1] == 0:
                    count_home_correct = count_home_correct + 1
                if count_home_correct < int(percentil * i) and i > 0:
                    print("Casa: Para o índice: ", i, ", ", percentil * 100, "% corresponde a: ", percentil * i,
                          " acertos, Índice: ", i, " Quantidade de acertos: ", count_home_correct,
                          ", Quantidade de erros: ", i - count_home_correct, " Entropia maxima home: ",
                          entropy_home[i][0], " Entropia viável home: ", entropy_home[i2][0])
                    break
                else:
                    i2 = i
            i2 = 0
            for i in range(len(entropy_work)):
                if entropy_work[i][1] == 0:
                    count_work_correct = count_work_correct + 1
                if count_work_correct < int(percentil * i) and i > 0:
                    print("Trabalho: Para o índice: ", i, ", ", percentil * 100, "% corresponde a: ", percentil * i,
                          " acertos, Índice: ", i, " Quantidade de acertos: ", count_work_correct,
                          ", Quantidade de erros: ", i - count_work_correct, " Entropia maxima work: ",
                          entropy_work[i][0], " Entropia viável work: ", entropy_work[i2][0])
                    break
                else:
                    i2 = i
            print("p h: ", percentil_home)
            print("p w: ", percentil_work)
        else:
            print("Plot entropy - Max home entropy: ", max_home_entropy, " Max work entropy: ", max_work_entropy)
            for i in range(len(entropy_home)):
                if entropy_home[i][1] == 0 and entropy_home[i][0] <= max_home_entropy:
                    count_home_correct = count_home_correct + 1
                elif entropy_home[i][0] > max_home_entropy:
                    break
            print("Casas com entropias menores que " + str(max_home_entropy) + " : ", count_home_correct,
                  ", Essa quantidade corresponde a ", ((100 * count_home_correct) / len(entropy_home)),
                  "% do total de entropias de casa", " E corresponde a ", ((100 * count_home_correct) / (i - 1)),
                  "% do total de entropias menores que ", max_home_entropy)

            for i in range(len(entropy_work)):
                if entropy_work[i][1] == 0 and entropy_work[i][0] <= max_work_entropy:
                    count_work_correct = count_work_correct + 1
                elif entropy_work[i][0] > max_work_entropy:
                    break
            print("Trabalhos com entropias menores que " + str(max_work_entropy) + " : ", count_work_correct,
                  ", Essa quantidade corresponde a ", ((100 * count_work_correct) / len(entropy_work)),
                  "% do total de entropias de trabalho", " E corresponde a ", ((100 * count_work_correct) / (i - 1)),
                  "% do total de entropias menores que ", max_work_entropy)

        fig, axs = plt.subplots(1, 4, sharey=True)
        axs[0].set_title('Casa correto')
        axs[1].set_title('Casa errado')
        axs[2].set_title('Trabalho correto')
        axs[3].set_title('Trabalho errado')
        fig.tight_layout()
        sns.distplot(entropy_home_correct, ax=axs[0], kde=True, label="casa correto")
        sns.distplot(entropy_home_wrong, ax=axs[1], kde=True, label="casa errado")
        sns.distplot(entropy_work_correct, ax=axs[2], kde=True, label="trabalho correto")
        sns.distplot(entropy_work_wrong, ax=axs[3], kde=True, label="trabalho errado")
        fig.savefig("entropia_distplot.png", dpi=400)

        """fig2, axs2 = plt.subplots(1,4, sharey=True)
        fig2.tight_layout()
        sns.lineplot(x='usuario',y='entropia', data=pd.DataFrame({'usuario':[i for i in range(len(entropy_home_correct))], 'entropia':entropy_home_correct}), ax=axs2[0])
        sns.lineplot(x='usuario',y='entropia', data=pd.DataFrame({'usuario':[i for i in range(len(entropy_home_wrong))], 'entropia':entropy_home_wrong}), ax=axs2[1])
        sns.lineplot(x='usuario',y='entropia', data=pd.DataFrame({'usuario':[i for i in range(len(entropy_work_correct))], 'entropia':entropy_work_correct}), ax=axs2[2]) 
        sns.lineplot(x='usuario',y='entropia', data=pd.DataFrame({'usuario':[i for i in range(len(entropy_work_wrong))], 'entropia':entropy_work_wrong}), ax=axs2[3])  
        fig.savefig("entropia_lineplot.png", dpi=600)"""
        # sns.displot(entropy_home_correct, kde=False)]
        print("\n--------")
        print("Entropia:")
        print("Média casa correto: ", st.mean(entropy_home_correct), "média casa errado: ", st.mean(entropy_home_wrong))
        print("Mediana casa correto: ", st.median(entropy_home_correct), "mediana casa errado: ",
              st.median(entropy_home_wrong))
        print("Variancia populacional casa correto: ", st.pvariance(entropy_home_correct),
              "variancia populacional casa errado: ", st.pvariance(entropy_home_wrong))
        print("\n")
        print("Média trabalho correto: ", st.mean(entropy_work_correct), "média trabalho errado: ",
              st.mean(entropy_work_wrong))
        print("Mediana trabalho correto: ", st.median(entropy_work_correct), "mediana trabalho errado: ",
              st.median(entropy_work_wrong))
        print("Variancia populacional trabalho correto: ", st.pvariance(entropy_work_correct),
              "variancia populacional trabalho errado: ", st.pvariance(entropy_work_wrong))
        print("-------- \n")
        print("------------------------------------------------------------------- \n")

    def characterize_users_mobility(self, max_home_entropy, max_work_entropy):

        for i in range(len(self.users)):
            self.users[i].characterize_mobility(max_home_entropy, max_work_entropy)

    def calculate_metrics(self):
        for i in range(len(self.users)):
            self.user_pois_classification(i)
        super().calculate_metrics()

    def classify_users_points_of_interest(self):
        cont = 0
        for i in range(len(self.users)):
            cont = cont + self.users[i].classify_user_pois()
        #cont = super().classify_users_points_of_interest()
        print("Usuarios com intervalo inativo:", cont)

        #self.calculate_metrics()

    def inactive_interval(self, events_hours):
        events_hours = events_hours + events_hours
        start_hour = -1
        end_hour = -1
        t = 0
        interval = []
        for i in range(len(events_hours)):
            if events_hours[i] == 0:
                if start_hour == -1:
                    start_hour = i % 24
                    end_hour = i % 24
                    t = t + 1
                else:
                    end_hour = i % 24
                    t = t + 1
            else:
                if start_hour != -1:
                    interval.append((start_hour, end_hour, t))
                    start_hour = -1
                    end_hour = -1
                    t = 0
            if start_hour > 23 or end_hour > 23:
                print("Errou")
        interval.append((start_hour, end_hour, t))
        # if t==0:
        #    return -1,-1
        max_inactive_interval = max(interval, key=lambda x: x[2])
        start_hour_max = max_inactive_interval[0]
        end_hour_max = max_inactive_interval[1]
        lenght = max_inactive_interval[2]

        if lenght <= 1:
            return -1, -1

        return start_hour_max, end_hour_max