import math
import datetime as dt


class Events:

    def __init__(self, times):
        self.times = times

    def mean_week_frequency(self):
        times = self._times
        start = times[0]
        end = times[-1]
        elapsed = end - start
        weeks = math.ceil(elapsed.days / 7)
        week_events = [0] * weeks
        j = 1
        datetime_before = None
        for i in range(len(times)):
            datatempo = times[i]
            if datatempo < (start + dt.timedelta(days=7 * j)):
                if datatempo.day != datetime_before:
                    week_events[j] = week_events[j] + 1

            else:
                j = j + 1
                week_events[j] = week_events[j] + 1
            datetime_before = datatempo
        print("week events: ", week_events)