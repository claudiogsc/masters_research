#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import numpy as np
from sklearn.cluster import DBSCAN
from operator import itemgetter
from geopy.distance import distance as distance
from geopy.distance import lonlat
import sys
import statistics as sts
import datetime as dt
import pyproj
from functools import partial
import math


class Entropy:

    def __init__(self, probabilities):

        self.probabilities = probabilities
        self.entropy = self.calculate_entropy()

    def calculate_entropy(self):
        entropy = 0
        for p in self.probabilities:
            if p > 0:
                entropy = entropy - p * math.log(p, 2.7)
        """if entropy == -0:
            print("menos 0", self.probabilities)
        if entropy == 0:
            print("zero 0", self.probabilities)"""
        # print(-entropy)
        return entropy
