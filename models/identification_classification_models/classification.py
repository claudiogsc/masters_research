from abc import ABC, abstractmethod
import pandas as pd
from bokeh.plotting import show
from bokeh.models import ColumnDataSource, GMapOptions
from bokeh.plotting import gmap

id_s = 1790565


class Classification(ABC):

    def __init__(self, name, users, horario_trabalho=None, horario_casa=None):
        self.solution_name = name
        self.users_id = 0
        self.users = users  # list of lists of pois
        self.home_precision = 0
        self.work_precision = 0
        self.leisure_precision = 0
        self.total_homes = 0
        self.total_works = 0
        self.total_leisures = 0
        self.home_tp = 0
        self.work_tp = 0
        self.leisure_tp = 0
        self.home_fp = 0
        self.work_fp = 0
        self.leisure_fp = 0
        self.home_recall = 0
        self.work_recall = 0
        self.leisure_recall = 0
        self.home_fscore = 0
        self.work_fscore = 0
        self.leisure_fscore = 0
        # self._usuarios_dias_trabalho = self.calcular_usuarios_dias_trabalho()
        self.calculate_classes_total()
        self.home_was_work = 0
        self.home_was_other = 0
        self.work_was_home = 0
        self.work_was_other = 0
        self.entropy_home_correct = []
        self.entropy_home_wrong = []
        self.work_classified = 0
        self.work_classified_real_none = 0
        self.work_classified_none_real_none = 0

    @abstractmethod
    def rotulate_user_poi(self):
        pass

    @abstractmethod
    def user_pois_classification(self):
        pass

    def selected_pois_validation(self, home, work, leisure=None, id_=None):

        if self.users[id_].id not in [1789693, 1802162, 1789150, 1787325, 1789730, 1886698, 1860909, 1852239, 1842021,
                                      1839732, 1852239, 1808635, 1889918, 1879578, 1891267, 1836162, 1840812, 1836162,
                                      1834231, 1813167, 1791948, 1864121, 1861827, 1799897, 1879833, 1849572, 1853479,
                                      1835030, 1883882, 1851815, 1792604, 1814156] and not self.users[
            id_].not_has_home_and_work():
            # print("Usuário: ", str(self.users[id_].id), " não tem casa e trabalho")
            pass

        if home != None:
            if home.compare_poi_classes("Casa"):
                # print("casa : ", home.centroid, "home events: ", home.total_home_events, "work events: ", home.total_work_events)
                # print("Casa correto")
                self.add_home_tp()
                """if id_ != None:
                        probabilities = poi.probabilities()
                    self.users[id_].entropy_home_correct = entropy.Entropy(probabilities)"""
                # self.get_home_poi(id_)
                self.users[id_].home_correct = True
            else:
                if home.poi_real_class == "Trabalho":
                    self.home_was_work = self.home_was_work + 1
                else:
                    if self.users[id_].id not in [1798258, 1879833, 1829037, 1860821, 1814752, 1814156, 1846832,
                                                  1813167, 1828359, 1856937, 1799897, 1883882, 1861827, 1822505,
                                                  1851815]:
                        # print("casa was other id: ", self.users[id_].id,"casa errada: ", (home.centroid[1], home.centroid[0]), "home events: ", home.total_home_events, "work events: ", home.total_work_events)
                        self.home_was_other = self.home_was_other + 1
                """if id_ != None:
                    home.calculate_probabilities_home_work_events()
                    probabilities = home.probabilities()
                    self.users[id_].entropy_home_wrong = entropy.Entropy(probabilities)"""
                """if home != None:
                    print("id: ", self.users_id[id_],"casa errada: ", home.centroid, "home events: ", home.total_home_events, "work events: ", home.total_work_events)
                home = self.find_home(id_)
                if home != None:
                    print("id: ", self.users_id[id_],"casa correta: ", home.centroid, "home events: ", home.total_home_events, "work events: ", home.total_work_events)"""
                self.add_home_fp()

        if work != None:
            if work.compare_poi_classes("Trabalho"):
                # print("id: ", self.users_id[id_],"Trabalho correto")
                self.add_work_tp()
                """if id_ != None:
                    work.calculate_probabilities_home_work_events()
                    probabilities = work.probabilities()
                    self.users[id_].entropy_work_correct = entropy.Entropy(probabilities)"""
                # self.get_work_poi(id_)
                self.users[id_].work_correct = True
            else:
                if work.poi_real_class == "Casa" and self.users[id_].id not in [1789693, 1802162, 1789150, 1787325,
                                                                                1789730, 1886698, 1860909, 1852239,
                                                                                1842021, 1839732, 1852239, 1808635,
                                                                                1889918, 1879578, 1891267, 1836162,
                                                                                1840812, 1836162, 1834231, 1813167,
                                                                                1791948, 1864121, 1861827, 1799897,
                                                                                1879833, 1849572, 1853479]:
                    self.work_was_home = self.work_was_home + 1
                    """print("id: ", self.users_id[id_], "home events: ", work.total_home_events, "work events: ", work.total_work_events, " home time", work.home_time, " work time: ", work.work_time)
                    work = self.find_work(id_)
                    if work != None:
                        print("trabalho: ", work.centroid, "home events: ", work.total_home_events, "work events: ", work.total_work_events)"""
                else:
                    if self.users[id_].id not in [1798258, 1879833, 1829037, 1860821, 1814752, 1814156, 1846832,
                                                  1813167, 1828359, 1856937, 1799897, 1883882, 1861827, 1822505,
                                                  1851815, 1852485]:
                        # print("trabalho was other id: ", self.users[id_].id, "(", work.centroid[1], work.centroid[0], ")", "home events: ", work.total_home_events, "work events: ", work.total_work_events, " home time", work.home_time, " work time: ", work.work_time)
                        self.work_was_other = self.work_was_other + 1
                # self.get_work_poi(id_)
                self.add_work_fp()
                """if id_ != None:
                    work.calculate_probabilities_home_work_events()
                    probabilities = work.probabilities()
                    self.users[id_].entropy_work_wrong = entropy.Entropy(probabilities)"""
                # if self.users_id[id_] == id_s:
                # print("id: ", self.users_id[id_], "work centroid: ", work.centroid, "home events: ", work.total_home_events, "work events: ", work.total_work_events, " home time", work.home_time, " work time: ", work.work_time)
            if self.users[id_].real_work == False:
                self.work_classified_real_none = self.work_classified_real_none + 1
        else:
            if self.users[id_].real_work == False:
                self.work_classified_none_real_none = self.work_classified_none_real_none + 1

    def add_home_tp(self):
        self.home_tp = self.home_tp + 1

    def add_work_tp(self):
        self.work_tp = self.work_tp + 1

    def add_leisure_tp(self):
        self.leisure_tp = self.leisure_tp + 1

    def add_home_fp(self):
        self.home_fp = self.home_fp + 1

    def add_work_fp(self):
        self.work_fp = self.work_fp + 1

    def add_leisure_fp(self):
        self.leisure_fp = self.leisure_fp + 1

    @abstractmethod
    def classify_users_points_of_interest(self):
        cont = 0
        for i in range(len(self.users)):
            cont = cont + self.rotulate_user_poi(i)
        return cont
        # print("Usuarios com intervalo inativo:",cont)

    @abstractmethod
    def calculate_metrics(self):
        """for i in range(len(self.users)):
            self.user_pois_validation(i)"""
        print("Algoritmo: " + self.solution_name)
        print("Número de usuários processados: ", len(self.users))
        print("Acertos casa:", self.home_tp, "Total casa:", self.total_homes)
        print("Acertos trabalho:", self.work_tp, "Total trabalho:", self.total_works)
        print("Acertos lazer:", self.leisure_tp, "Total lazer:", self.total_leisures)
        if (self.home_tp + self.home_fp) > 0:
            self.home_precision = self.home_tp / (self.home_tp + self.home_fp)
        if (self.work_tp + self.work_fp) > 0:
            self.work_precision = self.work_tp / (self.work_tp + self.work_fp)
        if (self.leisure_tp + self.leisure_fp) > 0:
            self.leisure_precision = self.leisure_tp / (self.leisure_tp + self.leisure_fp)
        if self.total_homes > 0:
            self.home_recall = self.home_tp / (self.total_homes)
        if self.total_works > 0:
            self.work_recall = self.work_tp / (self.total_works)
        if self.total_leisures > 0:
            self.leisure_recall = self.leisure_tp / (self.total_leisures)
        if (self.home_precision + self.home_recall) > 0:
            self.home_fscore = (2 * self.home_precision * self.home_recall) / (self.home_precision + self.home_recall)
        if (self.work_precision + self.work_recall) > 0:
            self.work_fscore = (2 * self.work_precision * self.work_recall) / (self.work_precision + self.work_recall)
        if (self.leisure_precision + self.leisure_recall) > 0:
            self.leisure_fscore = (2 * self.leisure_precision * self.leisure_recall) / (
                        self.leisure_precision + self.leisure_recall)

    def display(self, dis):
        df = pd.DataFrame(
            data={"Distancia": [dis], "Casa:": [str(self.home_precision)], "Trabalho:": [str(self.work_precision)]})
        # df.to_csv("proposta.csv", mode="a", index=False)
        print("Precisao casa:", str(self.home_precision), "Revocacao casa:", str(self.home_recall), "Fscore casa:",
              str(self.home_fscore))
        print("Precisao trabalho:", str(self.work_precision), "Revocacao trabalho:", str(self.work_recall),
              "Fscore trabalho:", str(self.work_fscore))
        print("Número de pontos de interesse classificados como trabalho: ", (self.work_tp + self.work_fp))
        print("Número de pontos de interesse classificados como trabalho corretamente: ", self.work_tp)
        print("Número de usuários sem trabalho rotulado e que o algoritmo classificou como trabalho: ",
              self.work_classified_real_none)
        print("Número de usuários sem trabalho rotulado e que o algoritmo considerou que não tinha: ",
              self.work_classified_none_real_none)
        print("Precisao lazer:", str(self.leisure_precision), "Revocacao lazer:", str(self.leisure_precision),
              "Fscore lazer:", str(self.leisure_fscore))
        print("Considerou casa mas era trabalho: ", self.home_was_work, " Considerou trabalho mas era casa: ",
              self.work_was_home)
        print("Considerou casa mas era outro: ", self.home_was_other, "Considerou trabalho mas era outro: ",
              self.work_was_other)

    def calculate_classes_total(self):
        for i in range(len(self.users)):
            home = 0
            work = 0
            leisure = 0
            for j in range(len(self.users[i].pois)):
                if self.users[i].pois[j].poi_real_class == "Casa":
                    home = 1
                if self.users[i].pois[j].poi_real_class == "Trabalho":
                    work = 1
                if self.users[i].pois[j].poi_real_class == "Lazer":
                    leisure = 1

            self.add_total_homes(home)
            self.add_total_works(work)
            self.add_total_leisures(leisure)

    def add_total_homes(self, casa):
        self.total_homes = self.total_homes + casa

    def add_total_works(self, trabalho):
        self.total_works = self.total_works + trabalho

    def add_total_leisures(self, leisure):
        self.total_leisures = self.total_leisures + leisure

    def find_home(self, id_):
        home = None
        for poi in self.users[id_].pois:
            if poi.poi_real_class == "Casa":
                home = poi
                break
        return home

    def find_work(self, id_):
        work = None
        for poi in self.users[id_].pois:
            if poi.poi_real_class == "Trabalho":
                work = poi
                break
        return work

    def plot_classified_users_pois(self):

        correct = {}
        correct['Casa'] = 0
        correct['Trabalho'] = 0

        for user, user_id in zip(self.users, self.users_id):
            if user_id == id_s:
                print("\n")
                print("id:", user_id)
                real_class_latitude = {'home': []}
                real_class_longitude = {'home': []}
                real_class_longitude['work'] = []
                real_class_latitude['work'] = []
                classified_class_latitude = {'home': []}
                classified_class_latitude['work'] = []
                classified_class_longitude = {'work': []}
                classified_class_longitude['home'] = []
                poi_real_class = []
                clasified_class = []
                for poi in user:
                    poi_real_class = poi.poi_real_class
                    poi_classified_class = poi.poi_classified_class
                    if poi_real_class == poi_classified_class:
                        print("Correto:", poi_real_class)
                        correct[poi_classified_class] = correct[poi_classified_class] + 1
                    if poi_real_class == 'Casa':
                        real_class_latitude['home'].append(poi.latitude)
                        real_class_longitude['home'].append(poi.longitude + 0.0003)
                    elif poi_real_class == 'Trabalho':
                        real_class_latitude['work'].append(poi.latitude)
                        real_class_longitude['work'].append(poi.longitude + 0.0003)
                    if poi_classified_class == 'Casa':
                        classified_class_latitude['home'].append(poi.latitude)
                        classified_class_longitude['home'].append(poi.longitude)
                    elif poi_classified_class == 'Trabalho':
                        classified_class_latitude['work'].append(poi.latitude)
                        classified_class_longitude['work'].append(poi.longitude)
                output_file(
                    "/home/claudiocapanema/algoritmos-de-deteccao-de-pois/pontos-de-interesse/mestrado/mapas classificacao/" + str(
                        self.solution_name) + "/" + str(user_id) + ".html")

                map_options = GMapOptions(lat=poi.latitude, lng=poi.longitude, map_type="satellite", zoom=11)
                p = gmap("AIzaSyACuKxehD3slP-G7tCNb3bk9qXiwBAP_JE", map_options, title="Poi")

                source = ColumnDataSource(
                    data=dict(lat=real_class_latitude['home'],
                              lon=real_class_longitude['home'])
                )

                p.circle(x="lon", y="lat", size=15, line_color="blue", fill_alpha=0, source=source)

                source2 = ColumnDataSource(
                    data=dict(lat=classified_class_latitude['home'],
                              lon=classified_class_longitude['home'])
                )

                p.circle(x="lon", y="lat", size=12, line_color="red", fill_alpha=0, source=source2)

                source = ColumnDataSource(
                    data=dict(lat=real_class_latitude['work'],
                              lon=real_class_longitude['work'])
                )

                p.square(x="lon", y="lat", size=15, line_color="blue", fill_alpha=0, source=source)

                source2 = ColumnDataSource(
                    data=dict(lat=classified_class_latitude['work'],
                              lon=classified_class_longitude['work'])
                )

                p.square(x="lon", y="lat", size=12, line_color="red", fill_alpha=0, source=source2)

                show(p)

            # print("Acertos exatos:", correct)
