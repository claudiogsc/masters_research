from enum import Enum

class DayType(Enum):

    WEEK_DAY = 0
    WEEKEND = 1