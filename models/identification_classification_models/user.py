#!/usr/bin/env python
# coding: utf-8
import datetime as dt
import copy
import pandas as pd
from models.identification_classification_models import entropy
from utils.identification_classification_util import score, calculate_entropy
import math
import numpy as np
from models.identification_classification_models.rotina import Rotina
from geopy.distance import distance as distance
from geopy.distance import lonlat
from sklearn.metrics.pairwise import cosine_similarity, paired_euclidean_distances
from scipy.stats import entropy as scipy_entropy
import statistics as st
from configurations.identification_classification_config import DISTANCIA_MINIMA, LOCATION_ID
from models.identification_classification_models.day_type import DayType
import copy
from shapely.geometry import Point, MultiPoint
from shapely.ops import nearest_points
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.neighbors import RadiusNeighborsClassifier

SMOOTH_VALUE = 0.1
LOG = "printd"

class User:

    def __init__(self, id_, pois, coordenadas_deslocamento, tempos_deslocamento, la_lo, datetimes):
        self.id = id_
        self.pois = pois
        self.home_correct = False
        self.work_correct = False
        self.home_entropy = None
        # self.entropy_home_wrong = None
        self.work_entropy = None
        self.total_home_events = 0
        self.total_work_events = 0
        self.home_indexes = []
        self.work_indexes = []
        # self.entropy_work_wrong = None
        self.mean_home_events = []
        self.mean_work_events = []
        self.mean_other_week_events = []
        self.home_different_days = []
        self.work_different_days = []
        self.other_different_days = []
        self.displacement_times_week = [0] * 24
        self.displacement_times_weekend = [0] * 24
        self.entropy_hours_week = [-1] * 24
        self.average_entropy_hours_week = -1
        self.average_entropy_hours_weekend = -1
        self.entropy_hours_weekend = [-1] * 24
        self.median_entropy_hours_weekend = -1
        self.p75_entropy_hours_weekend = -1
        self.p95_entropy_hours_weekend = -1
        self.real_work = False
        self.empty_hours_week = 0
        self.empty_hours_weekend = 0
        self.empty_hours_week_filled = 0
        self.empty_hours_weekend_filled = 0
        self.empty_hours_filled = 0
        self.empty_hours_weekend = 0
        self.result_empty_hours_weekend = 0
        self.result_empty_hours_week = 0
        self.inactive_hours = []
        self.has_home = False
        self.home_index = -1

        self.empty_hours = 0
        self.result_empty_hours = 0
        self.entropy_hours = 0
        self.average_entropy_hours = 0
        self.median_entropy_hours = 0
        self.p75_entropy_hours = 0
        self.p95_entropy_hours = 0

        self.cosine_similarity = [0]*3
        self.mean_cosine_similarity = 0
        self.inactive_interval_lenght = 0

        # Formado pela media das entropias
        self.average_score_week = int
        self.average_score_weekend = int

        # Formado pela mediana das entropias
        self.median_score_week = int
        self.median_score_weekend = int

        self.steps = la_lo
        self.datetimes = datetimes

        self.displacement_times_days = {0: [0] * 24,
                                  1:[0] * 24,
                                  2:[0] * 24,
                                  3:[0] * 24,
                                  4:[0] * 24,
                                  5:[0] * 24,
                                  6:[0] * 24}
        self.user_entropy = int()
        self.locations_prob_days_hours = None
        # Days indexes in which it is possible to consider that user is at home. This is useful to fill sequences.
        self.days_to_fill_sequence = []

        self.rotina = Rotina()

        self.find_real_work()
        self.calculate_displacement_times(tempos_deslocamento)

    def new_row(self, event):
        event = copy.deepcopy(event)
        event[2] = str(event[2])
        columns = ["local", "hora", "datetime"]
        df = pd.DataFrame({columns[i]: [event[i]] for i in range(len(columns))})
        df['datetime'] = pd.to_datetime(df['datetime'])
        return df

    def fill_sequence(self, sequence, datetime_index):
        # To consider that user is at home during inactive's interval hours



        if self.has_home and len(self.inactive_hours) > 0 and len(self.days_to_fill_sequence) > 0:
            home = 0
            columns = ["local", "hora", "datetime"]
            new_trajetory = pd.DataFrame({columns[i]: [] for i in range(len(columns))})
            day = sequence[0][datetime_index].weekday()
            day_events = pd.DataFrame({columns[i]: [] for i in range(len(columns))})
            for i in range(len(sequence)):
                if sequence[i][datetime_index].weekday() == day:
                    day_events = day_events.append(self.new_row(sequence[i]), ignore_index=True)
                else:
                    date = day_events["datetime"][0].date()
                    if day_events["datetime"][0].weekday() < 5:
                        day_type = 0
                    else:
                        day_type = 1
                    columns = ["local", "hora", "datetime"]
                    new_day_datetimes = pd.DataFrame({columns[i]: [] for i in range(len(columns))})
                    for hour in self.inactive_hours:
                        new_day_datetimes = new_day_datetimes.append(self.new_row([home, hour,
                                                                                   dt.datetime(year=date.year, month=date.month, day=date.day, hour=hour)]), ignore_index=True)

                    day_events = day_events.append(new_day_datetimes, ignore_index=True)
                    day_events['datetime'] = pd.to_datetime(day_events['datetime'])
                    day_events = day_events.sort_values(by=['datetime'])
                    day_events['local'] = day_events['local'].astype('int64')
                    day_events['hora'] = day_events['hora'].astype('int64')
                    new_trajetory = new_trajetory.append(copy.deepcopy(day_events), ignore_index=True)

                    # Starts a new day
                    day = sequence[i][datetime_index].weekday()
                    day_events = pd.DataFrame({columns[i]: [] for i in range(len(columns))})
                    day_events = day_events.append(self.new_row(sequence[i]))

            new_trajetory = new_trajetory.values.tolist()

            for i in range(len(new_trajetory)):
                new_trajetory[i][0] = str(new_trajetory[i][0])
                new_trajetory[i][1] = int(new_trajetory[i][1])
                new_trajetory[i][2] = int(new_trajetory[i][2])
                new_trajetory[i] = tuple(new_trajetory[i])

            return new_trajetory

        else:
            new_trajetory = []

            for e in sequence:
                new_e = [str(e[2]), e[1], e[0]]
                new_trajetory.append(tuple(new_e))

            return new_trajetory




        # for i in range(len(new_trajetory)):
        #     e = new_trajetory[i]
        #     if type(e[datetime_index]) is str:
        #         if dt.datetime.strptime(e[datetime_index], "%Y-%m-%d %H:%M:%S").hour != e[1]:
        #             print("erro: ", e[datetime_index])
        #         pass
        #     elif type(e[datetime_index]) is int:
        #         if dt.datetime.fromtimestamp(int(e[datetime_index])).hour != e[1]:
        #             a = dt.datetime.fromtimestamp(int(e[datetime_index])).hour
        #             print("erro1: ", a, e[1], "Diferença: ", max([a,e[1]]) - min([a,e[1]]))
        #         e[datetime_index] = dt.datetime.fromtimestamp(int(e[datetime_index])).strftime("%Y-%m-%d %H:%M:%S")
        #     else:
        #         e[datetime_index] = str(e[datetime_index])
        #         #e[datetime_index] = dt.datetime.strptime(e[datetime_index], "%Y-%m-%d %H:%M:%S")
        #         if dt.datetime.strptime(e[datetime_index], "%Y-%m-%d %H:%M:%S").hour != e[1]:
        #             print("erro2: ", e[datetime_index])

    def find_inactive_hours(self, start, end):

        inactive_hours = []

        if start <= end:
            for i in range(start, end + 1):
                inactive_hours.append(i)
            return inactive_hours
        else:
            for i in range(start, 24):
                inactive_hours.append(i)
            for i in range(0, end + 1):
                inactive_hours.append(i)
            return inactive_hours

    def find_locations_time_spans(self):

        self.mean_n_events = 0
        self.mean_n_events_week = 0
        self.mean_n_events_weekend = 0
        self.mean_work_events = 0
        self.mean_home_events = 0
        events_hours = [0] * 24
        pois = [[i, self.pois[i]] for i in range(len(self.pois))]
        pois_indexes = sorted(pois, key=lambda p: p[1].n_events, reverse=True)[
                       :5]  # Selecionar os top N pois com mais eventos
        pois_indexes = [poi_index[0] for poi_index in pois_indexes]

        self.n_pois = len(pois_indexes)
        # pois_indexes = [i for i in range(len(self.users[id_]))]
        # t = []
        for i in pois_indexes:  # definir se há intervalo de horário inativo
            times = self.pois[i].times.times
            t = []
            for k in range(len(times)):
                d = times[k]
                if d.weekday() >= 5:
                    continue
                t.append(d)
            for k in range(len(t)):
                events_hours[t[k].hour] = events_hours[t[k].hour] + 1
        # print("horario: ", events_hours)
        inactive_user = self.inactive_interval(events_hours)

        has_inactive_interval = 0
        if inactive_user[0] != -1 and inactive_user[1] != -1:
            has_inactive_interval = 1

        # se nao foi possivel estabelecer o intervalo inativo, utiliza-se parametros fixos
        if inactive_user[0] < 0:
            home_hour = {}
            home_hour['start'] = 20
            home_hour['end'] = 8
            work_hour = {}
            work_hour['start'] = 10
            work_hour['end'] = 18
        else:
            # Obter as horas inativas do usuário
            inactive_hours = self.find_inactive_hours(inactive_user[0], inactive_user[1])
            home_hour = {}
            if (inactive_user[0] - 3) < 0:
                home_hour['start'] = 23 + (inactive_user[0] - 3)
            else:
                home_hour['start'] = inactive_user[0] - 3
            if (inactive_user[1] + 1) > 23:
                home_hour['end'] = inactive_user[1] + 1 - 23
            else:
                home_hour['end'] = inactive_user[1] + 1
            work_hour = {}
            if (inactive_user[0] - 3) < 0:
                work_hour['end'] = 23 + (inactive_user[0] - 3)
            else:
                work_hour['end'] = inactive_user[0] - 3
            if (inactive_user[1] + 3) > 23:
                work_hour['start'] = inactive_user[1] + 3 - 23
            else:
                work_hour['start'] = inactive_user[1] + 3

        for i in range(len(self.pois)):
            self.pois[i].set_has_inactive_interval()
            self.pois[i].inactive_hours = self.find_inactive_hours(inactive_user[0], inactive_user[1])
            self.inactive_hours = self.find_inactive_hours(inactive_user[0], inactive_user[1])
            self.pois[i].home_time = home_hour
            self.pois[i].work_time = work_hour
            self.mean_n_events = self.mean_n_events + self.pois[i].n_events
            self.mean_work_events = self.mean_work_events + self.pois[i].total_work_events
            self.mean_home_events = self.mean_home_events + self.pois[i].total_home_events

        self.mean_n_events = self.mean_n_events / self.n_pois
        self.mean_work_events = self.mean_work_events / self.n_pois

        for i in range(len(self.pois)):
            self.pois[i].calculate_n_events_proposta()
            self.pois[i].calculate_probabilities_home_work_events()

        return has_inactive_interval

    def set_user_pois_class(self):

        # seleciona o grupo com mais amostras nos intervalos de casa e trabalho
        max_home = 3
        max_work = 5
        max_home_index = -1
        max_work_index = -1
        max_events = 0
        home_indexes = []
        work_indexes = []
        home = None
        work = None
        pois = [[i, self.pois[i]] for i in range(len(self.pois))]
        pois_indexes = sorted(pois, key=lambda p: p[1].n_events, reverse=True)[
                       :5]  # Selecionar os top N pois com mais eventos
        pois_indexes = [poi_index[0] for poi_index in pois_indexes]

        for i in range(len(self.pois)):
            if max_work_index != i:
                poi = self.pois[i]
                # home_indexes.append(i)
                if poi.total_home_events >= 3:
                    home_indexes.append(i)
                    pass
                if poi.total_home_events > max_home:
                    max_home = poi.total_home_events
                    max_home_index = i
        if max_home_index > -1:
            self.pois[max_home_index].poi_classified_class = "Casa"
            home = self.pois[max_home_index]

        for i in range(len(self.pois)):
            if max_home_index != i:
                # work_indexes.append(i)
                poi = self.pois[i]
                if poi.total_work_events >= 5:
                    work_indexes.append(i)
                    pass
                if poi.total_work_events >= max_work:
                    max_work = poi.total_work_events
                    max_work_index = i
                    max_events = poi.n_events
        if max_work_index > -1 and max_work_index != max_home_index and self.pois[
            max_work_index].different_days > 5:
            self.pois[max_work_index].poi_classified_class = "Trabalho"
            work = self.pois[max_work_index]

        if home == None and work != None:
            print("home time: ", self.pois[max_work_index].home_time, "work time: ",
                  self.pois[max_work_index].work_time)
            max_home = 3
            max_work = 17
            max_leisure = -1
            max_home_index = -1
            max_work_index = -1
            max_events = 0
            home_indexes = []
            work_indexes = []
            home = None
            work = None
            for i in range(len(self.pois)):
                if max_work_index != i:
                    # home_indexes.append(i)
                    poi = self.pois[i]
                    if poi.total_home_events >= 3:
                        home_indexes.append(i)
                        pass
                    if poi.total_work_events > max_home:
                        max_home = poi.total_work_events
                        max_home_index = i
            if max_home_index > -1:
                self.pois[max_home_index].poi_classified_class = "Casa"
                home = self.pois[max_home_index]

            for i in range(len(self.pois)):
                if max_home_index != i:
                    # work_indexes.append(i)
                    poi = self.pois[i]
                    if poi.total_work_events >= 5:
                        work_indexes.append(i)
                        pass
                    if poi.total_home_events >= max_work:
                        max_work = poi.total_home_events
                        max_work_index = i
                        max_events = poi.n_events
            if max_work_index > -1 and max_work_index != max_home_index:
                self.pois[max_work_index].poi_classified_class = "Trabalho"
                work = self.pois[max_work_index]
        self.home_indexes = home_indexes
        self.work_indexes = work_indexes
        # self.selected_pois_validation(home, work, leisure, id_)

    def classify_user_pois(self):

        has_inactive_interval = self.find_locations_time_spans()

        self.set_user_pois_class()

        return has_inactive_interval


    def sequence_of_visited_locations(self, hours48, week_weekend, fill_sequence) -> list:

        sequence = []
        centroides = [[poi.centroid[1], poi.centroid[0]] for poi in self.pois]
        steps = [[step[0], step[1]] for step in self.steps]
        pois_classes = [poi.poi_classified_class for poi in self.pois]
        pois_types = [LOCATION_ID[poi_class] for poi_class in pois_classes]
        distancia = 0.03/6371.0088
        #clf = NearestCentroid(metric='haversine')
        clf = RadiusNeighborsClassifier(metric='haversine', radius=distancia, outlier_label=2)
        clf.fit(np.radians(centroides), pois_types)
        y_predict = clf.predict(np.radians(steps))
        for i in range(len(self.steps)):
            #poi, distancia = self.nearest_point(self.steps[i], self.pois, centroides)

            hour = self.datetimes[i].hour
            day_number = self.datetimes[i].weekday()
            datetime = self.datetimes[i]
            entropy = self.user_entropy
            day_type_category = int
            if day_number < 5:
                day_type = DayType.WEEK_DAY.value
                day_type_category = 0
            else:
                day_type = DayType.WEEKEND.value
                day_type_category = 1
                if hours48:
                    hour = 24 + hour

            type = y_predict[i]
            if fill_sequence:
                sequence.append([type, hour, datetime])
            else:
                sequence.append((str(datetime), hour, type))

        if fill_sequence:
            sequence = self.fill_sequence(sequence, 2)

        return sequence


    def number_of_events_on_week_weekend(self):

        week = 0
        weekend = 0

        week_list = self.displacement_times_week
        weekend_list = self.displacement_times_weekend

        for e in self.displacement_times_week:
            if e != 0:
                week = week + 1

        for e in self.displacement_times_weekend:
            if e != 0:
                weekend = weekend + 1

        for p in self.pois:
            number_week_weekend = p.number_of_events_on_week_weekend()
            week = week + number_week_weekend[0]
            weekend = weekend + number_week_weekend[1]
            week_list = [a + b for a, b in zip(week_list, number_week_weekend[2])]
            weekend_list = [a + b for a, b in zip(weekend_list, number_week_weekend[3])]

        return week, weekend, week_list, weekend_list

    def find_home_centroid(self) -> tuple:
        for p in self.pois:
            if p.poi_classified_class == 'Casa':
                home_point = Point(p.centroid[0], p.centroid[1])
                return p.centroid, home_point

        return ((),())

    def values_normalization(self, values: list, keep_flag: bool = False):

        if keep_flag:
            return values

        for i in range(len(values)):
            for j in range(len(values[i])):
                values[i][j] = (1-SMOOTH_VALUE)*values[i][j] + SMOOTH_VALUE*2.7

        return values

    def user_predicted_places(self):
        predicted_places = self.rotina.probabiities.loc[ 'Predicted_place' , : ].tolist()
        data = {str(i): [predicted_places[i]] for i in range(24)}
        data[str(self.id)] = self.id
        return pd.DataFrame(data)

    def nearest_point(self, point, pois, centroides):
        d = 1000000

        centroides = [(b, a) for a, b in centroides]
        nearest_point = [i for i in nearest_points(Point(point[1], point[0]), MultiPoint(centroides))][1].gkt
        d = distance(point, nearest_point).km*1000
        nearest = ()
        if d < DISTANCIA_MINIMA:
            for p in pois:
                if p.centroid == (nearest_point[1], nearest_point[0]):
                    nearest = p
                    break

        # for i in range(len(centroides)):
        #     #distancia = distance(point, lonlat(*centroides[i])).km*1000
        #     nearest_point = nearest_points(point, )
        #     if distancia < d:
        #         nearest = pois[i]
        #         d = distancia
        return nearest, d

    def value_to_percentage(self, value, total):

        if total > 0:
            return value/total
        else:
            return 0

    def inactive_interval(self, events_hours):

        events_hours = events_hours + events_hours

        if sum(events_hours) == 0:
            return -1, -1

        start_hour = -1
        end_hour = -1
        t = 0
        interval = []
        for i in range(len(events_hours)):
            if events_hours[i] == 0:
                if start_hour == -1:
                    start_hour = i % 24
                    end_hour = i % 24
                    t = t + 1
                else:
                    end_hour = i % 24
                    t = t + 1
            else:
                if start_hour != -1:
                    interval.append((start_hour, end_hour, t))
                    start_hour = -1
                    end_hour = -1
                    t = 0
            if start_hour > 23 or end_hour > 23:
                print("Errou")
        interval.append((start_hour, end_hour, t))
        # if t==0:
        #    return -1,-1
        max_inactive_interval = max(interval, key=lambda x: x[2])
        start_hour_max = max_inactive_interval[0]
        end_hour_max = max_inactive_interval[1]
        lenght = max_inactive_interval[2]

        if lenght <= 1:
            return -1, -1

        return start_hour_max, end_hour_max

    def calculate_routine_predict_performance(self):

        DISTANCIA_MINIMA = 20
        home = 0
        other = 0
        displacement = 0

        centroides = [poi.centroid for poi in self.pois]

        df_events = pd.DataFrame(data=[[0 for i in range(24)], [0 for i in range(24)], [0 for i in range(24)]], columns=[str(i) for i in range(24)],
                     index=['Home', 'Other', 'Displacement'])

        for i in range(len(self.la_lo_test)):
            poi, distancia = self.nearest_point(self.la_lo_test[i], self.pois, centroides)

            hour = self.datetimes_test[i].hour

            if distancia <= DISTANCIA_MINIMA:
                type = poi.poi_classified_class

                if type == "Casa":
                    df_events.loc['Home'][hour] = df_events.loc['Home'][hour] + 1
                elif type == "Outro":
                    df_events.loc['Other'][hour] = df_events.loc['Other'][hour] + 1
            else:
                df_events.loc['Displacement'][hour] = df_events.loc['Displacement'][hour] + 1

        home = df_events.loc['Home'].sum()
        other = df_events.loc['Other'].sum()
        disp = df_events.loc['Displacement'].sum()

        df_events.loc['Home'] = df_events.loc['Home'].apply(lambda e: self.value_to_percentage(e, home))
        df_events.loc['Other'] = df_events.loc['Other'].apply(lambda e: self.value_to_percentage(e, other))
        df_events.loc['Displacement'] = df_events.loc['Displacement'].apply(lambda e: self.value_to_percentage(e, disp))

        rotina = self.rotina.probabiities_list
        euclidian_distance = [0]*3
        rotina_normalized = self.values_normalization( self.rotina.probabiities_list)
        matrix_events = self.values_normalization(df_events.values.tolist())
        for i in range(3):
            self.cosine_similarity[i] = float(cosine_similarity([rotina_normalized[i]], [matrix_events[i]])[0])
            euclidian_distance[i] = float(paired_euclidean_distances([rotina_normalized[i]], [matrix_events[i]]))
            if sum(rotina_normalized[i]) == 0 or sum(matrix_events[i]) == 0:
                continue

        if LOG == "print":
            print("User: ", self.id)
            print("cosine similarity: ", self.cosine_similarity, ", Average: ", st.mean(self.cosine_similarity))
            print("euclidian distance: ", euclidian_distance, ", Average: ", st.mean(euclidian_distance))

        self.mean_cosine_similarity = st.mean(self.cosine_similarity)

    def calculate_home_entropy(self):
        probabilities = []
        home_events = []
        for i in self.home_indexes:
            if self.total_home_events > 0:
                home_events.append(self.pois[i].total_home_events)

        total_home_events = sum(home_events)
        for i in range(len(home_events)):
            probabilities.append(home_events[i] / total_home_events)
        self.home_entropy = entropy.Entropy(probabilities).entropy

    def calculate_work_entropy(self):
        probabilities = []
        work_events = []
        for i in self.work_indexes:
            if self.total_work_events > 0:
                work_events.append(self.pois[i].total_work_events)

        total_work_events = sum(work_events)
        for i in range(len(work_events)):
            probabilities.append(work_events[i] / total_work_events)
        self.work_entropy = entropy.Entropy(probabilities).entropy

    def calculate_total_home_work_events(self):

        for poi in self.pois:
            self.total_home_events = self.total_home_events + poi.total_home_events
            self.total_work_events = self.total_work_events + poi.total_work_events

    def characterize_mobility(self, max_home_entropy, max_work_entropy):

        if self.home_entropy <= max_home_entropy and self.work_entropy <= max_work_entropy:
            pass
        else:
            pass

    def set_pois_class_classifier(self, pois_id, classes):

        for i in range(len(pois_id)):
            self.pois[pois_id[i]].poi_classified_class = classes[i]

    def calculate_user_routine_and_entropy(self, entropy_type, plot_routine=False):

        has_home = False
        home_hours_events_days = {0: [0] * 24,
                                  1: [0] * 24,
                                  2: [0] * 24,
                                  3: [0] * 24,
                                  4: [0] * 24,
                                  5: [0] * 24,
                                  6: [0] * 24}
        work_hours_events_days = {0: [0] * 24,
                                  1: [0] * 24,
                                  2: [0] * 24,
                                  3: [0] * 24,
                                  4: [0] * 24,
                                  5: [0] * 24,
                                  6: [0] * 24}
        other_hours_events_days = {0: [0] * 24,
                                   1: [0] * 24,
                                   2: [0] * 24,
                                   3: [0] * 24,
                                   4: [0] * 24,
                                   5: [0] * 24,
                                   6: [0] * 24}
        total_hours_events_days = {0: [0] * 24,
                                   1: [0] * 24,
                                   2: [0] * 24,
                                   3: [0] * 24,
                                   4: [0] * 24,
                                   5: [0] * 24,
                                   6: [0] * 24}

        for i in range(len(self.pois)):
            # print("tipo: ", type(self.pois[i]),self.pois[i])
            poi_class = self.pois[i].poi_classified_class
            if poi_class == "Casa":
                home_hours_events_days = self.pois[i].days_n_events_hours
                has_home = True
                self.has_home = True
                self.home_index = i
                home = self.pois[i]

            elif poi_class == "Trabalho":
                work_hours_events_days = self.pois[i].days_n_events_hours
            else:
                days_n_events_hours = self.pois[i].days_n_events_hours
                # Sum other events by day and hour
                for i in range(7):
                    for j in range(24):
                        other_hours_events_days[i][j] = other_hours_events_days[i][j] + days_n_events_hours[i][j]

        # Sum other and work events by day and hour
        for i in range(7):
            for j in range(24):
                other_hours_events_days[i][j] = other_hours_events_days[i][j] + work_hours_events_days[i][j]

        # calculate total event for each day
        for i in range(7):
            for j in range(24):
                total_hours_events_days[i][j] = home_hours_events_days[i][j] \
                                                + other_hours_events_days[i][j] \
                                                + self.displacement_times_days[i][j]

        # Calculate location probabilities on each day
        locations_prob_days_hours = self.locations_probabilities_for_each_day(home_hours_events_days,
                                                                              other_hours_events_days,
                                                                              self.displacement_times_days,
                                                                              total_hours_events_days,
                                                                              has_home,
                                                                              entropy_type)

        # all_days considers all days of the week to get the respective entropy and calculate the median entropy
        if entropy_type == "all_days":
            self.user_entropy = self.calculate_entropy_all_days(locations_prob_days_hours, metric="mean")
        elif entropy_type == "week_weekend":
            self.user_entropy_week, self.user_entropy_weekend = self.calculate_entropy_week_weekend(locations_prob_days_hours, metric="mean")

        self.locations_prob_days_hours = locations_prob_days_hours

    def calculate_entropy_week_weekend(self, locations_prob_days_hours, metric="median"):

        entropies_week = []
        entropy_week = 0
        week_hours_filled = 0
        for i in range(5):
            fill_day = False
            for j in range(24):
                j = str(j)
                probs = locations_prob_days_hours[i][j].tolist()
                if sum(probs) == 0 and int(j) in self.inactive_hours:
                    probs[0] = 1
                    week_hours_filled = week_hours_filled + 1
                    fill_day = True
                entropy = calculate_entropy(probs)
                entropies_week.append(entropy)
            if fill_day:
                self.days_to_fill_sequence.append(i)
        if metric=="median":
            entropy_week = st.median(entropies_week)
        elif metric=="mean":
            entropy_week = st.mean(entropies_week)
        else:
            print("sem metrica")

        entropies_weekend = []
        entropy_weekend = 0
        weekend_hours_filled = 0
        for i in [5, 6]:
            empty_hours = []
            fill_day = True
            for j in range(24):
                j = str(j)
                probs = locations_prob_days_hours[i][j].tolist()
                if sum(probs) == 0:
                    empty_hours.append(int(j))
                entropy = calculate_entropy(probs)
                entropies_weekend.append(entropy)
            for empty_hour in empty_hours:
                if empty_hour not in self.inactive_hours:
                    fill_day = False
                    break

            if fill_day:
                probs = [1, 0, 0]
                for j in empty_hours:
                    entropy = calculate_entropy(probs)
                    entropies_weekend[j] = entropy

                self.days_to_fill_sequence.append(i)

        if metric == "median":
            entropy_weekend = st.median(entropies_weekend)
        elif metric == "mean":
            entropy_weekend = st.mean(entropies_weekend)
        else:
            print("sem metrica")
        # if entropy_weekend == entropy_week:
        #     print("IGUAL")
        # if entropy_weekend != entropy_week:
        #     print("DIFERENTE")

        return entropy_week, entropy_weekend

    def calculate_locations_probabilities(self, pois_id=None, classes=None):

        if pois_id != None and classes != None:
            self.set_pois_class_classifier(pois_id, classes)

        home_hours_events_week = [0] * 24
        work_hours_events_week = [0] * 24
        other_hours_events_week = [0] * 24
        total_hours_events_week = [0] * 24
        home_hours_events_weekend = [0] * 24
        work_hours_events_weekend = [0] * 24
        other_hours_events_weekend = [0] * 24
        total_hours_events_weekend = [0] * 24

        home_hours_prob_week = [0] * 24
        work_hours_prob_week = [0] * 24
        other_hours_prob_week = [0] * 24
        displacement_prob_week = [0] * 24
        home_hours_prob_weekend = [0] * 24
        work_hours_prob_weekend = [0] * 24
        other_hours_prob_weekend = [0] * 24
        displacement_prob_weekend = [0] * 24
        has_home = False
        home = None

        # Count events at each poi on each hour
        for i in range(len(self.pois)):
            poi_class = self.pois[i].poi_classified_class
            if poi_class == "Casa":
                home_hours_events_week = self.pois[i].week_n_events_day_hours
                home_hours_events_weekend = self.pois[i].weekend_n_events_day_hours
                home_hours_events_days = self.pois[i].days_n_events_hours
                has_home = True
                self.has_home = True
                self.home_index = i
                home = self.pois[i]

            elif poi_class == "Trabalho":
                work_hours_events_week = self.pois[i].week_n_events_day_hours
                work_hours_events_weekend = self.pois[i].weekend_n_events_day_hours
                work_hours_events_days = self.pois[i].days_n_events_hours
            else:
                week_events_hours = self.pois[i].week_n_events_day_hours
                other_hours_events_week = [other_hours_events_week[i] + week_events_hours[i] for i in range(24)]
                weekend_events_hours = self.pois[i].weekend_n_events_day_hours
                other_hours_events_weekend = [other_hours_events_weekend[i] + weekend_events_hours[i] for i in
                                              range(24)]

                other_hours_events_days = self.pois[i].days_n_events_hours

        other_hours_events_week = [other_hours_events_week[i] + work_hours_events_week[i] for i in range(24)]
        other_hours_events_weekend = [other_hours_events_weekend[i] + work_hours_events_weekend[i] for i in range(24)]

        for i in range(24):
            total_hours_events_week[i] = home_hours_events_week[i] + work_hours_events_week[i] + \
                                         other_hours_events_week[i] + self.displacement_times_week[i]
            total_hours_events_weekend[i] = home_hours_events_weekend[i] + work_hours_events_weekend[i] + \
                                            other_hours_events_weekend[i] + self.displacement_times_weekend[i]


        # Calculate probabilities
        for i in range(24):
            # Casa
            if total_hours_events_week[i] == 0:
                self.empty_hours_week = self.empty_hours_week + 1
                home_hours_prob_week[i] = 0
                if has_home:
                    # print(self.inactive_hours)
                    if len(self.inactive_hours) > 0:
                        if i in self.inactive_hours:
                            home_hours_prob_week[i] = 1
                            self.empty_hours_week_filled = self.empty_hours_week_filled + 1
                # work_hours_prob[i] = 0
                other_hours_prob_week[i] = 0
                displacement_prob_week[i] = 0
            else:
                home_hours_prob_week[i] = home_hours_events_week[i] / total_hours_events_week[i]
                # work_hours_prob[i] = work_hours_events[i]/total_hours_events[i]
                other_hours_prob_week[i] = other_hours_events_week[i] / total_hours_events_week[i]
                displacement_prob_week[i] = self.displacement_times_week[i] / total_hours_events_week[i]
            # Trabalho
            if total_hours_events_weekend[i] == 0:
                self.empty_hours_weekend = self.empty_hours_weekend + 1
                home_hours_prob_weekend[i] = 0
                # work_hours_prob[i] = 0
                # if has_home:
                #     print(self.inactive_hours)
                #     if len(self.inactive_hours) > 0:
                #         if i in self.inactive_hours:
                #             home_hours_prob_weekend[i] = 1
                #             self.empty_hours_weekend_filled = self.empty_hours_weekend_filled + 1
                #     pass
                other_hours_prob_weekend[i] = 0
                displacement_prob_weekend[i] = 0
            else:
                home_hours_prob_weekend[i] = home_hours_events_weekend[i] / total_hours_events_weekend[i]
                # work_hours_prob[i] = work_hours_events[i]/total_hours_events[i]
                other_hours_prob_weekend[i] = other_hours_events_weekend[i] / total_hours_events_weekend[i]
                displacement_prob_weekend[i] = self.displacement_times_weekend[i] / total_hours_events_weekend[i]

        # Se tem casa, tentar considerar que está em casa nos horários vazios caso seja igual ao intervalo de inatividade
        if has_home:
            home_hours_prob_weekend = self.fill_weekend_empty_hours(total_hours_events_weekend, home_hours_prob_weekend)

        entropy_hours_week = []
        for i in range(24):
            if home_hours_prob_week[i] == 0 and other_hours_prob_week[i] == 0 and displacement_prob_week[i] == 0:
                self.result_empty_hours_week = self.result_empty_hours_week + 1
                # entropia máxima quando não há eventos
                entropy = calculate_entropy([0.333, 0.333, 0.333])
            else:
                entropy =  calculate_entropy([home_hours_prob_week[i], other_hours_prob_week[i],
                                              displacement_prob_week[i]])
            #entropy = min_max(entropy, - 0.333 * math.log(0.333, ENTROPY_BASE) * 3)
            entropy_hours_week.append(entropy)

        self.entropy_hours_week = entropy_hours_week
        if len(self.entropy_hours_week) > 0:
            self.average_entropy_hours_week = st.mean(entropy_hours_week)
            self.median_entropy_hours_week = st.median(entropy_hours_week)
            self.p75_entropy_hours_week = np.percentile(entropy_hours_week, 75)
            self.p95_entropy_hours_week = np.percentile(entropy_hours_week, 95)
        else:
            print("week vazio")
            self.average_entropy_hours_week = -1
            self.median_entropy_hours_week = -1
            self.p75_entropy_hours_week = -1
            self.p95_entropy_hours_week = -1

        self.average_score_week = score(self.average_entropy_hours_week, self.result_empty_hours_week)
        self.median_score_week = score(self.median_entropy_hours_week, self.result_empty_hours_week)

        entropy_hours_weekend = []
        for i in range(24):
            if home_hours_prob_weekend[i] == 0 and other_hours_prob_weekend[i] == 0 and displacement_prob_weekend[
                i] == 0:
                self.result_empty_hours_weekend = self.result_empty_hours_weekend + 1
                # entropia máxima quando não há eventos
                entropy = calculate_entropy([0.333, 0.333, 0.333])
            else:
                entropy = calculate_entropy([home_hours_prob_weekend[i], other_hours_prob_weekend[i],
                                             displacement_prob_weekend[i]])
            #entropy = min_max(entropy, - 0.333 * math.log(0.333, ENTROPY_BASE)*3)
            entropy_hours_weekend.append(entropy)

        self.entropy_hours_weekend = entropy_hours_weekend
        if len(entropy_hours_weekend) > 0:
            self.average_entropy_hours_weekend = st.mean(entropy_hours_weekend)
            self.median_entropy_hours_weekend = np.percentile(entropy_hours_weekend, 50)
            self.p75_entropy_hours_weekend = np.percentile(entropy_hours_weekend, 75)
            self.p95_entropy_hours_weekend = np.percentile(entropy_hours_weekend, 95)
        else:
            print("weekend vazio")
            self.average_entropy_hours_weekend = -1
            self.median_entropy_hours_weekend = -1
            self.p75_entropy_hours_weekend = -1
            self.p95_entropy_hours_weekend = -1

        self.average_score_weekend = score(self.average_entropy_hours_weekend, self.result_empty_hours_weekend)
        self.median_score_weekend = score(self.median_entropy_hours_weekend, self.result_empty_hours_weekend)

    def calculate_entropy_all_days(self, locations_prob_days_hours, metric="mean"):

        entropies = []
        for i in range(7):
            for j in range(24):
                j = str(j)
                probs = locations_prob_days_hours[i][j].tolist()
                entropy = calculate_entropy(probs)
                entropies.append(entropy)
        if metric=="median":
            return st.median(entropies)
        elif metric=="mean":
            return st.mean(entropies)
        else:
            print("sem metrica")

        # entropies_week = []
        # entropy_week = 0
        # week_hours_filled = 0
        # for i in range(5):
        #     for j in range(24):
        #         j = str(j)
        #         probs = locations_prob_days_hours[i][j].tolist()
        #         if sum(probs) == 0 and int(j) in self.inactive_hours:
        #             probs[0] = 1
        #             week_hours_filled = week_hours_filled + 1
        #         entropy = calculate_entropy(probs)
        #         entropies_week.append(entropy)
        #
        # entropies_weekend = []
        # entropy_weekend = 0
        # weekend_hours_filled = 0
        # for i in [5, 6]:
        #     empty_hours = []
        #     fill = True
        #     for j in range(24):
        #         j = str(j)
        #         probs = locations_prob_days_hours[i][j].tolist()
        #         if sum(probs) == 0:
        #             empty_hours.append(int(j))
        #         entropy = calculate_entropy(probs)
        #         entropies_weekend.append(entropy)
        #     for empty_hour in empty_hours:
        #         if empty_hour not in self.inactive_hours:
        #             fill = False
        #             break
        #
        #     if fill:
        #         probs = [1, 0, 0]
        #         for j in empty_hours:
        #             entropy = calculate_entropy(probs)
        #             entropies_weekend[j] = entropy
        #             weekend_hours_filled = weekend_hours_filled + 1
        #
        # if metric == "median":
        #     entropy = st.median(entropies_week + entropies_weekend)
        # elif metric == "mean":
        #     entropy = st.mean(entropies_week + entropies_weekend)
        # else:
        #     print("sem metrica")
        #
        # return entropy

    def locations_probabilities_for_each_day(self,
                                             home_hours_events_days,
                                             other_hours_events_days,
                                             displacement_hours_events_days,
                                             total_hours_events_days,
                                             has_home,
                                             entropy_type):

        routine = pd.DataFrame(data = 0, columns = [str(i) for i in range(24)], index = ['Home', 'Other', 'Displacement'])
        locations_prob_days_hours = {0: routine,
                                     1: routine,
                                     2: routine,
                                     3: routine,
                                     4: routine,
                                     5: routine,
                                     6: routine}

        routines = []
        for i in range(7):
            routine = pd.DataFrame({str(i):[0]*3 for i in range(24)}, index=['Home', 'Other', 'Displacement'])
            home = [0]*24
            other = [0]*24
            displacement = [0]*24
            for j in range(24):
                if total_hours_events_days[i][j] != 0:
                    home[j] = home_hours_events_days[i][j]/total_hours_events_days[i][j]
                    other[j] = other_hours_events_days[i][j]/total_hours_events_days[i][j]
                    displacement[j] = displacement_hours_events_days[i][j]/total_hours_events_days[i][j]

            data = [home, other, displacement]
            routine = pd.DataFrame(data, columns=[str(k) for k in range(24)], index=['Home', 'Other', 'Displacement'])
            locations_prob_days_hours[i] = routine

        # Infer that user is at home on the inactive interval
        # if has_home == True and len(self.inactive_hours) > 0 and -1 not in self.inactive_hours:
        #     for i in range(5):
        #         for j in self.inactive_hours:
        #             j = str(j)
        #             if locations_prob_days_hours[i].loc['Home'][j] > 0\
        #                     or locations_prob_days_hours[i].loc['Other'][j] > 0\
        #                     or locations_prob_days_hours[i].loc['Displacement'][j] > 0:
        #                 print("erro serio")
        #                 if (locations_prob_days_hours[i].loc['Home'][j] +
        #                        locations_prob_days_hours[i].loc['Other'][j] +
        #                        locations_prob_days_hours[i].loc['Displacement'][j]) == 1:
        #                     print("igual")
        #                     print(locations_prob_days_hours[i].loc['Home'][j],
        #                           locations_prob_days_hours[i].loc['Other'][j],
        #                           locations_prob_days_hours[i].loc['Displacement'][j])
                    #locations_prob_days_hours[i].loc['Home'][j] = 1

        return locations_prob_days_hours


    def fill_weekend_empty_hours(self, total_hours_events_weekend, home_hours_prob_weekend):

        # Considerar que está em casa se não tiver eventos no intervalo de inatividade
        home_hours_prob_weekend_filled = copy.copy(home_hours_prob_weekend)
        empty_hours_weekend_filled = 0
        for hour in self.inactive_hours:
            if total_hours_events_weekend[hour] == 0:
                home_hours_prob_weekend_filled[hour] = 1
                empty_hours_weekend_filled = empty_hours_weekend_filled + 1
            else:
                return home_hours_prob_weekend

        self.empty_hours_weekend_filled = empty_hours_weekend_filled
        return home_hours_prob_weekend_filled

    def calculate_locations_probabilities_all(self, pois_id=None, classes=None):

        if pois_id != None and classes != None:
            self.set_pois_class_classifier(pois_id, classes)

        ENTROPY_BASE = 2.7
        home_hours_events = [0] * 24
        work_hours_events = [0] * 24
        other_hours_events = [0] * 24
        total_hours_events = [0] * 24

        home_hours_prob = [0] * 24
        work_hours_prob = [0] * 24
        other_hours_prob = [0] * 24
        displacement_prob = [0] * 24
        predicted_place = ['unknow'] * 24
        has_home = False
        home = None

        for i in range(len(self.pois)):
            # print("tipo: ", type(self.pois[i]),self.pois[i])
            poi_class = self.pois[i].poi_classified_class
            if poi_class == "Casa":
                home_hours_events = self.pois[i].n_events_day_hours
                has_home = True
                self.has_home = True
                self.home_index = i
                home = self.pois[i]

            elif poi_class == "Trabalho":
                work_hours_events = self.pois[i].n_events_day_hours
            else:
                events_hours = self.pois[i].n_events_day_hours
                other_hours_events = [other_hours_events[i] + events_hours[i] for i in range(24)]

        other_hours_events = [other_hours_events[i] + work_hours_events[i] for i in range(24)]

        for i in range(24):
            total_hours_events[i] = home_hours_events[i] + work_hours_events[i] + other_hours_events[i] + \
                                    self.displacement_times[i]

        for i in range(24):
            if total_hours_events[i] == 0:
                self.empty_hours = self.empty_hours + 1
                home_hours_prob[i] = 0
                if has_home:
                    # print(self.inactive_hours)
                    if len(self.inactive_hours) > 0:
                        if i in self.inactive_hours:
                            home_hours_prob[i] = 1
                            self.empty_hours_filled = self.empty_hours_filled + 1
                # work_hours_prob[i] = 0
                other_hours_prob[i] = 0
                displacement_prob[i] = 0
            else:
                home_hours_prob[i] = home_hours_events[i] / total_hours_events[i]
                # work_hours_prob[i] = work_hours_events[i]/total_hours_events[i]
                other_hours_prob[i] = other_hours_events[i] / total_hours_events[i]
                displacement_prob[i] = self.displacement_times[i] / total_hours_events[i]

                maximum = max([home_hours_prob[i], other_hours_prob[i],  displacement_prob[i]])
                if home_hours_prob[i] == maximum:
                    predicted_place[i] = 'home'
                elif other_hours_prob[i] == maximum:
                    predicted_place[i] = 'other'
                elif displacement_prob[i] == maximum:
                    predicted_place[i] = 'displacement'

        self.rotina.set_probabilities([home_hours_prob, other_hours_prob, displacement_prob, predicted_place])
        self.rotina.probabiities_list = [home_hours_prob, other_hours_prob, displacement_prob]
        #print(self.rotina.probabiities)

        entropy_hours = []
        for i in range(24):
            entropy = 0
            if home_hours_prob[i] == 0 and other_hours_prob[i] == 0 and displacement_prob[i] == 0:
                self.result_empty_hours = self.result_empty_hours + 1
                # entropy = entropy - 3*0.333*math.log(0.333, ENTROPY_BASE)
            else:
                for p in [home_hours_prob[i], other_hours_prob[i], displacement_prob[i]]:
                    if p > 0:
                        entropy = entropy - p * math.log(p, ENTROPY_BASE)
            entropy_hours.append(entropy)

        self.entropy_hours = entropy_hours
        self.average_entropy_hours = st.mean(entropy_hours)
        self.median_entropy_hours = st.median(entropy_hours)
        self.p75_entropy_hours = np.percentile(entropy_hours, 75)
        self.p95_entropy_hours = np.percentile(entropy_hours, 95)

    def calculate_displacement_times(self, displacement_times):
        times = [0] * 24
        for i in range(len(displacement_times)):
            datatempo = displacement_times[i]
            times[datatempo.hour] = times[datatempo.hour] + 1
            day_type = datatempo.weekday()
            self.displacement_times_days[day_type][datatempo.hour] \
                = self.displacement_times_days[day_type][datatempo.hour] + 1
            if datatempo.weekday() < 5:
                self.displacement_times_week[datatempo.hour] = self.displacement_times_week[datatempo.hour] + 1
            else:
                self.displacement_times_weekend[datatempo.hour] = self.displacement_times_weekend[datatempo.hour] + 1

        self.displacement_times = times

    def classified_user_pois_analysis(self):
        mean_home_week_events = []
        mean_work_week_events = []
        mean_other_week_events = []
        home_different_days = []
        work_different_days = []
        other_different_days = []
        for poi in self.pois:
            poi.mean_week_events()
            # poi.calculate_different_days()

        for poi in self.pois:
            if poi.poi_real_class == "Casa":
                mean_home_week_events.append(poi.mean_week_event)
                home_different_days.append(poi.different_days)
            if poi.poi_real_class == "Trabalho":
                mean_work_week_events.append(poi.mean_week_event)
                work_different_days.append(poi.different_days)
            if poi.poi_real_class == "Outro" or poi.poi_real_class == "Lazer":
                mean_other_week_events.append(poi.mean_week_event)
                other_different_days.append(poi.different_days)

        self.mean_home_week_events = mean_home_week_events
        self.mean_work_week_events = mean_work_week_events
        self.mean_other_week_events = mean_other_week_events
        self.home_different_days = home_different_days
        self.work_different_days = work_different_days
        self.other_different_days = other_different_days

    def calculate_different_days(self):
        for i in range(len(self.pois)):
            self.pois[i].calculate_different_days()

    def find_real_work(self):
        for poi in self.pois:
            if poi.compare_poi_classes("Trabalho"):
                self.real_work = True
                return

    def user_pois_to_dataframe(self):
        total_home_time_events = 0
        total_work_time_events = 0
        total_user_events = 0
        total_different_days = 0
        total_user_week_events = 0
        total_user_weekend_events = 0
        pois = self.pois

        for p in pois:
            total_home_time_events = total_home_time_events + p.total_home_events
            total_work_time_events = total_work_time_events + p.total_work_events
            total_user_events = total_user_events + p.total_points
            total_different_days = total_different_days + p.different_days
            total_user_week_events = total_user_week_events + p.week_events
            total_user_weekend_events = total_user_weekend_events + p.weekend_events
        n_pois = len(pois)
        percentage = [1 / n_pois]
        ids = [self.id] * n_pois
        pois_ids = [i for i in range(n_pois)]

        df = pd.DataFrame({'home_percentage_total_events': [], 'work_percentage_total_events': [], 'total_points': [],
                           'home_time_events': [], 'work_time_events': [], 'different_days': [], 'period': [],
                           'week_events': [], 'weekend_events': [], 'classe': []})
        for i in range(len(pois)):
            df = df.append(pois[i].to_dataframe(total_home_time_events, total_work_time_events, total_user_events,
                                                total_different_days, total_user_week_events,
                                                total_user_weekend_events), ignore_index=True)

        df['installation_id'] = pd.Series(ids)
        df['poi_id'] = pd.Series(pois_ids)

        df['percentage'] = pd.Series(percentage)
        # print("dfusuario: ", df.shape[0])
        return df

    def not_has_home_and_work(self):
        home = False
        work = False

        for p in self.pois:
            if p.poi_real_class == "Casa":
                home = True
            elif p.poi_real_class == "Trabalho":
                work = True

        if home == False and work == False:
            return False
        else:
            return True

