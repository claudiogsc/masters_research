from keras.layers import GRU, LSTM, CuDNNGRU, CuDNNLSTM, Activation, Dense, Masking, Dropout, SimpleRNN, Input, Lambda, \
    Flatten, Reshape
from keras.layers.merge import add,concatenate
from keras.layers.embeddings import Embedding
from keras.models import Model
from keras_multi_head import MultiHeadAttention
from keras_transformer.attention import MultiHeadSelfAttention
from keras_transformer.position import AddPositionalEncoding
from keras.regularizers import l1, l2

from configurations.next_location_predict_baseline_config import STEP_SIZE, N_USUARIOS, LOCATION_INPUT_DIM, TIME_INPUT_DIM
from models.base.neural_network_base_model import NNBase

class GRUBaselineFilled(NNBase):
    """
        It has two locations outputs
    """

    def __init__(self):
        super().__init__("GRU_baseline_filled")

    def build(self):
        s_input = Input((STEP_SIZE,), dtype='int32', name='spatial')
        t_input = Input((STEP_SIZE,), dtype='int32', name='temporal')
        id_input = Input((STEP_SIZE,), dtype='float32', name='id')

        # The embedding layer converts integer encoded vectors to the specified
        # shape (none, input_lenght, output_dim) with random weights, which are
        # ajusted during the training turning helpful to find correlations between words.
        # Moreover, when you are working with one-hot-encoding
        # and the vocabulary is huge, you got a sparse matrix which is not computationally efficient.
        gru_units = 9
        n = 2
        id_output_dim = (gru_units//8)*8 + 8*n - gru_units
        emb1 = Embedding(input_dim=LOCATION_INPUT_DIM, output_dim=10, input_length=STEP_SIZE)
        emb2 = Embedding(input_dim=TIME_INPUT_DIM, output_dim=15, input_length=STEP_SIZE)
        emb3 = Embedding(input_dim=5600, output_dim=gru_units, input_length=STEP_SIZE)

        spatial_embedding = emb1(s_input)
        temporal_embedding = emb2(t_input)
        id_embedding = emb3(id_input)

        concat_1 = concatenate(inputs=[spatial_embedding, temporal_embedding])
        print("concat_1: ", concat_1.shape)

        # Unlike LSTM, the GRU can find correlations between location/events
        # separated by longer times (bigger sentences)
        concat_1 = Dropout(0.5)(concat_1)
        gru_1 = GRU(gru_units, return_sequences=True)(concat_1)
        gru_1 = Dropout(0.5)(gru_1)
        print("gru_1: ", gru_1.shape, "id_embedding: ", id_embedding.shape)

        concat_2 = concatenate(inputs=[gru_1, id_embedding])
        print("concat_2: ", concat_2.shape)

        reshape_size = (gru_units + id_output_dim*2)*STEP_SIZE
        y_mhsa = self.mhsa(input=concat_2, id_embedding=id_embedding, reshape_size = reshape_size)
        y_pe = self.pe(input=concat_2, id_embedding=id_embedding, reshape_size = reshape_size)

        model = Model(inputs=[s_input, t_input, id_input], outputs=[y_mhsa, y_pe], name="GRU_baseline")

        return model

    def mhsa(self, input, id_embedding, reshape_size, numLocations=3):
        # att_layer = MultiHeadAttention(
        #     head_num=8,
        #     name='Multi-Head',
        # )(concat_2)
        att_layer = MultiHeadSelfAttention(
            num_heads=3,
            use_masking=False,
            name='Multi-Head-self-attention',
        )(input)
        print("att", att_layer.shape, "att id_embedding", id_embedding.shape)

        # att_layer = concatenate(inputs=[att_layer, id_embedding])
        # print("att concat_3: ", att_layer.shape)

        flatten_1 = Flatten(name="ma_flatten_1")(att_layer)

        drop_1 = Dropout(0.7, name="drop_1")(flatten_1)

        dense_1 = Dense(numLocations, name='dense_1')(drop_1)
        print("mhsa dense_1: ", dense_1.shape)
        y_mhsa = Activation('softmax', name='ma_activation_1')(dense_1)

        return y_mhsa

    def pe(self, input, id_embedding, reshape_size, numLocations=3):
        # att_layer = MultiHeadAttention(
        #     head_num=8,
        #     name='Multi-Head',
        # )(concat_2)

        pe_layer = AddPositionalEncoding()(input)
        print("pe_layer: ", pe_layer.shape)

        # pe_layer = concatenate(inputs=[pe_layer, id_embedding])
        # print("pe concat_3: ", pe_layer.shape)

        flatten_1 = Flatten(name="pe_flatten_1")(pe_layer)

        flatten_1 = Dropout(0.4, name="pe_drop_1")(flatten_1)

        dense_1 = Dense(numLocations, name='pe_dense_1')(flatten_1)
        print("pe dense_1: ", dense_1.shape)
        y_pe = Activation('softmax', name='pe_activation_1')(dense_1)

        return y_pe