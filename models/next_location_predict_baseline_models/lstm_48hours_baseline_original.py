from keras.layers import GRU, LSTM, CuDNNGRU, CuDNNLSTM, Activation, Dense, Masking, Dropout, SimpleRNN, Input, Lambda, \
    Flatten, Reshape
from keras.layers.merge import add,concatenate
from keras.layers.embeddings import Embedding
from keras.models import Model
from keras_multi_head import MultiHeadAttention
from keras_transformer.attention import MultiHeadSelfAttention
from keras_transformer.position import AddPositionalEncoding
from keras.regularizers import l1, l2

from configurations.next_location_predict_baseline_config import STEP_SIZE, N_USUARIOS, LOCATION_INPUT_DIM, TIME_INPUT_DIM
from models.base.neural_network_base_model import NNBase

class LSTM48HoursBaselineOriginal(NNBase):

    def __init__(self):
        super().__init__("LSTM_48_hours_baseline_original")

    def build(self):
        super().build()

        s_input = Input((STEP_SIZE,), dtype='int32', name='spatial')
        t_input = Input((STEP_SIZE,), dtype='int32', name='temporal')
        id_input = Input((STEP_SIZE,), dtype='float32', name='id')

        # The embedding layer converts integer encoded vectors to the specified
        # shape (none, input_lenght, output_dim) with random weights, which are
        # ajusted during the training turning helpful to find correlations between words.
        # Moreover, when you are working with one-hot-encoding
        # and the vocabulary is huge, you got a sparse matrix which is not computationally efficient.
        units = 15
        n = 1
        id_output_dim = (units//8)*8 + 8*n - units
        emb1 = Embedding(input_dim=LOCATION_INPUT_DIM, output_dim=5, input_length=STEP_SIZE)
        emb2 = Embedding(input_dim=48, output_dim=10, input_length=STEP_SIZE)
        emb3 = Embedding(input_dim=5600, output_dim=units, input_length=STEP_SIZE)

        spatial_embedding = emb1(s_input)
        temporal_embedding = emb2(t_input)
        id_embedding = emb3(id_input)

        id_embedding = Dropout(0.7)(id_embedding)

        concat_1 = concatenate(inputs=[spatial_embedding, temporal_embedding])
        print("concat_1: ", concat_1.shape)
        concat_1 = Dropout(0.6)(concat_1)
        # Unlike LSTM, the GRU can find correlations between location/events
        # separated by longer times (bigger sentences)
        lstm_1 = LSTM(units, return_sequences=True)(concat_1)
        print("lstm_1: ", lstm_1.shape, "id_embedding: ", id_embedding.shape)
        #lstm_1 = concatenate(inputs=[lstm_1, id_embedding])
        #lstm_1 = Dropout(0.6)(lstm_1)
        lstm_1 = add(inputs=[lstm_1, id_embedding])
        lstm_1 = Dropout(0.6)(lstm_1)
        flatten_1 = Flatten(name="ma_flatten_1")(lstm_1)
        dense_1 = Dense(LOCATION_INPUT_DIM)(flatten_1)
        print("dense_1: ", dense_1.shape)
        # out_vec_1 = add([dense_1, id_embedding])
        # print("out_vec_1: ", out_vec_1.shape)
        pred_location = Activation('softmax')(dense_1)

        model = Model(inputs=[s_input, t_input, id_input], outputs=[pred_location], name="lstm_48_hours_baseline")

        return model