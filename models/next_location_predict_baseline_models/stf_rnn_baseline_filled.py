from keras.layers import GRU, LSTM, CuDNNGRU, CuDNNLSTM, Activation, Dense, Masking, Dropout, SimpleRNN, Input, Lambda, \
    Flatten, Reshape
from keras.layers.merge import add,concatenate
from keras.layers.embeddings import Embedding
from keras.models import Model
from keras_multi_head import MultiHeadAttention
from keras_transformer.attention import MultiHeadSelfAttention
from keras_transformer.position import AddPositionalEncoding
from keras.regularizers import l1, l2

from configurations.next_location_predict_baseline_config import STEP_SIZE, N_USUARIOS, LOCATION_INPUT_DIM, TIME_INPUT_DIM
from models.base.neural_network_base_model import NNBase

class STFrnnBaselineFilled(NNBase):

    def __init__(self):
        super().__init__("STF_baseline_filled")

    def build(self):
        print("Modelo: STR-RNN with dropout")
        s_input = Input((STEP_SIZE,), dtype='int32', name='spatial')
        t_input = Input((STEP_SIZE,), dtype='int32', name='temporal')
        id_input = Input((STEP_SIZE,), dtype='float32', name='id')

        # The embedding layer converts integer encoded vectors to the specified
        # shape (none, input_lenght, output_dim) with random weights, which are
        # ajusted during the training turning helpful to find correlations between words.
        # Moreover, when you are working with one-hot-encoding
        # and the vocabulary is huge, you got a sparse matrix which is not computationally efficient.
        simple_rnn_units = 30
        n = 2
        id_output_dim = (simple_rnn_units//8)*8 + 8*n - simple_rnn_units
        emb1 = Embedding(input_dim=LOCATION_INPUT_DIM, output_dim=5, input_length=STEP_SIZE)
        emb2 = Embedding(input_dim=TIME_INPUT_DIM, output_dim=10, input_length=STEP_SIZE)
        emb3 = Embedding(input_dim=N_USUARIOS, output_dim=2, input_length=STEP_SIZE)

        spatial_embedding = emb1(s_input)
        temporal_embedding = emb2(t_input)
        id_embedding = emb3(id_input)

        concat_1 = concatenate(inputs=[spatial_embedding, temporal_embedding])
        concat_1 = Dropout(0.7)(concat_1)
        print("concat_1: ", concat_1.shape)

        # Unlike LSTM, the GRU can find correlations between location/events
        # separated by longer times (bigger sentences)
        srnn = SimpleRNN(simple_rnn_units)(concat_1)
        drop_1 = Dropout(0.6)(srnn)
        y_srnn = Dense(LOCATION_INPUT_DIM, activation='softmax')(drop_1)



        model = Model(inputs=[s_input, t_input, id_input], outputs=[y_srnn], name="STF_RNN_baseline")

        return model

