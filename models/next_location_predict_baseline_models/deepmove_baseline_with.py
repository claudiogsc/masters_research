from keras.layers import GRU, LSTM, CuDNNGRU, CuDNNLSTM, Activation, Dense, Masking, Dropout, SimpleRNN, Input, Lambda, \
    Flatten, Reshape
from keras.layers.merge import add,concatenate, average
from keras.layers.embeddings import Embedding
from keras.models import Model
from keras_multi_head import MultiHeadAttention
from keras_transformer.attention import MultiHeadSelfAttention
from keras_transformer.position import AddPositionalEncoding
from keras.regularizers import l1, l2
from keras_self_attention import SeqSelfAttention

from configurations.next_location_predict_baseline_config import STEP_SIZE, N_USUARIOS, LOCATION_INPUT_DIM, TIME_INPUT_DIM
from models.base.neural_network_base_model import NNBase

class DeepMoveBaseline(NNBase):

    def __init__(self):
        super().__init__("Deep_move_baseline")

    def build(self):
        super().build()

        input_size = 9
        s_input = Input(shape=(input_size,), dtype='int32', name='spatial')
        t_input = Input(shape=(input_size,), dtype='int32', name='temporal')
        id_input = Input(shape=(input_size,), dtype='float32', name='id')

        s_input_current = Input((input_size,), dtype='int32', name='spatial_current')
        t_input_current = Input((input_size,), dtype='int32', name='temporal_current')
        id_input_current = Input((input_size,), dtype='float32', name='id_current')

        # The embedding layer converts integer encoded vectors to the specified
        # shape (none, input_lenght, output_dim) with random weights, which are
        # ajusted during the training turning helpful to find correlations between words.
        # Moreover, when you are working with one-hot-encoding
        # and the vocabulary is huge, you got a sparse matrix which is not computationally efficient.
        units = 30
        gru_units = 30
        n = 1
        id_output_dim = (units//8)*8 + 8*n - units
        history_input = 24
        emb1 = Embedding(input_dim=LOCATION_INPUT_DIM+1, output_dim=5, mask_zero=True)
        emb2 = Embedding(input_dim=24, output_dim=10, mask_zero=True)
        emb3 = Embedding(input_dim=970, output_dim=2, mask_zero=True)

        emb4 = Embedding(input_dim=LOCATION_INPUT_DIM, output_dim=5, mask_zero=True)
        emb5 = Embedding(input_dim=24, output_dim=10, mask_zero=True)
        emb6 = Embedding(input_dim=970, output_dim=2, mask_zero=True)

        spatial_embedding = emb1(s_input)
        temporal_embedding = emb2(t_input)
        id_embedding = emb3(id_input)

        spatial_embedding_current = emb4(s_input_current)
        temporal_embedding_current = emb5(t_input_current)
        id_embedding_current = emb6(id_input_current)

        print("spatial current: ", spatial_embedding_current.shape, " temporal current: ",
              temporal_embedding_current.shape, " ids current: ", id_embedding_current.shape)
        concat_1_current = concatenate(inputs=[spatial_embedding_current, temporal_embedding_current])
        #concat_1_current = concatenate(inputs=[concat_1_current, id_input_current])

        gru, gru_last = GRU(gru_units, return_sequences=True, return_state=True)(concat_1_current)
        print("gru shape: ", gru.shape, " gru last: ", gru_last.shape)

        concat_1 = concatenate(inputs=[spatial_embedding, temporal_embedding])
        print("concat_1: ", concat_1.shape)

        hma = self.hma(concat_1, gru)

        concat_2 = concatenate(inputs=[hma, gru])
        concat_2 = concatenate(inputs=[concat_2, id_embedding_current])

        #flatten_1 = Flatten(name="ma_flatten_1")(concat_2)
        dense_1 = Dense(LOCATION_INPUT_DIM)(concat_2)
        print("dense_1: ", dense_1.shape)
        pred_location = Activation('softmax')(dense_1)

        model = Model(inputs=[s_input, t_input, id_input, s_input_current, t_input_current, id_input_current], outputs=[pred_location], name="lstm_48_hours_baseline")

        return model

    def hma(self, input, gru):

        print("hma input: ", input.shape)
        #avg = average([flatten, flatten])
        #input = Flatten()(input)
        #input = Reshape((1440,))(input)

        dense = Dense(4)(input)

        print("hma dense: ", dense.shape)

        # print("hma dense: ", dense.shape)
        #dense = Reshape((48,))(dense)
        print("Flatten dense: ", dense.shape)

        #gru = Reshape((1,30))(gru)
        #gru = Flatten()(gru)
        print("Flatten: ", gru.shape)
        #gru = Reshape((120,))(gru)


        concat = concatenate(name="concat_hma", inputs=[dense, gru], axis=-1)
        print("concat hma: ", concat.shape)

        att = SeqSelfAttention(attention_width=15,
                               attention_activation='sigmoid',
                               name='Attention')(concat)

        return att
