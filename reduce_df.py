import pandas as pd
import statistics as st


df = pd.read_csv('df_mais_de_1_mil_limite_500_pontos.csv')

print(df.columns)

ids = df['installation_id'].unique().tolist()
df['reference_date'] = pd.to_datetime(df['reference_date'], infer_datetime_format=True)
cont = 0
periodos = []
df_new = None
for id_ in ids:

    user = df.query('installation_id ==' + str(id_))
    user = user.sort_values(by=['reference_date']).head(250)
    if df_new is None:
        df_new = user
    else:
        df_new = pd.concat([df_new, user], ignore_index=True)
    dates = df['reference_date'].describe()
    periodo = dates['last'] - dates['first']
    periodos.append(periodo.days)
    if cont < 2:
        pass

    cont = cont + 1

s = pd.Series(periodos)

print("media: ", st.mean(periodos))

df_new.to_csv('df_mais_de_1_mil_limite_250_pontos.csv', index_label=False)