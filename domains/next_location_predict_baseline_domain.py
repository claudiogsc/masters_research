import copy
import ast
import numpy as np
import datetime as dt
import pandas as pd
from sklearn.model_selection import KFold

from models.next_location_predict_baseline_models.gru_baseline_original import GRUBaselineOriginal
from models.next_location_predict_baseline_models.lstm_48hours_baseline_original import LSTM48HoursBaselineOriginal
from models.next_location_predict_baseline_models.gru_baseline_filled import GRUBaselineFilled
from models.next_location_predict_baseline_models.stf_rnn_baseline_original import STFrnnBaselineOriginal
from models.next_location_predict_baseline_models.lstm_48hours_baseline_filled import LSTM48HoursBaselineFilled
from models.next_location_predict_baseline_models.stf_rnn_baseline_filled import STFrnnBaselineFilled
from models.next_location_predict_baseline_models.deepmove_baseline_with import DeepMoveBaseline
from models.next_location_predict_baseline_models.map_filled import MAPBaselineFilled
from models.next_location_predict_baseline_models.map_original import MAPBaselineOriginal
from models.next_location_predict_baseline_models.mhsa_baseline import MHSABaselineWithDropout
from keras.utils import np_utils
from sklearn.preprocessing import OneHotEncoder
import sklearn.metrics as skm
from utils.next_location_predict_util import sequence_to_x_y, remove_hour_from_sequence_x, \
    remove_hour_from_sequence_y, sequence_tuples_to_ndarray_x, sequence_tuples_to_ndarray_y, one_hot_decoding, \
    hours_shift_x, sequence_tuples_to_spatial_and_temporal_ndarrays, return_hour_from_sequence_y

from configurations.next_location_predict_baseline_config import DATASET, N_USUARIOS

class NextLocationPredictBaselineDomain:

    def model(self, model_name):

        try:
            if model_name == "GRU_baseline_original":
                return GRUBaselineOriginal().build()
            elif model_name == "GRU_baseline_filled":
                return GRUBaselineFilled().build()
            elif model_name == "STF_RNN_original":
                return STFrnnBaselineOriginal().build()
            elif model_name == "STF_RNN_filled":
                return STFrnnBaselineFilled().build()
            elif model_name == "LSTM_48hours_baseline_original":
                return LSTM48HoursBaselineOriginal().build()
            elif model_name == "LSTM_48hours_baseline_filled":
                return LSTM48HoursBaselineFilled().build()
            elif model_name == "Deep_move":
                return DeepMoveBaseline().build()
            elif model_name == "MAP_original":
                return MAPBaselineOriginal().build()
            elif model_name == "MAP_filled":
                return MAPBaselineFilled().build()
            elif model_name == "MHSA":
                return MHSABaselineWithDropout().build()
            else:
                raise ValueError("Selecione um modelo")
        except Exception as e:
            raise e

    def extract_train_test_dataset(self, filename, location_num_classes, time_num_classes, timeOutput, n_location_outputs,
                                   step_size, deep_move):

        df = pd.read_csv(filename)

        # Convert string of list to list
        users_list = []
        locations_count = {0: 0, 1: 0, 2: 0}
        for i in range(df['location_sequence'].shape[0]):
            user_list = []
            user = df['location_sequence'].iloc[i]
            user = user.replace("[", "").replace("]", "").split("), ")
            for e in user:
                e = e.replace("(", "").replace(")", "")
                #location, hour = e.split(",")
                datetime, hour, location = e.split(",")
                datetime = str(datetime).replace("'","")
                #datetime = pd.to_datetime(pd.Series([datetime]), format="%Y-%m-%d %H:%M:%S")[0]
                datetime = dt.datetime.strptime(datetime, "%Y-%m-%d %H:%M:%S")
                location = int(location)
                locations_count[location] = locations_count[location] + 1
                hour = int(hour)
                location = int(location)
                hour = int(hour)
                id = i
                if deep_move:
                    # location = location + 1
                    # hour = hour + 1
                    # id = id + 1
                    user_list.append((location, hour, id, datetime))
                else:
                    user_list.append((location, hour, id))
            users_list.append(user_list)


        x_train_fold = []
        x_test_fold = []
        y_train_fold = []
        y_test_fold = []
        # Get train and test datasets from user
        # users_list = users_list[:N_USUARIOS]

        X_train_concat = []
        X_test_concat = []
        y_train_concat = []
        y_test_concat = []
        kf = KFold(n_splits=4)
        for user in users_list:
            size = len(user)
            train_size = int(size * 0.7)
            test_size = size - train_size
            train = user[:train_size]
            test = user[train_size::]
            X_train, y_train = sequence_to_x_y(train, step_size)
            X_test, y_test = sequence_to_x_y(test, step_size)

            X_train_concat = X_train_concat + X_train
            X_test_concat = X_test_concat + X_test
            y_train_concat = y_train_concat + y_train
            y_test_concat = y_test_concat + y_test

        X_train = X_train_concat
        X_test = X_test_concat
        y_train = y_train_concat
        y_test = y_test_concat

        y_train_hours = return_hour_from_sequence_y(y_train)
        y_test_hours = return_hour_from_sequence_y(y_test)

        # Remove hours. Currently training without events hour
        # X_train = remove_hour_from_sequence_x(X_train)
        # X_test = remove_hour_from_sequence_x(X_test)
        y_train = remove_hour_from_sequence_y(y_train)
        y_test = remove_hour_from_sequence_y(y_test)

        # Sequence tuples to ndarray. Use it if the remove hour function was not called
        # X_train = sequence_tuples_to_ndarray_x(X_train)
        # X_test = sequence_tuples_to_ndarray_x(X_test)
        # y_train = sequence_tuples_to_ndarray_y(y_train)
        # y_test = sequence_tuples_to_ndarray_y(y_test)

        # Sequence tuples to [spatial[,step_size], temporal[,step_size]] ndarray. Use with embedding layer.
        X_train = sequence_tuples_to_spatial_and_temporal_ndarrays(X_train, id=True, deep_move=deep_move)
        X_test = sequence_tuples_to_spatial_and_temporal_ndarrays(X_test, id=True, deep_move=deep_move)

        #print("x train: ", X_train[0].shape, X_train[1][0], X_train[2][0], X_train[3].shape, X_train[4][0], X_train[5][0])

        # X_train = np.asarray(X_train)
        # X_train = X_train.reshape(X_train.shape + (1,)) # perform it if the step doesn't have hour
        y_train = np.asarray(y_train)
        # X_test = np.asarray(X_test)
        # X_test = X_test.reshape(X_test.shape + (1,))
        y_test = np.asarray(y_test)

        # Convert integers to one-hot-encoding. It is important to convert the y (labels) to that.

        # X_train = np_utils.to_categorical(X_train)
        # X_test = np_utils.to_categorical(X_test)

        if deep_move:
            #     location_num_classes = location_num_classes + 1
            #     time_num_classes = time_num_classes + 1
            pad_zero = [0]*(9-1)
            y_train_2 = np.asarray([[np_utils.to_categorical([e]+pad_zero, num_classes=location_num_classes).tolist()]*step_size for e in y_train])
            y_train = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=location_num_classes) for e in y_train])
            y_train_hours = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=time_num_classes) for e in y_train_hours])
            y_test_2 = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=location_num_classes) for e in y_test])
            y_test = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=location_num_classes) for e in y_test])
            y_test_hours = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=time_num_classes) for e in y_test_hours])
        else:
            y_train_2 = np_utils.to_categorical(y_train, num_classes=location_num_classes)
            y_train = np_utils.to_categorical(y_train, num_classes=location_num_classes)
            y_train_hours = np_utils.to_categorical(y_train_hours, num_classes=time_num_classes)
            y_test_2 = np_utils.to_categorical(y_test, num_classes=location_num_classes)
            y_test = np_utils.to_categorical(y_test, num_classes=location_num_classes)
            y_test_hours = np_utils.to_categorical(y_test_hours, num_classes=time_num_classes)

        # when using multiple output [location, hour]
        if timeOutput:
            y_train = [y_train, y_train_hours]
            y_test = [y_test, y_test_hours]
        else:
            if n_location_outputs == 2:
                y_train = [y_train, y_train_2]
                y_test = [y_test, y_test_2]
            elif n_location_outputs == 1:
                y_train = [y_train]
                y_test = [y_test]

        print("Quantidade ---- Casa: ", locations_count[0], " ---- Outro: ", locations_count[1],
              " ---- Deslocamento: ", locations_count[2], "y_train shape: ", y_train[0].shape, " y_test shape: ", y_test[0].shape)

        return X_train, X_test, y_train, y_test

    def extract_train_test_indexes_k_fold(self, filename, location_num_classes, time_num_classes, timeOutput, n_location_outputs,
                                          step_size, deep_move, n_splits):
        print("leu: ", filename)
        df = pd.read_csv(filename)

        # Convert string of list to list
        users_list = []
        locations_count = {0: 0, 1: 0, 2: 0}
        max_id = -1
        for i in range(df['location_sequence'].shape[0]):
            user_list = []
            user = df['location_sequence'].iloc[i]
            user = user.replace("[", "").replace("]", "").split("), ")
            for e in user:
                e = e.replace("(", "").replace(")", "")
                #location, hour = e.split(",")
                datetime, hour, location = e.split(",")
                datetime = str(datetime).replace("'","")
                #datetime = pd.to_datetime(pd.Series([datetime]), format="%Y-%m-%d %H:%M:%S")[0]
                datetime = dt.datetime.strptime(datetime, "%Y-%m-%d %H:%M:%S")
                location = int(location)
                locations_count[location] = locations_count[location] + 1
                hour = int(hour)
                location = int(location)
                hour = int(hour)
                id = i
                if deep_move:
                    # location = location + 1
                    # hour = hour + 1
                    # id = id + 1
                    user_list.append((location, hour, id, datetime))
                else:
                    user_list.append((location, hour, id))
                    if id > max_id:
                        max_id = id
            users_list.append(user_list)

        kf = KFold(n_splits=n_splits)
        users_train_indexes = [None]*n_splits
        users_test_indexes = [None]*n_splits
        for user in users_list:
            i = 0
            for train_indexes, test_indexes in kf.split(user):
                if users_train_indexes[i] is None:
                    users_train_indexes[i] = [train_indexes]
                    users_test_indexes[i] = [test_indexes]
                else:
                    users_train_indexes[i].append(train_indexes)
                    users_test_indexes[i].append(test_indexes)
                i = i + 1

        print("Quantidade ---- Casa: ", locations_count[0], " ---- Outro: ", locations_count[1],
              " ---- Deslocamento: ", locations_count[2])

        print("Max id: ", max_id)

        return users_list, users_train_indexes, users_test_indexes



    def run_tests(self, n_tests, model_name, outputTime, epochs, time_num_classes, filename, n_location_outputs,
                  optimizer, class_weight, X_train, y_train, X_test, y_test):
        if not outputTime:
            if n_location_outputs == 2:
                return  self.run_tests_two_locations_outputs(n_tests=n_tests, model_name=model_name, epochs=epochs,
                                                             X_train=X_train, optimizer=optimizer, y_train=y_train, X_test=X_test, y_test=y_test, filename=filename)
            elif n_location_outputs == 1:
                return self.run_tests_one_location_output(n_tests=n_tests, model_name=model_name, epochs=epochs,
                                                          optimizer=optimizer, class_weight=class_weight, X_train=X_train, y_train=y_train,
                                                          X_test=X_test, y_test=y_test, filename=filename)
        else:
            return self.run_test_location_time(n_tests=n_tests, model_name=model_name, epochs=epochs,
                                               optimizer=optimizer, X_train=X_train, y_train=y_train, X_test=X_test,
                                               y_test=y_test, time_num_classes=time_num_classes, filename=filename)

    def run_test_location_time(self, n_tests, model_name, epochs, X_train, optimizer, y_train, X_test, y_test,
                               time_num_classes, filename):

        location_report_1 = {"Casa": {"precision": 0, "recall": 0, "f1-score": 0},
                             "Outro": {"precision": 0, "recall": 0, "f1-score": 0},
                             "Deslocamento": {"precision": 0, "recall": 0, "f1-score": 0}, "accuracy": 0}

        time_report = {str(i): {"precision": 0, "recall": 0, "f1-score": 0} for i in range(24)}
        time_report['accuracy'] = 0

        for i in range(n_tests):
            model = NextLocationPredictBaselineDomain().model(model_name)

            losses = {
                "location_output": "categorical_crossentropy",
                "time_output": "categorical_crossentropy",
            }
            lossWeights = {"location_output": 1.0, "time_output": 1.0}
            INIT_LR = 1e-3
            # opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
            model.compile(optimizer=optimizer, loss=["categorical_crossentropy", "categorical_crossentropy"],
                          loss_weights=[1, 1], metrics=['accuracy'])

            # % % time
            class_weight = {0: 0.4,
                            1: 0.3,
                            2: 0.3}

            # , class_weight=class_weight
            hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs)

            h = pd.DataFrame(hi.history)
            print("summary: ", model.summary())
            # print("history: ", h)

            y_predict_location, y_predict_time = model.predict(X_test, batch_size=10)

            scores = model.evaluate(X_test, y_test, verbose=0)
            # location_acc = scores
            # print(scores)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------- Location ------------")
            y_predict_location = one_hot_decoding(y_predict_location)
            y_test_location = one_hot_decoding(y_test[0])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_1 = self.add_location_report(location_report_1, report)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------ Time ------------")
            y_predict_time = one_hot_decoding(y_predict_time)
            y_test_time = one_hot_decoding(y_test[1])
            # print("Original: ", y_test)
            # print("previu: ", y_predict_time)
            report = skm.classification_report(y_test_time, y_predict_time, target_names=[str(i) for i in range(
                time_num_classes)], output_dict=True)
            time_report = self.add_time_report(time_report, report)

        # Calculate average for each metric
        location_report_1 = self.calculate_resultant_report(location_report_1, n_tests)

        time_report = self.calculate_resultant_report(time_report, n_tests)

        print("Relatorio final: " + model_name)
        print("Dataset: ", filename)
        print("Testes: ", n_tests, "Epocas: ", epochs)
        print("Location")
        print(location_report_1)

        print("Time")
        print(time_report)

        return location_report_1, time_report, h

    def run_tests_two_locations_outputs(self, n_tests: int, model_name: str, epochs, optimizer, X_train,
                                        y_train, X_test, y_test, filename):
        location_report_1 = {"Casa": {"precision": [], "recall": [], "f1-score": []},
                             "Outro": {"precision": [], "recall": [], "f1-score": []},
                             "Deslocamento": {"precision": [], "recall": [], "f1-score": []},
                             "accuracy": []}

        location_report_plot = {"precisao_casa": [],
                                "precisao_outro": [],
                                "precisao_deslocamento": [],
                                "revocacao_casa": [],
                                "revocacao_outro": [],
                                "revocacao_deslocamento": [],
                                "fscore_casa": [],
                                "fscore_outro": [],
                                "fscore_deslocamento": [],
                                "accurary": [],
                                "location": []}

        for i in range(n_tests):
            model = NextLocationPredictBaselineDomain().model(model_name)

            losses = {
                "location_output": "categorical_crossentropy",
                "time_output": "categorical_crossentropy",
            }
            lossWeights = {"location_output": 1.0, "time_output": 1.0}
            INIT_LR = 1e-3
            # opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
            model.compile(optimizer=optimizer, loss=["categorical_crossentropy", "categorical_crossentropy"],
                          loss_weights=[1,1], metrics=['accuracy'])

            # , class_weight=class_weight
            hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs,
                           class_weight={0: 1, 1: 9, 2: 1})

            h = pd.DataFrame(hi.history)
            print("summary: ", model.summary())
            # print("history: ", h)

            y_predict_location_1, y_predict_location_2 = model.predict(X_test, batch_size=100)
            #y_predict_location_1 = y_predict_location_2
            y_predict_location_1 = np.multiply(np.add(np.multiply(y_predict_location_1, 1.9), np.multiply(y_predict_location_2, 1.1)), 1/3)

            print("tipoo: ", y_predict_location_1)

            scores = model.evaluate(X_test, y_test, verbose=0)
            # location_acc = scores
            # print(scores)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------- Location ------------")
            y_predict_location_1 = one_hot_decoding(y_predict_location_1)
            y_test_location = one_hot_decoding(y_test[0])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location_1,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_1 = self.add_location_report(location_report_1, report)

        location_report = copy.deepcopy(location_report_1)

        print("lista: ", location_report_1)
        for l_key in location_report_1:
            if l_key == 'accuracy' or l_key == 'tests' or l_key == 'epochs':
                location_report_1[l_key] = str(location_report_1[l_key])
                continue
            for v_key in location_report_1[l_key]:
                location_report_1[l_key][v_key] = str(location_report_1[l_key][v_key])

        location_report_1['tests'] = n_tests
        location_report_1['epochs'] = epochs

        # Calculate average for each metric
        location_report = self.calculate_resultant_report(location_report, n_tests, epochs)
        plot_report = self.report_to_plot_report(pd.DataFrame(location_report_1))

        print("Relatorio final: " + model_name)
        print("Dataset: ", filename)
        print("Optimizer: ", optimizer)
        print("Testes: ", n_tests, "Epocas: ", epochs)
        print("Location")
        print(location_report)
        return location_report_1, None, h, plot_report

    def run_tests_one_location_output(self, n_tests: int, model_name: str, epochs, optimizer, class_weight, X_train,
                                       y_train, X_test, y_test, filename):
        location_report_1 = {"Casa": {"precision": [], "recall": [], "f1-score": []},
                             "Outro": {"precision": [], "recall": [], "f1-score": []},
                             "Deslocamento": {"precision": [], "recall": [], "f1-score": []},
                             "accuracy": []}

        location_report_plot = {"precisao_casa": [],
                             "precisao_outro": [],
                             "precisao_deslocamento": [],
                             "revocacao_casa": [],
                             "revocacao_outro": [],
                             "revocacao_deslocamento": [],
                             "fscore_casa": [],
                             "fscore_outro": [],
                             "fscore_deslocamento": [],
                             "accurary": [],
                                "location": []}

        for i in range(n_tests):
            model = NextLocationPredictBaselineDomain().model(model_name)

            losses = {
                "location_output": "categorical_crossentropy",
                "time_output": "categorical_crossentropy",
            }
            lossWeights = {"location_output": 1.0, "time_output": 1.0}
            INIT_LR = 1e-3
            # opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
            model.compile(optimizer=optimizer, loss=["categorical_crossentropy"],
                          loss_weights=[1], metrics=['accuracy'])

            # , class_weight=class_weight
            hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs,
                           class_weight=class_weight)

            h = pd.DataFrame(hi.history)
            print("summary: ", model.summary())
            # print("history: ", h)

            y_predict_location= model.predict(X_test, batch_size=100)

            scores = model.evaluate(X_test, y_test, verbose=0)
            # location_acc = scores
            # print(scores)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------- Location ------------")
            y_predict_location = one_hot_decoding(y_predict_location)
            y_test_location = one_hot_decoding(y_test[0])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_1 = self.add_location_report(location_report_1, report)

        location_report = copy.deepcopy(location_report_1)

        print("lista: ", location_report_1)
        for l_key in location_report_1:
            if l_key == 'accuracy' or l_key == 'tests' or l_key == 'epochs':
                location_report_1[l_key] = str(location_report_1[l_key])
                continue
            for v_key in location_report_1[l_key]:
                location_report_1[l_key][v_key] = str(location_report_1[l_key][v_key])

        location_report_1['tests'] = n_tests
        location_report_1['epochs'] = epochs

        # Calculate average for each metric
        location_report = self.calculate_resultant_report(location_report, n_tests, epochs)
        plot_report = self.report_to_plot_report(pd.DataFrame(location_report_1))

        print("Relatorio final: " + model_name)
        print("Dataset: ", filename)
        print("Optimizer: ", optimizer)
        print("Testes: ", n_tests, "Epocas: ", epochs)
        print("Location")
        print(location_report)
        return location_report_1, None, h, plot_report

    def run_tests_one_location_output_k_fold(self, model_name: str, epochs, optimizer, class_weight,
                                             users_list, users_train_indexes, users_test_indexes, filename, step_size,
                                             deep_move, location_num_classes, time_num_classes, n_location_outputs, n_testes):

        location_report_1 = {"Casa": {"precision": [], "recall": [], "f1-score": []},
                             "Outro": {"precision": [], "recall": [], "f1-score": []},
                             "Deslocamento": {"precision": [], "recall": [], "f1-score": []},
                             "accuracy": []}

        location_report_plot = {"precisao_casa": [],
                             "precisao_outro": [],
                             "precisao_deslocamento": [],
                             "revocacao_casa": [],
                             "revocacao_outro": [],
                             "revocacao_deslocamento": [],
                             "fscore_casa": [],
                             "fscore_outro": [],
                             "fscore_deslocamento": [],
                             "accurary": [],
                                "location": []}

        k_folds = n_testes

        for i in range(k_folds):
            X_train, X_test, y_train, y_test = self.extrat_train_test_from_indexes_k_fold(users_list=users_list,
                                                                                      train_indexes_fold=users_train_indexes[i],
                                                                                      test_indexes_fold=users_test_indexes[i],
                                                                                      step_size=step_size,
                                                                                      deep_move=deep_move,
                                                                                      location_num_classes=location_num_classes,
                                                                                      time_num_classes=time_num_classes,
                                                                                      n_location_outputs=n_location_outputs)
            model = NextLocationPredictBaselineDomain().model(model_name)

            losses = {
                "location_output": "categorical_crossentropy",
                "time_output": "categorical_crossentropy",
            }
            lossWeights = {"location_output": 1.0, "time_output": 1.0}
            INIT_LR = 1e-3
            # opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)

            if model_name not in ["GRU_baseline_original", "GRU_baseline_filled"]:
                model.compile(optimizer=optimizer, loss=["categorical_crossentropy"],
                              loss_weights=[1], metrics=['accuracy'])

                # , class_weight=class_weight
                hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs,
                               class_weight=class_weight)

                h = pd.DataFrame(hi.history)
                print("summary: ", model.summary())
                # print("history: ", h)

                y_predict_location= model.predict(X_test, batch_size=100)

            else:
                model.compile(optimizer=optimizer, loss=["categorical_crossentropy", "categorical_crossentropy"],
                              loss_weights=[1, 1], metrics=['accuracy'])

                # , class_weight=class_weight
                hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs,
                               class_weight=class_weight)

                h = pd.DataFrame(hi.history)
                print("summary: ", model.summary())
                # print("history: ", h)

                y_predict_location_1, y_predict_location_2 = model.predict(X_test, batch_size=100)
                # y_predict_location_1 = y_predict_location_2
                y_predict_location = np.multiply(
                    np.add(np.multiply(y_predict_location_1, 1.9), np.multiply(y_predict_location_2, 1.1)), 1 / 3)

            scores = model.evaluate(X_test, y_test, verbose=0)
            # location_acc = scores
            # print(scores)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------- Location ------------")
            y_predict_location = one_hot_decoding(y_predict_location)
            y_test_location = one_hot_decoding(y_test[0])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_1 = self.add_location_report(location_report_1, report)

        location_report = copy.deepcopy(location_report_1)

        print("lista: ", location_report_1)
        for l_key in location_report_1:
            if l_key == 'accuracy' or l_key == 'tests' or l_key == 'epochs':
                location_report_1[l_key] = str(location_report_1[l_key])
                continue
            for v_key in location_report_1[l_key]:
                location_report_1[l_key][v_key] = str(location_report_1[l_key][v_key])

        location_report_1['tests'] = k_folds
        location_report_1['epochs'] = epochs

        # Calculate average for each metric
        location_report = self.calculate_resultant_report(location_report, k_folds, epochs)
        plot_report = self.report_to_plot_report(pd.DataFrame(location_report_1))

        print("Relatorio final: " + model_name)
        print("Dataset: ", filename)
        print("Optimizer: ", optimizer)
        print("k_folds: ", k_folds, "Epocas: ", epochs)
        print("Location")
        print(location_report)
        return location_report_1, None, h, plot_report

    def extrat_train_test_from_indexes_k_fold(self, users_list, train_indexes_fold, test_indexes_fold, step_size,
                                              deep_move, location_num_classes, time_num_classes, n_location_outputs):

        X_train_concat = []
        X_test_concat = []
        y_train_concat = []
        y_test_concat = []
        print("tamanho users list: ", len(users_list), " tamanho indexes: ", len(train_indexes_fold),
              " tamanho indexes test: ", len(test_indexes_fold))
        for i in range(len(users_list)):
            user = np.asarray(users_list[i])
            train = user[train_indexes_fold[i]]
            test = user[test_indexes_fold[i]]
            X_train, y_train = sequence_to_x_y(train, step_size)
            X_test, y_test = sequence_to_x_y(test, step_size)

            X_train_concat = X_train_concat + X_train
            X_test_concat = X_test_concat + X_test
            y_train_concat = y_train_concat + y_train
            y_test_concat = y_test_concat + y_test

        X_train = X_train_concat
        X_test = X_test_concat
        y_train = y_train_concat
        y_test = y_test_concat

        y_train_hours = return_hour_from_sequence_y(y_train)
        y_test_hours = return_hour_from_sequence_y(y_test)

        # Remove hours. Currently training without events hour
        # X_train = remove_hour_from_sequence_x(X_train)
        # X_test = remove_hour_from_sequence_x(X_test)
        y_train = remove_hour_from_sequence_y(y_train)
        y_test = remove_hour_from_sequence_y(y_test)

        # Sequence tuples to ndarray. Use it if the remove hour function was not called
        # X_train = sequence_tuples_to_ndarray_x(X_train)
        # X_test = sequence_tuples_to_ndarray_x(X_test)
        # y_train = sequence_tuples_to_ndarray_y(y_train)
        # y_test = sequence_tuples_to_ndarray_y(y_test)

        # Sequence tuples to [spatial[,step_size], temporal[,step_size]] ndarray. Use with embedding layer.
        X_train = sequence_tuples_to_spatial_and_temporal_ndarrays(X_train, id=True, deep_move=deep_move)
        X_test = sequence_tuples_to_spatial_and_temporal_ndarrays(X_test, id=True, deep_move=deep_move)

        #print("x train: ", X_train[0].shape, X_train[1][0], X_train[2][0], X_train[3].shape, X_train[4][0], X_train[5][0])

        # X_train = np.asarray(X_train)
        # X_train = X_train.reshape(X_train.shape + (1,)) # perform it if the step doesn't have hour
        y_train = np.asarray(y_train)
        # X_test = np.asarray(X_test)
        # X_test = X_test.reshape(X_test.shape + (1,))
        y_test = np.asarray(y_test)

        # Convert integers to one-hot-encoding. It is important to convert the y (labels) to that.

        # X_train = np_utils.to_categorical(X_train)
        # X_test = np_utils.to_categorical(X_test)

        if deep_move:
            #     location_num_classes = location_num_classes + 1
            #     time_num_classes = time_num_classes + 1
            pad_zero = [0]*(9-1)
            y_train_2 = np.asarray([[np_utils.to_categorical([e]+pad_zero, num_classes=location_num_classes).tolist()]*step_size for e in y_train])
            y_train = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=location_num_classes) for e in y_train])
            y_train_hours = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=time_num_classes) for e in y_train_hours])
            y_test_2 = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=location_num_classes) for e in y_test])
            y_test = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=location_num_classes) for e in y_test])
            y_test_hours = np.asarray([np_utils.to_categorical([e]+pad_zero, num_classes=time_num_classes) for e in y_test_hours])
        else:
            y_train_2 = np_utils.to_categorical(y_train, num_classes=location_num_classes)
            y_train = np_utils.to_categorical(y_train, num_classes=location_num_classes)
            y_train_hours = np_utils.to_categorical(y_train_hours, num_classes=time_num_classes)
            y_test_2 = np_utils.to_categorical(y_test, num_classes=location_num_classes)
            y_test = np_utils.to_categorical(y_test, num_classes=location_num_classes)
            y_test_hours = np_utils.to_categorical(y_test_hours, num_classes=time_num_classes)

        # when using multiple output [location, hour]
        if n_location_outputs == 2:
            y_train = [y_train, y_train_2]
            y_test = [y_test, y_test_2]
        elif n_location_outputs == 1:
            y_train = [y_train]
            y_test = [y_test]

        return X_train, X_test, y_train, y_test

    def add_location_report(self, location_report, report):
        for l_key in location_report:
            if l_key == 'accuracy':
                location_report[l_key].append(report[l_key])
                continue
            for v_key in location_report[l_key]:
                location_report[l_key][v_key].append(report[l_key][v_key])

        return location_report

    def add_time_report(self, time_report, report):
        for l_key in time_report:
            if l_key == 'accuracy':
                time_report[l_key] = time_report[l_key] + report[l_key]
                continue
            for v_key in time_report[l_key]:
                time_report[l_key][v_key] = time_report[l_key][v_key] + report[l_key][v_key]

        return time_report

    def calculate_resultant_report(self, report, n_tests, epochs):

        new_report = copy.deepcopy(report)
        for l_key in report:
            if l_key == 'accuracy':
                new_report[l_key] = sum(report[l_key]) / n_tests
                continue
            for v_key in report[l_key]:
                new_report[l_key][v_key] = sum(report[l_key][v_key]) / n_tests

        new_report['tests'] = n_tests
        new_report['epochs'] = epochs

        return new_report

    def report_to_plot_report(self, df):

        columns = []
        for column in df.columns:
            if column not in ["metric", "accuracy", "epochs", "tests"]:
                columns.append(column)
        names = []
        indexes = df.index.tolist()
        # for index in df.index.tolist():
        #     if index not in ["metric", "acuracy", "epochs", "tests"]:
        #         indexes.append(index)

        values = []

        new_df = {}

        for index in indexes:
            row = df.loc[index]
            values = []
            for column in columns:
                value = ast.literal_eval(row[column])
                size = len(value)
                name = [column]*size
                values = values + value

            new_df[index] = values

        for column in columns:
            names = names + [column]*size

        new_df['location'] = names

        return pd.DataFrame(new_df)

