from extractors.csv_extractor import CSVExtractor
from configurations.identification_classification_config import JOB_CONFIG, JOB_CONFIG_INDEX, N_USUARIOS


class IdentificationClassificationDomain:

    def __init__(self):
        self.csv_extractor = CSVExtractor()

    def extract_events_from_csv(self, filename: str) -> dict:
        print("---- Lendo arquivo ----")
        df = self.csv_extractor.read(filename)

        dados = {}  # dados de cada usuario

        ids = df['id'].unique().tolist()
        users_limit = JOB_CONFIG[JOB_CONFIG_INDEX]["users_limit"]
        if users_limit != -1:
            ids = ids[:users_limit]
        quantidade_amostras = len(df['latitude'].tolist())
        media_pontos_usuarios = quantidade_amostras / len(ids)
        cont = 0
        for i in ids:
            if cont >= N_USUARIOS:
                break
            df2 = df.query("id==" + str(i) + "")
            d = df2.sort_values(by=['datetime'])
            dados[i] = d
            cont = cont + 1

        return dados

    # def idenfity_classify(self, data, metrica, df_metrics, mode):
    #
    #     poi_identification = identification.Identification(DAYS_TO_PREDICT[i], DAYS_TO_TRAIN)
    #     usuarios = poi_identification.identify_points_of_interest(data)
    #     solution = p.Proposta(copy.deepcopy(usuarios), DAYS_TO_PREDICT[i], metrica.COSINE, metrica.EUCLIDIAN_DISTANCE,
    #                           metrica.RELATIVE_ENTROPY)
    #     solution.rotulate()
    #     solution.calculate_metrics()
    #     # print("Distancia:", dis)
    #     # solution.plot_entropy(0.8, 0.08, 0.006)
    #     # solution.display(0.1)
    #     # solution.rotulated_users_pois_analysis()
    #     """solution.users_pois_to_dataframe("pois_1k_to_predict.csv")
    #     clf = load('classificadortreinado.joblib')
    #     df = pd.read_csv('pois_10k_to_predict.csv')[['home_percentage_total_events', 'home_time_events', \
    #                                                  'work_percentage_total_events', 'work_time_events', \
    #                                                 'installation_id', 'poi_id', 'user_index']]
    #     x_predict = df[['home_percentage_total_events', 'home_time_events', 'work_percentage_total_events', 'work_time_events']].values.tolist()
    #     predicted = clf.predict(x_predict)
    #     predicted = [location_id_to_name(e) for e in predicted]
    #     df['class'] = pd.Series(predicted)
    #     df['poi_id'] = df['poi_id'].astype(np.int64)
    #     solution.calculate_users_locations_probabilities(df[['installation_id', 'poi_id', 'user_index', 'class']])"""
    #     # solution.solution_one_calculate_users_locations_probabilities()
    #     solution.solution_one_calculate_users_locations_probabilities_all()
    #     solution.users_predicted_places_to_csv()
    #     solution.users_home_entropy_to_csv_all("users_home_entropy_10k_all.csv")
    #     df_metrics = solution.routine_performance_dataframe(df_metrics, METRIC)