from jobs.identification_classification_job import IdentificationClassificationJob
from jobs.next_location_predict_job import NextLocationPredictJob
from jobs.next_location_predict_baseline_job import NextLocationPredictBaselineJob

if __name__ == "__main__":

    #job = IdentificationClassificationJob()
    job = NextLocationPredictJob(0)
    #job = NextLocationPredictBaselineJob(8) # 0 ,2, 3 and 4 working

    job.start()