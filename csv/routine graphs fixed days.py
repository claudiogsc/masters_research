import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd
palette = sns.color_palette()

metrica = "cosine"
quantidade = "200"
file = {"cosine":[quantidade + "users_days_to_train_7metrica_cosine.csv", quantidade + "users_days_to_train_14metrica_cosine.csv"],
        "relative":["400users_days_to_train_7metrica_relative.csv", "400users_days_to_train_11metrica_relative.csv",
        "400users_days_to_train_14metrica_relative.csv"]}

d7 = pd.read_csv(file[metrica][0], encoding='utf-8')
d7.columns=['Average','Days to predict', 'Displacement average','Home average', 'id', 'inactive_interval_lenght', 'Metric','Other average','Train period','Users']
d7['Days to predict'] = d7['Days to predict'].astype('int64')

# d11 = pd.read_csv(file[metrica][1], encoding='utf-8')
# d11.columns=['Average','Days to predict', 'Displacement average','Home average','id', 'inactive_interval_lenght', 'Metric','Other average','Train period','Users']
# d11['Days to predict'] = d11['Days to predict'].astype('int64')

d14 = pd.read_csv(file[metrica][1], encoding='utf-8')
d14.columns=['Average','Days to predict', 'Displacement average','Home average', 'id', 'inactive_interval_lenght', 'Metric','Other average','Train period','Users']
d14['Days to predict'] = d14['Days to predict'].astype('int64')

intersection = pd.Index(d7['id'].unique().tolist()).intersection(pd.Index(d14['id'].unique().tolist()))
intersection = intersection.intersection(pd.Index(d14['id'].unique().tolist())).tolist()
# intersection = pd.DataFrame({"idi":intersection})
# print("colunas: ", d7.columns)
# d7 = pd.merge(d7, intersection, how='right', left_on='key', right_on='idi')
# d11 = pd.merge(d11, intersection, how='right', left_on='key', right_on='idi')
# d14 = pd.merge(d14, intersection, how='right', left_on='key', right_on='idi')
d7 = d7.query("id in " + str(intersection))
#d11 = d11.query("id in " + str(intersection))
d14 = d14.query("id in " + str(intersection))
tamanho = min([d7.shape[0], d14.shape[0]])
print(tamanho)
d7 = d7[:tamanho]
d14 = d14[:tamanho]

averages_index = 3
averages = ['Average', 'Displacement average', 'Home average', 'Other average', 'inactive_interval_lenght']
average = averages[averages_index]
ax = plt.gca()
ax.set_xlabel("Days to predict", fontsize=16)
ax.set_ylabel("Average", fontsize=17)
#ax.set_ylim((0.4,0.8))
#ax.legend(loc='lower right')
for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize(16)
for tick in ax.yaxis.get_ticklabels():
    tick.set_fontsize(16)
#ax.tick_params(labelsize=14)
graph = sns.lineplot(x="Days to predict", y=average, data=d7, hue=["7 train days"]*tamanho, palette="Reds", err_style="band", ci=95, ax=ax)
#plt.setp(ax.get_legend().get_texts(), fontsize='15')
ax.legend(loc='lower right', fontsize='15')
# graph = sns.lineplot(x="Days to predict", y=average, data=d11, hue=["11 train days"] * tamanho, palette="cubehelix", err_style="band", ci=95, ax=ax)
# #plt.setp(ax.get_legend().get_texts(), fontsize='15')
# ax.legend(loc='lower right', fontsize='15')
graph = sns.lineplot(x="Days to predict", y=average, data=d14, hue=["14 train days"] * tamanho, palette="Blues", err_style="band", ci=95, ax=ax)
#plt.setp(ax.get_legend().get_texts(), fontsize='15')
ax.legend(loc='lower right', fontsize='15')
#precisao = sns.lineplot(x="Distância (m)", y="Precisão", data=dbscanpo, hue=["Dbscan puro"]*10, palette="prism_r")
figura_fscore = graph.get_figure()
figura_fscore.savefig(quantidade + "_users_fixed_days_" + average + "_metrica_" + metrica + ".png", dpi=400, bbox_inches='tight')