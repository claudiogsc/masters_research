import pandas as pd
import statistics as st


df = pd.read_csv('df_mais_de_1_mil_limite_500_pontos.csv')

print(df.columns)

ids = df['installation_id'].unique().tolist()
df['reference_date'] = pd.to_datetime(df['reference_date'], infer_datetime_format=True)
cont = 0
periodos = []
print("Quantidade: ", len(ids))
for id_ in ids:

    user = df.query('installation_id ==' + str(id_))
    dates = user['reference_date'].describe()
    periodo = dates['last'] - dates['first']
    periodos.append(periodo.days)
    if cont < 2:
        pass

    cont = cont + 1

s = pd.Series(periodos)

print("media: ", st.mean(periodos), s.describe())